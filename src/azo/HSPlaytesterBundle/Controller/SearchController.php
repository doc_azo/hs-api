<?php

namespace azo\HSPlaytesterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SearchController extends Controller
{
    public function mainAction()
    {
        $provider = $this->get('azo_hs_playtester.api_provider');

        $cards = array($provider->get('Piloted Shredder'), $provider->get('Nexus-Champion Saraad'));

        return $this->render('azoHSPlaytesterBundle:Search:main.html.twig', array('data' => $cards));
    }
}
