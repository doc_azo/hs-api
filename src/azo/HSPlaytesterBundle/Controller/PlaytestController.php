<?php

namespace azo\HSPlaytesterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PlaytestController extends Controller
{

    public function mainAction(Request $request)
    {
        return $this->render('azoHSPlaytesterBundle:Playtest:main.html.twig', array());
    }

}
