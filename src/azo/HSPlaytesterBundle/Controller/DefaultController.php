<?php

namespace azo\HSPlaytesterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        $curl = new Client();
        $res = $curl->get('https://omgvamp-hearthstone-v1.p.mashape.com/cardbacks', array(
            'headers' => array('X-Mashape-Key' => 'mG0QIINl7ZmshLKCriuDWAAntYkPp1YbLsSjsnc4hg2a286P7t')
        ));
        $backs = json_decode($res->getBody(), true);
        if (is_null($backs) || $backs === false) {
            throw new \Exception('Could not retrieve cards.');
        }

        $back = $backs[7]['img'];

        $curl = new Client();
        $res = $curl->get('https://omgvamp-hearthstone-v1.p.mashape.com/cards/classes/Druid', array(
            'headers' => array('X-Mashape-Key' => 'mG0QIINl7ZmshLKCriuDWAAntYkPp1YbLsSjsnc4hg2a286P7t')
        ));

        $cards = json_decode($res->getBody(), true);
        if (is_null($cards) || $cards === false) {
            throw new \Exception('Could not retrieve cards.');
        }

        $cardsArray = [];
        foreach ($cards as $i => $card) {
            if (isset($card['img'])) {
                $cardsArray[] = $card['img'];
            }
        }

        return $this->render('azoHSPlaytesterBundle:Default:index.html.twig', array('cards' => $cardsArray, 'back' => $back));
    }


//    public function indexAction($name)
//    {
//        die('debug purpose');
//        $curl = new Client();
//        $res = $curl->get('https://omgvamp-hearthstone-v1.p.mashape.com/cards', array(
//            'headers' => array('X-Mashape-Key' => 'mG0QIINl7ZmshLKCriuDWAAntYkPp1YbLsSjsnc4hg2a286P7t')
//        ));
//
//        $cards = json_decode($res->getBody(), true);
//        if (is_null($cards) || $cards === false) {
//            throw new \Exception('Could not retrieve cards.');
//        }
//
//        $html = '<h1>Cartes</h1>';
//        foreach ($cards as $set => $setCards) {
//            $html .= '<h2>' . $set . '</h2><p>';
//            foreach ($setCards as $card) {
//                if (isset($card['img'])) {
//                    $html .= '<img src="' . $card['imgGold'] . '" />';
//                }
//            }
//            $html .= '</p>';
//        }
//
//        return new Response($html);
//    }
}
