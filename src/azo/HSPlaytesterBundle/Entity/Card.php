<?php
namespace azo\HSPlaytesterBundle\Entity;

class Card
{

    protected $id;
    protected $name;
    protected $type;
    protected $cost;
    protected $attack;
    protected $health;
    protected $text;
    protected $flavor;
    protected $artist;
    protected $legendary;
    protected $minionType;
    protected $img;
    protected $imgGold;
    protected $rarity;
    protected $class;
    protected $set;

    function __get($name)
    {
        return $this->$name;
    }

    function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __isset($name)
    {
        if (property_exists(get_class($this),$name)) {
            return true;
        }
        return false;
    }

}