<?php
namespace azo\HSPlaytesterBundle\Model\Provider;

interface CardsProviderInterface
{

    public function get($id);

    public function getBack($id);

}