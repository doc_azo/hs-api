<?php
namespace azo\HSPlaytesterBundle\Model\Provider;

use azo\HSPlaytesterBundle\Entity\Card;
use GuzzleHttp\Client;

class ApiProvider implements CardsProviderInterface
{
    protected $apiKey;

    public function __construct()
    {
        $this->apiKey = 'mG0QIINl7ZmshLKCriuDWAAntYkPp1YbLsSjsnc4hg2a286P7t';
    }

    protected function curlApi($url)
    {
        $curl = new Client();
        $res = $curl->get(
            $url,
            array(
                'headers' => array(
                    'X-Mashape-Key' => $this->apiKey
                )
            )
        );
        $return = json_decode($res->getBody(), true);
        if(is_array($return) && count($return) === 1){
            return array_pop($return);
        }
        return $return;
    }

    protected function toCard(array $array)
    {
        $card = new Card();
        foreach($array as $property => $value){
            $card->$property = $value;
        }
        return $card;
    }

    public function get($name)
    {
        $url = 'https://omgvamp-hearthstone-v1.p.mashape.com/cards/search/' . rawurlencode($name);
        $card = $this->curlApi($url);
        if(!is_array($card)){
            return null;
        }
        return $this->toCard($card);
    }

    public function getBack($id)
    {
        // TODO: Implement getBack() method.
    }


}