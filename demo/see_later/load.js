var requirejs, require, define;
(function (global) {
    function isFunction(t) {
        return "[object Function]" === ostring.call(t)
    }

    function isArray(t) {
        return "[object Array]" === ostring.call(t)
    }

    function each(t, e) {
        if (t) {
            var n;
            for (n = 0; t.length > n && (!t[n] || !e(t[n], n, t)); n += 1);
        }
    }

    function eachReverse(t, e) {
        if (t) {
            var n;
            for (n = t.length - 1; n > -1 && (!t[n] || !e(t[n], n, t)); n -= 1);
        }
    }

    function hasProp(t, e) {
        return hasOwn.call(t, e)
    }

    function getOwn(t, e) {
        return hasProp(t, e) && t[e]
    }

    function eachProp(t, e) {
        var n;
        for (n in t)if (hasProp(t, n) && e(t[n], n))break
    }

    function mixin(t, e, n, r) {
        return e && eachProp(e, function (e, i) {
            (n || !hasProp(t, i)) && (r && "string" != typeof e ? (t[i] || (t[i] = {}), mixin(t[i], e, n, r)) : t[i] = e)
        }), t
    }

    function bind(t, e) {
        return function () {
            return e.apply(t, arguments)
        }
    }

    function scripts() {
        return document.getElementsByTagName("script")
    }

    function getGlobal(t) {
        if (!t)return t;
        var e = global;
        return each(t.split("."), function (t) {
            e = e[t]
        }), e
    }

    function makeError(t, e, n, r) {
        var i = Error(e + "\nhttp://requirejs.org/docs/errors.html#" + t);
        return i.requireType = t, i.requireModules = r, n && (i.originalError = n), i
    }

    function newContext(t) {
        function e(t) {
            var e, n;
            for (e = 0; t[e]; e += 1)if (n = t[e], "." === n)t.splice(e, 1), e -= 1; else if (".." === n) {
                if (1 === e && (".." === t[2] || ".." === t[0]))break;
                e > 0 && (t.splice(e - 1, 2), e -= 2)
            }
        }

        function n(t, n, r) {
            var i, o, a, s, u, c, d, l, f, h, p, m = n && n.split("/"), v = m, g = _.map, y = g && g["*"];
            if (t && "." === t.charAt(0) && (n ? (v = getOwn(_.pkgs, n) ? m = [n] : m.slice(0, m.length - 1), t = v.concat(t.split("/")), e(t), o = getOwn(_.pkgs, i = t[0]), t = t.join("/"), o && t === i + "/" + o.main && (t = i)) : 0 === t.indexOf("./") && (t = t.substring(2))), r && (m || y) && g) {
                for (s = t.split("/"), u = s.length; u > 0; u -= 1) {
                    if (d = s.slice(0, u).join("/"), m)for (c = m.length; c > 0; c -= 1)if (a = getOwn(g, m.slice(0, c).join("/")), a && (a = getOwn(a, d))) {
                        l = a, f = u;
                        break
                    }
                    if (l)break;
                    !h && y && getOwn(y, d) && (h = getOwn(y, d), p = u)
                }
                !l && h && (l = h, f = p), l && (s.splice(0, f, l), t = s.join("/"))
            }
            return t
        }

        function r(t) {
            isBrowser && each(scripts(), function (e) {
                return e.getAttribute("data-requiremodule") === t && e.getAttribute("data-requirecontext") === w.contextName ? (e.parentNode.removeChild(e), !0) : void 0
            })
        }

        function i(t) {
            var e = getOwn(_.paths, t);
            return e && isArray(e) && e.length > 1 ? (r(t), e.shift(), w.require.undef(t), w.require([t]), !0) : void 0
        }

        function o(t) {
            var e, n = t ? t.indexOf("!") : -1;
            return n > -1 && (e = t.substring(0, n), t = t.substring(n + 1, t.length)), [e, t]
        }

        function a(t, e, r, i) {
            var a, s, u, c, d = null, l = e ? e.name : null, f = t, h = !0, p = "";
            return t || (h = !1, t = "_@r" + ($ += 1)), c = o(t), d = c[0], t = c[1], d && (d = n(d, l, i), s = getOwn(S, d)), t && (d ? p = s && s.normalize ? s.normalize(t, function (t) {
                return n(t, l, i)
            }) : n(t, l, i) : (p = n(t, l, i), c = o(p), d = c[0], p = c[1], r = !0, a = w.nameToUrl(p))), u = !d || s || r ? "" : "_unnormalized" + (O += 1), {
                prefix: d,
                name: p,
                parentMap: e,
                unnormalized: !!u,
                url: a,
                originalName: f,
                isDefine: h,
                id: (d ? d + "!" + p : p) + u
            }
        }

        function s(t) {
            var e = t.id, n = getOwn(C, e);
            return n || (n = C[e] = new w.Module(t)), n
        }

        function u(t, e, n) {
            var r = t.id, i = getOwn(C, r);
            !hasProp(S, r) || i && !i.defineEmitComplete ? s(t).on(e, n) : "defined" === e && n(S[r])
        }

        function c(t, e) {
            var n = t.requireModules, r = !1;
            e ? e(t) : (each(n, function (e) {
                var n = getOwn(C, e);
                n && (n.error = t, n.events.error && (r = !0, n.emit("error", t)))
            }), r || req.onError(t))
        }

        function d() {
            globalDefQueue.length && (apsp.apply(T, [T.length - 1, 0].concat(globalDefQueue)), globalDefQueue = [])
        }

        function l(t) {
            delete C[t]
        }

        function f(t, e, n) {
            var r = t.map.id;
            t.error ? t.emit("error", t.error) : (e[r] = !0, each(t.depMaps, function (r, i) {
                var o = r.id, a = getOwn(C, o);
                !a || t.depMatched[i] || n[o] || (getOwn(e, o) ? (t.defineDep(i, S[o]), t.check()) : f(a, e, n))
            }), n[r] = !0)
        }

        function h() {
            var t, e, n, o, a = 1e3 * _.waitSeconds, s = a && w.startTime + a < (new Date).getTime(), u = [], d = [], l = !1, p = !0;
            if (!y) {
                if (y = !0, eachProp(C, function (n) {
                        if (t = n.map, e = t.id, n.enabled && (t.isDefine || d.push(n), !n.error))if (!n.inited && s)i(e) ? (o = !0, l = !0) : (u.push(e), r(e)); else if (!n.inited && n.fetched && t.isDefine && (l = !0, !t.prefix))return p = !1
                    }), s && u.length)return n = makeError("timeout", "Load timeout for modules: " + u, null, u), n.contextName = w.contextName, c(n);
                p && each(d, function (t) {
                    f(t, {}, {})
                }), s && !o || !l || !isBrowser && !isWebWorker || k || (k = setTimeout(function () {
                    k = 0, h()
                }, 50)), y = !1
            }
        }

        function p(t) {
            hasProp(S, t[0]) || s(a(t[0], null, !0)).init(t[1], t[2])
        }

        function m(t, e, n, r) {
            t.detachEvent && !isOpera ? r && t.detachEvent(r, e) : t.removeEventListener(n, e, !1)
        }

        function v(t) {
            var e = t.currentTarget || t.srcElement;
            return m(e, w.onScriptLoad, "load", "onreadystatechange"), m(e, w.onScriptError, "error"), {
                node: e,
                id: e && e.getAttribute("data-requiremodule")
            }
        }

        function g() {
            var t;
            for (d(); T.length;) {
                if (t = T.shift(), null === t[0])return c(makeError("mismatch", "Mismatched anonymous define() module: " + t[t.length - 1]));
                p(t)
            }
        }

        var y, b, w, x, k, _ = {
            waitSeconds: 7,
            baseUrl: "./",
            paths: {},
            pkgs: {},
            shim: {},
            map: {},
            config: {}
        }, C = {}, z = {}, T = [], S = {}, E = {}, $ = 1, O = 1;
        return x = {
            require: function (t) {
                return t.require ? t.require : t.require = w.makeRequire(t.map)
            }, exports: function (t) {
                return t.usingExports = !0, t.map.isDefine ? t.exports ? t.exports : t.exports = S[t.map.id] = {} : void 0
            }, module: function (t) {
                return t.module ? t.module : t.module = {
                    id: t.map.id, uri: t.map.url, config: function () {
                        return _.config && getOwn(_.config, t.map.id) || {}
                    }, exports: S[t.map.id]
                }
            }
        }, b = function (t) {
            this.events = getOwn(z, t.id) || {}, this.map = t, this.shim = getOwn(_.shim, t.id), this.depExports = [], this.depMaps = [], this.depMatched = [], this.pluginMaps = {}, this.depCount = 0
        }, b.prototype = {
            init: function (t, e, n, r) {
                r = r || {}, this.inited || (this.factory = e, n ? this.on("error", n) : this.events.error && (n = bind(this, function (t) {
                    this.emit("error", t)
                })), this.depMaps = t && t.slice(0), this.errback = n, this.inited = !0, this.ignore = r.ignore, r.enabled || this.enabled ? this.enable() : this.check())
            }, defineDep: function (t, e) {
                this.depMatched[t] || (this.depMatched[t] = !0, this.depCount -= 1, this.depExports[t] = e)
            }, fetch: function () {
                if (!this.fetched) {
                    this.fetched = !0, w.startTime = (new Date).getTime();
                    var t = this.map;
                    return this.shim ? (w.makeRequire(this.map, {enableBuildCallback: !0})(this.shim.deps || [], bind(this, function () {
                        return t.prefix ? this.callPlugin() : this.load()
                    })), void 0) : t.prefix ? this.callPlugin() : this.load()
                }
            }, load: function () {
                var t = this.map.url;
                E[t] || (E[t] = !0, w.load(this.map.id, t))
            }, check: function () {
                if (this.enabled && !this.enabling) {
                    var t, e, n = this.map.id, r = this.depExports, i = this.exports, o = this.factory;
                    if (this.inited) {
                        if (this.error)this.emit("error", this.error); else if (!this.defining) {
                            if (this.defining = !0, 1 > this.depCount && !this.defined) {
                                if (isFunction(o)) {
                                    if (this.events.error)try {
                                        i = w.execCb(n, o, r, i)
                                    } catch (a) {
                                        t = a
                                    } else i = w.execCb(n, o, r, i);
                                    if (this.map.isDefine && (e = this.module, e && void 0 !== e.exports && e.exports !== this.exports ? i = e.exports : void 0 === i && this.usingExports && (i = this.exports)), t)return t.requireMap = this.map, t.requireModules = [this.map.id], t.requireType = "define", c(this.error = t)
                                } else i = o;
                                this.exports = i, this.map.isDefine && !this.ignore && (S[n] = i, req.onResourceLoad && req.onResourceLoad(w, this.map, this.depMaps)), delete C[n], this.defined = !0
                            }
                            this.defining = !1, this.defined && !this.defineEmitted && (this.defineEmitted = !0, this.emit("defined", this.exports), this.defineEmitComplete = !0)
                        }
                    } else this.fetch()
                }
            }, callPlugin: function () {
                var t = this.map, e = t.id, r = a(t.prefix);
                this.depMaps.push(r), u(r, "defined", bind(this, function (r) {
                    var i, o, d, f = this.map.name, h = this.map.parentMap ? this.map.parentMap.name : null, p = w.makeRequire(t.parentMap, {enableBuildCallback: !0});
                    return this.map.unnormalized ? (r.normalize && (f = r.normalize(f, function (t) {
                            return n(t, h, !0)
                        }) || ""), o = a(t.prefix + "!" + f, this.map.parentMap), u(o, "defined", bind(this, function (t) {
                        this.init([], function () {
                            return t
                        }, null, {enabled: !0, ignore: !0})
                    })), d = getOwn(C, o.id), d && (this.depMaps.push(o), this.events.error && d.on("error", bind(this, function (t) {
                        this.emit("error", t)
                    })), d.enable()), void 0) : (i = bind(this, function (t) {
                        this.init([], function () {
                            return t
                        }, null, {enabled: !0})
                    }), i.error = bind(this, function (t) {
                        this.inited = !0, this.error = t, t.requireModules = [e], eachProp(C, function (t) {
                            0 === t.map.id.indexOf(e + "_unnormalized") && l(t.map.id)
                        }), c(t)
                    }), i.fromText = bind(this, function (n, r) {
                        var o = t.name, u = a(o), d = useInteractive;
                        r && (n = r), d && (useInteractive = !1), s(u), hasProp(_.config, e) && (_.config[o] = _.config[e]);
                        try {
                            req.exec(n)
                        } catch (l) {
                            return c(makeError("fromtexteval", "fromText eval for " + e + " failed: " + l, l, [e]))
                        }
                        d && (useInteractive = !0), this.depMaps.push(u), w.completeLoad(o), p([o], i)
                    }), r.load(t.name, p, i, _), void 0)
                })), w.enable(r, this), this.pluginMaps[r.id] = r
            }, enable: function () {
                this.enabled = !0, this.enabling = !0, each(this.depMaps, bind(this, function (t, e) {
                    var n, r, i;
                    if ("string" == typeof t) {
                        if (t = a(t, this.map.isDefine ? this.map : this.map.parentMap, !1, !this.skipMap), this.depMaps[e] = t, i = getOwn(x, t.id))return this.depExports[e] = i(this), void 0;
                        this.depCount += 1, u(t, "defined", bind(this, function (t) {
                            this.defineDep(e, t), this.check()
                        })), this.errback && u(t, "error", this.errback)
                    }
                    n = t.id, r = C[n], hasProp(x, n) || !r || r.enabled || w.enable(t, this)
                })), eachProp(this.pluginMaps, bind(this, function (t) {
                    var e = getOwn(C, t.id);
                    e && !e.enabled && w.enable(t, this)
                })), this.enabling = !1, this.check()
            }, on: function (t, e) {
                var n = this.events[t];
                n || (n = this.events[t] = []), n.push(e)
            }, emit: function (t, e) {
                each(this.events[t], function (t) {
                    t(e)
                }), "error" === t && delete this.events[t]
            }
        }, w = {
            config: _,
            contextName: t,
            registry: C,
            defined: S,
            urlFetched: E,
            defQueue: T,
            Module: b,
            makeModuleMap: a,
            nextTick: req.nextTick,
            configure: function (t) {
                t.baseUrl && "/" !== t.baseUrl.charAt(t.baseUrl.length - 1) && (t.baseUrl += "/");
                var e = _.pkgs, n = _.shim, r = {paths: !0, config: !0, map: !0};
                eachProp(t, function (t, e) {
                    r[e] ? "map" === e ? mixin(_[e], t, !0, !0) : mixin(_[e], t, !0) : _[e] = t
                }), t.shim && (eachProp(t.shim, function (t, e) {
                    isArray(t) && (t = {deps: t}), !t.exports && !t.init || t.exportsFn || (t.exportsFn = w.makeShimExports(t)), n[e] = t
                }), _.shim = n), t.packages && (each(t.packages, function (t) {
                    var n;
                    t = "string" == typeof t ? {name: t} : t, n = t.location, e[t.name] = {
                        name: t.name,
                        location: n || t.name,
                        main: (t.main || "main").replace(currDirRegExp, "").replace(jsSuffixRegExp, "")
                    }
                }), _.pkgs = e), eachProp(C, function (t, e) {
                    t.inited || t.map.unnormalized || (t.map = a(e))
                }), (t.deps || t.callback) && w.require(t.deps || [], t.callback)
            },
            makeShimExports: function (t) {
                function e() {
                    var e;
                    return t.init && (e = t.init.apply(global, arguments)), e || t.exports && getGlobal(t.exports)
                }

                return e
            },
            makeRequire: function (e, r) {
                function i(n, o, u) {
                    var d, l, f;
                    return r.enableBuildCallback && o && isFunction(o) && (o.__requireJsBuild = !0), "string" == typeof n ? isFunction(o) ? c(makeError("requireargs", "Invalid require call"), u) : e && hasProp(x, n) ? x[n](C[e.id]) : req.get ? req.get(w, n, e) : (l = a(n, e, !1, !0), d = l.id, hasProp(S, d) ? S[d] : c(makeError("notloaded", 'Module name "' + d + '" has not been loaded yet for context: ' + t + (e ? "" : ". Use require([])")))) : (g(), w.nextTick(function () {
                        g(), f = s(a(null, e)), f.skipMap = r.skipMap, f.init(n, o, u, {enabled: !0}), h()
                    }), i)
                }

                return r = r || {}, mixin(i, {
                    isBrowser: isBrowser, toUrl: function (t) {
                        var r, i, o = t.lastIndexOf("."), a = t.split("/")[0], s = "." === a || ".." === a;
                        return -1 !== o && (!s || o > 1) && (r = t.substring(o, t.length), t = t.substring(0, o)), i = w.nameToUrl(n(t, e && e.id, !0), r || ".fake"), r ? i : i.substring(0, i.length - 5)
                    }, defined: function (t) {
                        return hasProp(S, a(t, e, !1, !0).id)
                    }, specified: function (t) {
                        return t = a(t, e, !1, !0).id, hasProp(S, t) || hasProp(C, t)
                    }
                }), e || (i.undef = function (t) {
                    d();
                    var n = a(t, e, !0), r = getOwn(C, t);
                    delete S[t], delete E[n.url], delete z[t], r && (r.events.defined && (z[t] = r.events), l(t))
                }), i
            },
            enable: function (t) {
                var e = getOwn(C, t.id);
                e && s(t).enable()
            },
            completeLoad: function (t) {
                var e, n, r, o = getOwn(_.shim, t) || {}, a = o.exports;
                for (d(); T.length;) {
                    if (n = T.shift(), null === n[0]) {
                        if (n[0] = t, e)break;
                        e = !0
                    } else n[0] === t && (e = !0);
                    p(n)
                }
                if (r = getOwn(C, t), !e && !hasProp(S, t) && r && !r.inited) {
                    if (!(!_.enforceDefine || a && getGlobal(a)))return i(t) ? void 0 : c(makeError("nodefine", "No define call for " + t, null, [t]));
                    p([t, o.deps || [], o.exportsFn])
                }
                h()
            },
            nameToUrl: function (t, e) {
                var n, r, i, o, a, s, u, c, d;
                if (req.jsExtRegExp.test(t))c = t + (e || ""); else {
                    for (n = _.paths, r = _.pkgs, a = t.split("/"), s = a.length; s > 0; s -= 1) {
                        if (u = a.slice(0, s).join("/"), i = getOwn(r, u), d = getOwn(n, u)) {
                            isArray(d) && (d = d[0]), a.splice(0, s, d);
                            break
                        }
                        if (i) {
                            o = t === i.name ? i.location + "/" + i.main : i.location, a.splice(0, s, o);
                            break
                        }
                    }
                    c = a.join("/"), c += e || (/\?/.test(c) ? "" : ".js"), c = ("/" === c.charAt(0) || c.match(/^[\w\+\.\-]+:/) ? "" : _.baseUrl) + c
                }
                return _.urlArgs ? c + ((-1 === c.indexOf("?") ? "?" : "&") + _.urlArgs) : c
            },
            load: function (t, e) {
                req.load(w, t, e)
            },
            execCb: function (t, e, n, r) {
                return e.apply(r, n)
            },
            onScriptLoad: function (t) {
                if ("load" === t.type || readyRegExp.test((t.currentTarget || t.srcElement).readyState)) {
                    interactiveScript = null;
                    var e = v(t);
                    w.completeLoad(e.id)
                }
            },
            onScriptError: function (t) {
                var e = v(t);
                return i(e.id) ? void 0 : c(makeError("scripterror", "Script error", t, [e.id]))
            }
        }, w.require = w.makeRequire(), w
    }

    function getInteractiveScript() {
        return interactiveScript && "interactive" === interactiveScript.readyState ? interactiveScript : (eachReverse(scripts(), function (t) {
            return "interactive" === t.readyState ? interactiveScript = t : void 0
        }), interactiveScript)
    }

    var req, s, head, baseElement, dataMain, src, interactiveScript, currentlyAddingScript, mainScript, subPath, version = "2.1.4", commentRegExp = /(\/\*([\s\S]*?)\*\/|([^:]|^)\/\/(.*)$)/gm, cjsRequireRegExp = /[^.]\s*require\s*\(\s*["']([^'"\s]+)["']\s*\)/g, jsSuffixRegExp = /\.js$/, currDirRegExp = /^\.\//, op = Object.prototype, ostring = op.toString, hasOwn = op.hasOwnProperty, ap = Array.prototype, apsp = ap.splice, isBrowser = !("undefined" == typeof window || !navigator || !document), isWebWorker = !isBrowser && "undefined" != typeof importScripts, readyRegExp = isBrowser && "PLAYSTATION 3" === navigator.platform ? /^complete$/ : /^(complete|loaded)$/, defContextName = "_", isOpera = "undefined" != typeof opera && "[object Opera]" == "" + opera, contexts = {}, cfg = {}, globalDefQueue = [], useInteractive = !1;
    if (void 0 === define) {
        if (requirejs !== void 0) {
            if (isFunction(requirejs))return;
            cfg = requirejs, requirejs = void 0
        }
        void 0 === require || isFunction(require) || (cfg = require, require = void 0), req = requirejs = function (t, e, n, r) {
            var i, o, a = defContextName;
            return isArray(t) || "string" == typeof t || (o = t, isArray(e) ? (t = e, e = n, n = r) : t = []), o && o.context && (a = o.context), i = getOwn(contexts, a), i || (i = contexts[a] = req.s.newContext(a)), o && i.configure(o), i.require(t, e, n)
        }, req.config = function (t) {
            return req(t)
        }, req.nextTick = "undefined" != typeof setTimeout ? function (t) {
            setTimeout(t, 4)
        } : function (t) {
            t()
        }, require || (require = req), req.version = version, req.jsExtRegExp = /^\/|:|\?|\.js$/, req.isBrowser = isBrowser, s = req.s = {
            contexts: contexts,
            newContext: newContext
        }, req({}), each(["toUrl", "undef", "defined", "specified"], function (t) {
            req[t] = function () {
                var e = contexts[defContextName];
                return e.require[t].apply(e, arguments)
            }
        }), isBrowser && (head = s.head = document.getElementsByTagName("head")[0], baseElement = document.getElementsByTagName("base")[0], baseElement && (head = s.head = baseElement.parentNode)), req.onError = function (t) {
            throw t
        }, req.load = function (t, e, n) {
            var r, i = t && t.config || {};
            return isBrowser ? (r = i.xhtml ? document.createElementNS("http://www.w3.org/1999/xhtml", "html:script") : document.createElement("script"), r.type = i.scriptType || "text/javascript", r.charset = "utf-8", r.async = !0, r.setAttribute("data-requirecontext", t.contextName), r.setAttribute("data-requiremodule", e), !r.attachEvent || r.attachEvent.toString && 0 > ("" + r.attachEvent).indexOf("[native code") || isOpera ? (r.addEventListener("load", t.onScriptLoad, !1), r.addEventListener("error", t.onScriptError, !1)) : (useInteractive = !0, r.attachEvent("onreadystatechange", t.onScriptLoad)), r.src = n, currentlyAddingScript = r, baseElement ? head.insertBefore(r, baseElement) : head.appendChild(r), currentlyAddingScript = null, r) : (isWebWorker && (importScripts(n), t.completeLoad(e)), void 0)
        }, isBrowser && eachReverse(scripts(), function (t) {
            return head || (head = t.parentNode), dataMain = t.getAttribute("data-main"), dataMain ? (cfg.baseUrl || (src = dataMain.split("/"), mainScript = src.pop(), subPath = src.length ? src.join("/") + "/" : "./", cfg.baseUrl = subPath, dataMain = mainScript), dataMain = dataMain.replace(jsSuffixRegExp, ""), cfg.deps = cfg.deps ? cfg.deps.concat(dataMain) : [dataMain], !0) : void 0
        }), define = function (t, e, n) {
            var r, i;
            "string" != typeof t && (n = e, e = t, t = null), isArray(e) || (n = e, e = []), !e.length && isFunction(n) && n.length && (("" + n).replace(commentRegExp, "").replace(cjsRequireRegExp, function (t, n) {
                e.push(n)
            }), e = (1 === n.length ? ["require"] : ["require", "exports", "module"]).concat(e)), useInteractive && (r = currentlyAddingScript || getInteractiveScript(), r && (t || (t = r.getAttribute("data-requiremodule")), i = contexts[r.getAttribute("data-requirecontext")])), (i ? i.defQueue : globalDefQueue).push([t, e, n])
        }, define.amd = {jQuery: !0}, req.exec = function (text) {
            return eval(text)
        }, req(cfg)
    }
})(this), define("requireLib", function () {
}), function (t) {
    String.prototype.trim === t && (String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "")
    }), Array.prototype.reduce === t && (Array.prototype.reduce = function (e) {
        if (this === void 0 || null === this)throw new TypeError;
        var n, r = Object(this), i = r.length >>> 0, o = 0;
        if ("function" != typeof e)throw new TypeError;
        if (0 == i && 1 == arguments.length)throw new TypeError;
        if (arguments.length >= 2)n = arguments[1]; else for (; ;) {
            if (o in r) {
                n = r[o++];
                break
            }
            if (++o >= i)throw new TypeError
        }
        for (; i > o;)o in r && (n = e.call(t, n, r[o], o, r)), o++;
        return n
    })
}();
var Zepto = function () {
    function t(t) {
        return null == t ? t + "" : Y[Q.call(t)] || "object"
    }

    function e(e) {
        return "function" == t(e)
    }

    function n(t) {
        return null != t && t == t.window
    }

    function r(t) {
        return null != t && t.nodeType == t.DOCUMENT_NODE
    }

    function i(e) {
        return "object" == t(e)
    }

    function o(t) {
        return i(t) && !n(t) && t.__proto__ == Object.prototype
    }

    function a(t) {
        return t instanceof Array
    }

    function s(t) {
        return "number" == typeof t.length
    }

    function u(t) {
        return O.call(t, function (t) {
            return null != t
        })
    }

    function c(t) {
        return t.length > 0 ? C.fn.concat.apply([], t) : t
    }

    function d(t) {
        return t.replace(/::/g, "/").replace(/([A-Z]+)([A-Z][a-z])/g, "$1_$2").replace(/([a-z\d])([A-Z])/g, "$1_$2").replace(/_/g, "-").toLowerCase()
    }

    function l(t) {
        return t in q ? q[t] : q[t] = RegExp("(^|\\s)" + t + "(\\s|$)")
    }

    function f(t, e) {
        return "number" != typeof e || N[d(t)] ? e : e + "px"
    }

    function h(t) {
        var e, n;
        return P[t] || (e = A.createElement(t), A.body.appendChild(e), n = j(e, "").getPropertyValue("display"), e.parentNode.removeChild(e), "none" == n && (n = "block"), P[t] = n), P[t]
    }

    function p(t) {
        return "children"in t ? $.call(t.children) : C.map(t.childNodes, function (t) {
            return 1 == t.nodeType ? t : k
        })
    }

    function m(t, e, n) {
        for (_ in e)n && (o(e[_]) || a(e[_])) ? (o(e[_]) && !o(t[_]) && (t[_] = {}), a(e[_]) && !a(t[_]) && (t[_] = []), m(t[_], e[_], n)) : e[_] !== k && (t[_] = e[_])
    }

    function v(t, e) {
        return e === k ? C(t) : C(t).filter(e)
    }

    function g(t, n, r, i) {
        return e(n) ? n.call(t, r, i) : n
    }

    function y(t, e, n) {
        null == n ? t.removeAttribute(e) : t.setAttribute(e, n)
    }

    function b(t, e) {
        var n = t.className, r = n && n.baseVal !== k;
        return e === k ? r ? n.baseVal : n : (r ? n.baseVal = e : t.className = e, k)
    }

    function w(t) {
        var e;
        try {
            return t ? "true" == t || ("false" == t ? !1 : "null" == t ? null : isNaN(e = Number(t)) ? /^[\[\{]/.test(t) ? C.parseJSON(t) : t : e) : t
        } catch (n) {
            return t
        }
    }

    function x(t, e) {
        e(t);
        for (var n in t.childNodes)x(t.childNodes[n], e)
    }

    var k, _, C, z, T, S, E = [], $ = E.slice, O = E.filter, A = window.document, P = {}, q = {}, j = A.defaultView.getComputedStyle, N = {
        "column-count": 1,
        columns: 1,
        "font-weight": 1,
        "line-height": 1,
        opacity: 1,
        "z-index": 1,
        zoom: 1
    }, M = /^\s*<(\w+|!)[^>]*>/, R = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, D = /^(?:body|html)$/i, L = ["val", "css", "html", "text", "data", "width", "height", "offset"], I = ["after", "prepend", "before", "append"], F = A.createElement("table"), Z = A.createElement("tr"), B = {
        tr: A.createElement("tbody"),
        tbody: F,
        thead: F,
        tfoot: F,
        td: Z,
        th: Z,
        "*": A.createElement("div")
    }, U = /complete|loaded|interactive/, V = /^\.([\w-]+)$/, W = /^#([\w-]*)$/, H = /^[\w-]+$/, Y = {}, Q = Y.toString, X = {}, G = A.createElement("div");
    return X.matches = function (t, e) {
        if (!t || 1 !== t.nodeType)return !1;
        var n = t.webkitMatchesSelector || t.mozMatchesSelector || t.oMatchesSelector || t.matchesSelector;
        if (n)return n.call(t, e);
        var r, i = t.parentNode, o = !i;
        return o && (i = G).appendChild(t), r = ~X.qsa(i, e).indexOf(t), o && G.removeChild(t), r
    }, T = function (t) {
        return t.replace(/-+(.)?/g, function (t, e) {
            return e ? e.toUpperCase() : ""
        })
    }, S = function (t) {
        return O.call(t, function (e, n) {
            return t.indexOf(e) == n
        })
    }, X.fragment = function (t, e, n) {
        t.replace && (t = t.replace(R, "<$1></$2>")), e === k && (e = M.test(t) && RegExp.$1), e in B || (e = "*");
        var r, i, a = B[e];
        return a.innerHTML = "" + t, i = C.each($.call(a.childNodes), function () {
            a.removeChild(this)
        }), o(n) && (r = C(i), C.each(n, function (t, e) {
            L.indexOf(t) > -1 ? r[t](e) : r.attr(t, e)
        })), i
    }, X.Z = function (t, e) {
        return t = t || [], t.__proto__ = C.fn, t.selector = e || "", t
    }, X.isZ = function (t) {
        return t instanceof X.Z
    }, X.init = function (t, n) {
        if (t) {
            if (e(t))return C(A).ready(t);
            if (X.isZ(t))return t;
            var r;
            if (a(t))r = u(t); else if (i(t))r = [o(t) ? C.extend({}, t) : t], t = null; else if (M.test(t))r = X.fragment(t.trim(), RegExp.$1, n), t = null; else {
                if (n !== k)return C(n).find(t);
                r = X.qsa(A, t)
            }
            return X.Z(r, t)
        }
        return X.Z()
    }, C = function (t, e) {
        return X.init(t, e)
    }, C.extend = function (t) {
        var e, n = $.call(arguments, 1);
        return "boolean" == typeof t && (e = t, t = n.shift()), n.forEach(function (n) {
            m(t, n, e)
        }), t
    }, X.qsa = function (t, e) {
        var n;
        return r(t) && W.test(e) ? (n = t.getElementById(RegExp.$1)) ? [n] : [] : 1 !== t.nodeType && 9 !== t.nodeType ? [] : $.call(V.test(e) ? t.getElementsByClassName(RegExp.$1) : H.test(e) ? t.getElementsByTagName(e) : t.querySelectorAll(e))
    }, C.contains = function (t, e) {
        return t !== e && t.contains(e)
    }, C.type = t, C.isFunction = e, C.isWindow = n, C.isArray = a, C.isPlainObject = o, C.isEmptyObject = function (t) {
        var e;
        for (e in t)return !1;
        return !0
    }, C.inArray = function (t, e, n) {
        return E.indexOf.call(e, t, n)
    }, C.camelCase = T, C.trim = function (t) {
        return t.trim()
    }, C.uuid = 0, C.support = {}, C.expr = {}, C.map = function (t, e) {
        var n, r, i, o = [];
        if (s(t))for (r = 0; t.length > r; r++)n = e(t[r], r), null != n && o.push(n); else for (i in t)n = e(t[i], i), null != n && o.push(n);
        return c(o)
    }, C.each = function (t, e) {
        var n, r;
        if (s(t)) {
            for (n = 0; t.length > n; n++)if (e.call(t[n], n, t[n]) === !1)return t
        } else for (r in t)if (e.call(t[r], r, t[r]) === !1)return t;
        return t
    }, C.grep = function (t, e) {
        return O.call(t, e)
    }, window.JSON && (C.parseJSON = JSON.parse), C.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (t, e) {
        Y["[object " + e + "]"] = e.toLowerCase()
    }), C.fn = {
        forEach: E.forEach,
        reduce: E.reduce,
        push: E.push,
        sort: E.sort,
        indexOf: E.indexOf,
        concat: E.concat,
        map: function (t) {
            return C(C.map(this, function (e, n) {
                return t.call(e, n, e)
            }))
        },
        slice: function () {
            return C($.apply(this, arguments))
        },
        ready: function (t) {
            return U.test(A.readyState) ? t(C) : A.addEventListener("DOMContentLoaded", function () {
                t(C)
            }, !1), this
        },
        get: function (t) {
            return t === k ? $.call(this) : this[t >= 0 ? t : t + this.length]
        },
        toArray: function () {
            return this.get()
        },
        size: function () {
            return this.length
        },
        remove: function () {
            return this.each(function () {
                null != this.parentNode && this.parentNode.removeChild(this)
            })
        },
        each: function (t) {
            return E.every.call(this, function (e, n) {
                return t.call(e, n, e) !== !1
            }), this
        },
        filter: function (t) {
            return e(t) ? this.not(this.not(t)) : C(O.call(this, function (e) {
                return X.matches(e, t)
            }))
        },
        add: function (t, e) {
            return C(S(this.concat(C(t, e))))
        },
        is: function (t) {
            return this.length > 0 && X.matches(this[0], t)
        },
        not: function (t) {
            var n = [];
            if (e(t) && t.call !== k)this.each(function (e) {
                t.call(this, e) || n.push(this)
            }); else {
                var r = "string" == typeof t ? this.filter(t) : s(t) && e(t.item) ? $.call(t) : C(t);
                this.forEach(function (t) {
                    0 > r.indexOf(t) && n.push(t)
                })
            }
            return C(n)
        },
        has: function (t) {
            return this.filter(function () {
                return i(t) ? C.contains(this, t) : C(this).find(t).size()
            })
        },
        eq: function (t) {
            return -1 === t ? this.slice(t) : this.slice(t, +t + 1)
        },
        first: function () {
            var t = this[0];
            return t && !i(t) ? t : C(t)
        },
        last: function () {
            var t = this[this.length - 1];
            return t && !i(t) ? t : C(t)
        },
        find: function (t) {
            var e, n = this;
            return e = "object" == typeof t ? C(t).filter(function () {
                var t = this;
                return E.some.call(n, function (e) {
                    return C.contains(e, t)
                })
            }) : 1 == this.length ? C(X.qsa(this[0], t)) : this.map(function () {
                return X.qsa(this, t)
            })
        },
        closest: function (t, e) {
            var n = this[0], i = !1;
            for ("object" == typeof t && (i = C(t)); n && !(i ? i.indexOf(n) >= 0 : X.matches(n, t));)n = n !== e && !r(n) && n.parentNode;
            return C(n)
        },
        parents: function (t) {
            for (var e = [], n = this; n.length > 0;)n = C.map(n, function (t) {
                return (t = t.parentNode) && !r(t) && 0 > e.indexOf(t) ? (e.push(t), t) : k
            });
            return v(e, t)
        },
        parent: function (t) {
            return v(S(this.pluck("parentNode")), t)
        },
        children: function (t) {
            return v(this.map(function () {
                return p(this)
            }), t)
        },
        contents: function () {
            return this.map(function () {
                return $.call(this.childNodes)
            })
        },
        siblings: function (t) {
            return v(this.map(function (t, e) {
                return O.call(p(e.parentNode), function (t) {
                    return t !== e
                })
            }), t)
        },
        empty: function () {
            return this.each(function () {
                this.innerHTML = ""
            })
        },
        pluck: function (t) {
            return C.map(this, function (e) {
                return e[t]
            })
        },
        show: function () {
            return this.each(function () {
                "none" == this.style.display && (this.style.display = null), "none" == j(this, "").getPropertyValue("display") && (this.style.display = h(this.nodeName))
            })
        },
        replaceWith: function (t) {
            return this.before(t).remove()
        },
        wrap: function (t) {
            var n = e(t);
            if (this[0] && !n)var r = C(t).get(0), i = r.parentNode || this.length > 1;
            return this.each(function (e) {
                C(this).wrapAll(n ? t.call(this, e) : i ? r.cloneNode(!0) : r)
            })
        },
        wrapAll: function (t) {
            if (this[0]) {
                C(this[0]).before(t = C(t));
                for (var e; (e = t.children()).length;)t = e.first();
                C(t).append(this)
            }
            return this
        },
        wrapInner: function (t) {
            var n = e(t);
            return this.each(function (e) {
                var r = C(this), i = r.contents(), o = n ? t.call(this, e) : t;
                i.length ? i.wrapAll(o) : r.append(o)
            })
        },
        unwrap: function () {
            return this.parent().each(function () {
                C(this).replaceWith(C(this).children())
            }), this
        },
        clone: function () {
            return this.map(function () {
                return this.cloneNode(!0)
            })
        },
        hide: function () {
            return this.css("display", "none")
        },
        toggle: function (t) {
            return this.each(function () {
                var e = C(this);
                (t === k ? "none" == e.css("display") : t) ? e.show() : e.hide()
            })
        },
        prev: function (t) {
            return C(this.pluck("previousElementSibling")).filter(t || "*")
        },
        next: function (t) {
            return C(this.pluck("nextElementSibling")).filter(t || "*")
        },
        html: function (t) {
            return t === k ? this.length > 0 ? this[0].innerHTML : null : this.each(function (e) {
                var n = this.innerHTML;
                C(this).empty().append(g(this, t, e, n))
            })
        },
        text: function (t) {
            return t === k ? this.length > 0 ? this[0].textContent : null : this.each(function () {
                this.textContent = t
            })
        },
        attr: function (t, e) {
            var n;
            return "string" == typeof t && e === k ? 0 == this.length || 1 !== this[0].nodeType ? k : "value" == t && "INPUT" == this[0].nodeName ? this.val() : !(n = this[0].getAttribute(t)) && t in this[0] ? this[0][t] : n : this.each(function (n) {
                if (1 === this.nodeType)if (i(t))for (_ in t)y(this, _, t[_]); else y(this, t, g(this, e, n, this.getAttribute(t)))
            })
        },
        removeAttr: function (t) {
            return this.each(function () {
                1 === this.nodeType && y(this, t)
            })
        },
        prop: function (t, e) {
            return e === k ? this[0] && this[0][t] : this.each(function (n) {
                this[t] = g(this, e, n, this[t])
            })
        },
        data: function (t, e) {
            var n = this.attr("data-" + d(t), e);
            return null !== n ? w(n) : k
        },
        val: function (t) {
            return t === k ? this[0] && (this[0].multiple ? C(this[0]).find("option").filter(function () {
                return this.selected
            }).pluck("value") : this[0].value) : this.each(function (e) {
                this.value = g(this, t, e, this.value)
            })
        },
        offset: function (t) {
            if (t)return this.each(function (e) {
                var n = C(this), r = g(this, t, e, n.offset()), i = n.offsetParent().offset(), o = {
                    top: r.top - i.top,
                    left: r.left - i.left
                };
                "static" == n.css("position") && (o.position = "relative"), n.css(o)
            });
            if (0 == this.length)return null;
            var e = this[0].getBoundingClientRect();
            return {
                left: e.left + window.pageXOffset,
                top: e.top + window.pageYOffset,
                width: Math.round(e.width),
                height: Math.round(e.height)
            }
        },
        css: function (e, n) {
            if (2 > arguments.length && "string" == typeof e)return this[0] && (this[0].style[T(e)] || j(this[0], "").getPropertyValue(e));
            var r = "";
            if ("string" == t(e))n || 0 === n ? r = d(e) + ":" + f(e, n) : this.each(function () {
                this.style.removeProperty(d(e))
            }); else for (_ in e)e[_] || 0 === e[_] ? r += d(_) + ":" + f(_, e[_]) + ";" : this.each(function () {
                this.style.removeProperty(d(_))
            });
            return this.each(function () {
                this.style.cssText += ";" + r
            })
        },
        index: function (t) {
            return t ? this.indexOf(C(t)[0]) : this.parent().children().indexOf(this[0])
        },
        hasClass: function (t) {
            return E.some.call(this, function (t) {
                return this.test(b(t))
            }, l(t))
        },
        addClass: function (t) {
            return this.each(function (e) {
                z = [];
                var n = b(this), r = g(this, t, e, n);
                r.split(/\s+/g).forEach(function (t) {
                    C(this).hasClass(t) || z.push(t)
                }, this), z.length && b(this, n + (n ? " " : "") + z.join(" "))
            })
        },
        removeClass: function (t) {
            return this.each(function (e) {
                return t === k ? b(this, "") : (z = b(this), g(this, t, e, z).split(/\s+/g).forEach(function (t) {
                    z = z.replace(l(t), " ")
                }), b(this, z.trim()), k)
            })
        },
        toggleClass: function (t, e) {
            return this.each(function (n) {
                var r = C(this), i = g(this, t, n, b(this));
                i.split(/\s+/g).forEach(function (t) {
                    (e === k ? !r.hasClass(t) : e) ? r.addClass(t) : r.removeClass(t)
                })
            })
        },
        scrollTop: function () {
            return this.length ? "scrollTop"in this[0] ? this[0].scrollTop : this[0].scrollY : k
        },
        position: function () {
            if (this.length) {
                var t = this[0], e = this.offsetParent(), n = this.offset(), r = D.test(e[0].nodeName) ? {
                    top: 0,
                    left: 0
                } : e.offset();
                return n.top -= parseFloat(C(t).css("margin-top")) || 0, n.left -= parseFloat(C(t).css("margin-left")) || 0, r.top += parseFloat(C(e[0]).css("border-top-width")) || 0, r.left += parseFloat(C(e[0]).css("border-left-width")) || 0, {
                    top: n.top - r.top,
                    left: n.left - r.left
                }
            }
        },
        offsetParent: function () {
            return this.map(function () {
                for (var t = this.offsetParent || A.body; t && !D.test(t.nodeName) && "static" == C(t).css("position");)t = t.offsetParent;
                return t
            })
        }
    }, C.fn.detach = C.fn.remove, ["width", "height"].forEach(function (t) {
        C.fn[t] = function (e) {
            var i, o = this[0], a = t.replace(/./, function (t) {
                return t[0].toUpperCase()
            });
            return e === k ? n(o) ? o["inner" + a] : r(o) ? o.documentElement["offset" + a] : (i = this.offset()) && i[t] : this.each(function (n) {
                o = C(this), o.css(t, g(this, e, n, o[t]()))
            })
        }
    }), I.forEach(function (e, n) {
        var r = n % 2;
        C.fn[e] = function () {
            var e, i, o = C.map(arguments, function (n) {
                return e = t(n), "object" == e || "array" == e || null == n ? n : X.fragment(n)
            }), a = this.length > 1;
            return 1 > o.length ? this : this.each(function (t, e) {
                i = r ? e : e.parentNode, e = 0 == n ? e.nextSibling : 1 == n ? e.firstChild : 2 == n ? e : null, o.forEach(function (t) {
                    if (a)t = t.cloneNode(!0); else if (!i)return C(t).remove();
                    x(i.insertBefore(t, e), function (t) {
                        null == t.nodeName || "SCRIPT" !== t.nodeName.toUpperCase() || t.type && "text/javascript" !== t.type || t.src || window.eval.call(window, t.innerHTML)
                    })
                })
            })
        }, C.fn[r ? e + "To" : "insert" + (n ? "Before" : "After")] = function (t) {
            return C(t)[e](this), this
        }
    }), X.Z.prototype = C.fn, X.uniq = S, X.deserializeValue = w, C.zepto = X, C
}();
window.Zepto = Zepto, "$"in window || (window.$ = Zepto), function (t) {
    function e(t) {
        var e = this.os = {}, n = this.browser = {}, r = t.match(/WebKit\/([\d.]+)/), i = t.match(/(Android)\s+([\d.]+)/), o = t.match(/(iPad).*OS\s([\d_]+)/), a = !o && t.match(/(iPhone\sOS)\s([\d_]+)/), s = t.match(/(webOS|hpwOS)[\s\/]([\d.]+)/), u = s && t.match(/TouchPad/), c = t.match(/Kindle\/([\d.]+)/), d = t.match(/Silk\/([\d._]+)/), l = t.match(/(BlackBerry).*Version\/([\d.]+)/), f = t.match(/(BB10).*Version\/([\d.]+)/), h = t.match(/(RIM\sTablet\sOS)\s([\d.]+)/), p = t.match(/PlayBook/), m = t.match(/Chrome\/([\d.]+)/) || t.match(/CriOS\/([\d.]+)/), v = t.match(/Firefox\/([\d.]+)/);
        (n.webkit = !!r) && (n.version = r[1]), i && (e.android = !0, e.version = i[2]), a && (e.ios = e.iphone = !0, e.version = a[2].replace(/_/g, ".")), o && (e.ios = e.ipad = !0, e.version = o[2].replace(/_/g, ".")), s && (e.webos = !0, e.version = s[2]), u && (e.touchpad = !0), l && (e.blackberry = !0, e.version = l[2]), f && (e.bb10 = !0, e.version = f[2]), h && (e.rimtabletos = !0, e.version = h[2]), p && (n.playbook = !0), c && (e.kindle = !0, e.version = c[1]), d && (n.silk = !0, n.version = d[1]), !d && e.android && t.match(/Kindle Fire/) && (n.silk = !0), m && (n.chrome = !0, n.version = m[1]), v && (n.firefox = !0, n.version = v[1]), e.tablet = !!(o || p || i && !t.match(/Mobile/) || v && t.match(/Tablet/)), e.phone = !(e.tablet || !(i || a || s || l || f || m && t.match(/Android/) || m && t.match(/CriOS\/([\d.]+)/) || v && t.match(/Mobile/)))
    }

    e.call(t, navigator.userAgent), t.__detect = e
}(Zepto), function (t) {
    function e(t) {
        return t._zid || (t._zid = h++)
    }

    function n(t, n, o, a) {
        if (n = r(n), n.ns)var s = i(n.ns);
        return (f[e(t)] || []).filter(function (t) {
            return !(!t || n.e && t.e != n.e || n.ns && !s.test(t.ns) || o && e(t.fn) !== e(o) || a && t.sel != a)
        })
    }

    function r(t) {
        var e = ("" + t).split(".");
        return {e: e[0], ns: e.slice(1).sort().join(" ")}
    }

    function i(t) {
        return RegExp("(?:^| )" + t.replace(" ", " .* ?") + "(?: |$)")
    }

    function o(e, n, r) {
        "string" != t.type(e) ? t.each(e, r) : e.split(/\s/).forEach(function (t) {
            r(t, n)
        })
    }

    function a(t, e) {
        return t.del && ("focus" == t.e || "blur" == t.e) || !!e
    }

    function s(t) {
        return m[t] || t
    }

    function u(n, i, u, c, d, l) {
        var h = e(n), p = f[h] || (f[h] = []);
        o(i, u, function (e, i) {
            var o = r(e);
            o.fn = i, o.sel = c, o.e in m && (i = function (e) {
                var n = e.relatedTarget;
                return !n || n !== this && !t.contains(this, n) ? o.fn.apply(this, arguments) : void 0
            }), o.del = d && d(i, e);
            var u = o.del || i;
            o.proxy = function (t) {
                var e = u.apply(n, [t].concat(t.data));
                return e === !1 && (t.preventDefault(), t.stopPropagation()), e
            }, o.i = p.length, p.push(o), n.addEventListener(s(o.e), o.proxy, a(o, l))
        })
    }

    function c(t, r, i, u, c) {
        var d = e(t);
        o(r || "", i, function (e, r) {
            n(t, e, r, u).forEach(function (e) {
                delete f[d][e.i], t.removeEventListener(s(e.e), e.proxy, a(e, c))
            })
        })
    }

    function d(e) {
        var n, r = {originalEvent: e};
        for (n in e)y.test(n) || void 0 === e[n] || (r[n] = e[n]);
        return t.each(b, function (t, n) {
            r[t] = function () {
                return this[n] = v, e[t].apply(e, arguments)
            }, r[n] = g
        }), r
    }

    function l(t) {
        if (!("defaultPrevented"in t)) {
            t.defaultPrevented = !1;
            var e = t.preventDefault;
            t.preventDefault = function () {
                this.defaultPrevented = !0, e.call(this)
            }
        }
    }

    var f = (t.zepto.qsa, {}), h = 1, p = {}, m = {mouseenter: "mouseover", mouseleave: "mouseout"};
    p.click = p.mousedown = p.mouseup = p.mousemove = "MouseEvents", t.event = {
        add: u,
        remove: c
    }, t.proxy = function (n, r) {
        if (t.isFunction(n)) {
            var i = function () {
                return n.apply(r, arguments)
            };
            return i._zid = e(n), i
        }
        if ("string" == typeof r)return t.proxy(n[r], n);
        throw new TypeError("expected function")
    }, t.fn.bind = function (t, e) {
        return this.each(function () {
            u(this, t, e)
        })
    }, t.fn.unbind = function (t, e) {
        return this.each(function () {
            c(this, t, e)
        })
    }, t.fn.one = function (t, e) {
        return this.each(function (n, r) {
            u(this, t, e, null, function (t, e) {
                return function () {
                    var n = t.apply(r, arguments);
                    return c(r, e, t), n
                }
            })
        })
    };
    var v = function () {
        return !0
    }, g = function () {
        return !1
    }, y = /^([A-Z]|layer[XY]$)/, b = {
        preventDefault: "isDefaultPrevented",
        stopImmediatePropagation: "isImmediatePropagationStopped",
        stopPropagation: "isPropagationStopped"
    };
    t.fn.delegate = function (e, n, r) {
        return this.each(function (i, o) {
            u(o, n, r, e, function (n) {
                return function (r) {
                    var i, a = t(r.target).closest(e, o).get(0);
                    return a ? (i = t.extend(d(r), {
                        currentTarget: a,
                        liveFired: o
                    }), n.apply(a, [i].concat([].slice.call(arguments, 1)))) : void 0
                }
            })
        })
    }, t.fn.undelegate = function (t, e, n) {
        return this.each(function () {
            c(this, e, n, t)
        })
    }, t.fn.live = function (e, n) {
        return t(document.body).delegate(this.selector, e, n), this
    }, t.fn.die = function (e, n) {
        return t(document.body).undelegate(this.selector, e, n), this
    }, t.fn.on = function (e, n, r) {
        return !n || t.isFunction(n) ? this.bind(e, n || r) : this.delegate(n, e, r)
    }, t.fn.off = function (e, n, r) {
        return !n || t.isFunction(n) ? this.unbind(e, n || r) : this.undelegate(n, e, r)
    }, t.fn.trigger = function (e, n) {
        return ("string" == typeof e || t.isPlainObject(e)) && (e = t.Event(e)), l(e), e.data = n, this.each(function () {
            "dispatchEvent"in this && this.dispatchEvent(e)
        })
    }, t.fn.triggerHandler = function (e, r) {
        var i, o;
        return this.each(function (a, s) {
            i = d("string" == typeof e ? t.Event(e) : e), i.data = r, i.target = s, t.each(n(s, e.type || e), function (t, e) {
                return o = e.proxy(i), i.isImmediatePropagationStopped() ? !1 : void 0
            })
        }), o
    }, "focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select keydown keypress keyup error".split(" ").forEach(function (e) {
        t.fn[e] = function (t) {
            return t ? this.bind(e, t) : this.trigger(e)
        }
    }), ["focus", "blur"].forEach(function (e) {
        t.fn[e] = function (t) {
            return t ? this.bind(e, t) : this.each(function () {
                try {
                    this[e]()
                } catch (t) {
                }
            }), this
        }
    }), t.Event = function (t, e) {
        "string" != typeof t && (e = t, t = e.type);
        var n = document.createEvent(p[t] || "Events"), r = !0;
        if (e)for (var i in e)"bubbles" == i ? r = !!e[i] : n[i] = e[i];
        return n.initEvent(t, r, !0, null, null, null, null, null, null, null, null, null, null, null, null), n.isDefaultPrevented = function () {
            return this.defaultPrevented
        }, n
    }
}(Zepto), function (t) {
    function e(e, n, r) {
        var i = t.Event(n);
        return t(e).trigger(i, r), !i.defaultPrevented
    }

    function n(t, n, r, i) {
        return t.global ? e(n || y, r, i) : void 0
    }

    function r(e) {
        e.global && 0 === t.active++ && n(e, null, "ajaxStart")
    }

    function i(e) {
        e.global && !--t.active && n(e, null, "ajaxStop")
    }

    function o(t, e) {
        var r = e.context;
        return e.beforeSend.call(r, t, e) === !1 || n(e, r, "ajaxBeforeSend", [t, e]) === !1 ? !1 : (n(e, r, "ajaxSend", [t, e]), void 0)
    }

    function a(t, e, r) {
        var i = r.context, o = "success";
        r.success.call(i, t, o, e), n(r, i, "ajaxSuccess", [e, r, t]), u(o, e, r)
    }

    function s(t, e, r, i) {
        var o = i.context;
        i.error.call(o, r, e, t), n(i, o, "ajaxError", [r, i, t]), u(e, r, i)
    }

    function u(t, e, r) {
        var o = r.context;
        r.complete.call(o, e, t), n(r, o, "ajaxComplete", [e, r]), i(r)
    }

    function c() {
    }

    function d(t) {
        return t && (t = t.split(";", 2)[0]), t && (t == _ ? "html" : t == k ? "json" : w.test(t) ? "script" : x.test(t) && "xml") || "text"
    }

    function l(t, e) {
        return (t + "&" + e).replace(/[&?]{1,2}/, "?")
    }

    function f(e) {
        e.processData && e.data && "string" != t.type(e.data) && (e.data = t.param(e.data, e.traditional)), !e.data || e.type && "GET" != e.type.toUpperCase() || (e.url = l(e.url, e.data))
    }

    function h(e, n, r, i) {
        var o = !t.isFunction(n);
        return {url: e, data: o ? n : void 0, success: o ? t.isFunction(r) ? r : void 0 : n, dataType: o ? i || r : r}
    }

    function p(e, n, r, i) {
        var o, a = t.isArray(n);
        t.each(n, function (n, s) {
            o = t.type(s), i && (n = r ? i : i + "[" + (a ? "" : n) + "]"), !i && a ? e.add(s.name, s.value) : "array" == o || !r && "object" == o ? p(e, s, r, n) : e.add(n, s)
        })
    }

    var m, v, g = 0, y = window.document, b = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, w = /^(?:text|application)\/javascript/i, x = /^(?:text|application)\/xml/i, k = "application/json", _ = "text/html", C = /^\s*$/;
    t.active = 0, t.ajaxJSONP = function (e) {
        if (!("type"in e))return t.ajax(e);
        var n, r = "jsonp" + ++g, i = y.createElement("script"), u = function () {
            clearTimeout(n), t(i).remove(), delete window[r]
        }, d = function (t) {
            u(), t && "timeout" != t || (window[r] = c), s(null, t || "abort", l, e)
        }, l = {abort: d};
        return o(l, e) === !1 ? (d("abort"), !1) : (window[r] = function (t) {
            u(), a(t, l, e)
        }, i.onerror = function () {
            d("error")
        }, i.src = e.url.replace(/=\?/, "=" + r), t("head").append(i), e.timeout > 0 && (n = setTimeout(function () {
            d("timeout")
        }, e.timeout)), l)
    }, t.ajaxSettings = {
        type: "GET",
        beforeSend: c,
        success: c,
        error: c,
        complete: c,
        context: null,
        global: !0,
        xhr: function () {
            return new window.XMLHttpRequest
        },
        accepts: {
            script: "text/javascript, application/javascript",
            json: k,
            xml: "application/xml, text/xml",
            html: _,
            text: "text/plain"
        },
        crossDomain: !1,
        timeout: 0,
        processData: !0,
        cache: !0
    }, t.ajax = function (e) {
        var n = t.extend({}, e || {});
        for (m in t.ajaxSettings)void 0 === n[m] && (n[m] = t.ajaxSettings[m]);
        r(n), n.crossDomain || (n.crossDomain = /^([\w-]+:)?\/\/([^\/]+)/.test(n.url) && RegExp.$2 != window.location.host), n.url || (n.url = "" + window.location), f(n), n.cache === !1 && (n.url = l(n.url, "_=" + Date.now()));
        var i = n.dataType, u = /=\?/.test(n.url);
        if ("jsonp" == i || u)return u || (n.url = l(n.url, "callback=?")), t.ajaxJSONP(n);
        var h, p = n.accepts[i], g = {}, y = /^([\w-]+:)\/\//.test(n.url) ? RegExp.$1 : window.location.protocol, b = n.xhr();
        n.crossDomain || (g["X-Requested-With"] = "XMLHttpRequest"), p && (g.Accept = p, p.indexOf(",") > -1 && (p = p.split(",", 2)[0]), b.overrideMimeType && b.overrideMimeType(p)), (n.contentType || n.contentType !== !1 && n.data && "GET" != n.type.toUpperCase()) && (g["Content-Type"] = n.contentType || "application/x-www-form-urlencoded"), n.headers = t.extend(g, n.headers || {}), b.onreadystatechange = function () {
            if (4 == b.readyState) {
                b.onreadystatechange = c, clearTimeout(h);
                var e, r = !1;
                if (b.status >= 200 && 300 > b.status || 304 == b.status || 0 == b.status && "file:" == y) {
                    i = i || d(b.getResponseHeader("content-type")), e = b.responseText;
                    try {
                        "script" == i ? (1, eval)(e) : "xml" == i ? e = b.responseXML : "json" == i && (e = C.test(e) ? null : t.parseJSON(e))
                    } catch (o) {
                        r = o
                    }
                    r ? s(r, "parsererror", b, n) : a(e, b, n)
                } else s(null, b.status ? "error" : "abort", b, n)
            }
        };
        var w = "async"in n ? n.async : !0;
        b.open(n.type, n.url, w);
        for (v in n.headers)b.setRequestHeader(v, n.headers[v]);
        return o(b, n) === !1 ? (b.abort(), !1) : (n.timeout > 0 && (h = setTimeout(function () {
            b.onreadystatechange = c, b.abort(), s(null, "timeout", b, n)
        }, n.timeout)), b.send(n.data ? n.data : null), b)
    }, t.get = function () {
        return t.ajax(h.apply(null, arguments))
    }, t.post = function () {
        var e = h.apply(null, arguments);
        return e.type = "POST", t.ajax(e)
    }, t.getJSON = function () {
        var e = h.apply(null, arguments);
        return e.dataType = "json", t.ajax(e)
    }, t.fn.load = function (e, n, r) {
        if (!this.length)return this;
        var i, o = this, a = e.split(/\s/), s = h(e, n, r), u = s.success;
        return a.length > 1 && (s.url = a[0], i = a[1]), s.success = function (e) {
            o.html(i ? t("<div>").html(e.replace(b, "")).find(i) : e), u && u.apply(o, arguments)
        }, t.ajax(s), this
    };
    var z = encodeURIComponent;
    t.param = function (t, e) {
        var n = [];
        return n.add = function (t, e) {
            this.push(z(t) + "=" + z(e))
        }, p(n, t, e), n.join("&").replace(/%20/g, "+")
    }
}(Zepto), function (t) {
    t.fn.serializeArray = function () {
        var e, n = [];
        return t(Array.prototype.slice.call(this.get(0).elements)).each(function () {
            e = t(this);
            var r = e.attr("type");
            "fieldset" != this.nodeName.toLowerCase() && !this.disabled && "submit" != r && "reset" != r && "button" != r && ("radio" != r && "checkbox" != r || this.checked) && n.push({
                name: e.attr("name"),
                value: e.val()
            })
        }), n
    }, t.fn.serialize = function () {
        var t = [];
        return this.serializeArray().forEach(function (e) {
            t.push(encodeURIComponent(e.name) + "=" + encodeURIComponent(e.value))
        }), t.join("&")
    }, t.fn.submit = function (e) {
        if (e)this.bind("submit", e); else if (this.length) {
            var n = t.Event("submit");
            this.eq(0).trigger(n), n.defaultPrevented || this.get(0).submit()
        }
        return this
    }
}(Zepto), function (t, e) {
    function n(t) {
        return r(t.replace(/([a-z])([A-Z])/, "$1-$2"))
    }

    function r(t) {
        return t.toLowerCase()
    }

    function i(t) {
        return o ? o + t : r(t)
    }

    var o, a, s, u, c, d, l, f, h = "", p = {
        Webkit: "webkit",
        Moz: "",
        O: "o",
        ms: "MS"
    }, m = window.document, v = m.createElement("div"), g = /^((translate|rotate|scale)(X|Y|Z|3d)?|matrix(3d)?|perspective|skew(X|Y)?)$/i, y = {};
    t.each(p, function (t, n) {
        return v.style[t + "TransitionProperty"] !== e ? (h = "-" + r(t) + "-", o = n, !1) : e
    }), a = h + "transform", y[s = h + "transition-property"] = y[u = h + "transition-duration"] = y[c = h + "transition-timing-function"] = y[d = h + "animation-name"] = y[l = h + "animation-duration"] = y[f = h + "animation-timing-function"] = "", t.fx = {
        off: o === e && v.style.transitionProperty === e,
        speeds: {_default: 400, fast: 200, slow: 600},
        cssPrefix: h,
        transitionEnd: i("TransitionEnd"),
        animationEnd: i("AnimationEnd")
    }, t.fn.animate = function (e, n, r, i) {
        return t.isPlainObject(n) && (r = n.easing, i = n.complete, n = n.duration), n && (n = ("number" == typeof n ? n : t.fx.speeds[n] || t.fx.speeds._default) / 1e3), this.anim(e, n, r, i)
    }, t.fn.anim = function (r, i, o, h) {
        var p, m, v, b = {}, w = "", x = this, k = t.fx.transitionEnd;
        if (i === e && (i = .4), t.fx.off && (i = 0), "string" == typeof r)b[d] = r, b[l] = i + "s", b[f] = o || "linear", k = t.fx.animationEnd; else {
            m = [];
            for (p in r)g.test(p) ? w += p + "(" + r[p] + ") " : (b[p] = r[p], m.push(n(p)));
            w && (b[a] = w, m.push(a)), i > 0 && "object" == typeof r && (b[s] = m.join(", "), b[u] = i + "s", b[c] = o || "linear")
        }
        return v = function (n) {
            if (n !== e) {
                if (n.target !== n.currentTarget)return;
                t(n.target).unbind(k, v)
            }
            t(this).css(y), h && h.call(this)
        }, i > 0 && this.bind(k, v), this.size() && this.get(0).clientLeft, this.css(b), 0 >= i && setTimeout(function () {
            x.each(function () {
                v.call(this)
            })
        }, 0), this
    }, v = null
}(Zepto), define("lib/zepto/zepto", function (t) {
    return function () {
        var e;
        return e || t.$
    }
}(this)), define("zepto", ["lib/zepto/zepto"], function (t) {
    return t
}), function (t) {
    if ("function" == typeof bootstrap)bootstrap("promise", t); else if ("object" == typeof exports)module.exports = t(); else if ("function" == typeof define && define.amd)define("lib/q/q", t); else if ("undefined" != typeof ses) {
        if (!ses.ok())return;
        ses.makeQ = t
    } else Q = t()
}(function () {
    function t(t) {
        var e = Function.call;
        return function () {
            return e.apply(t, arguments)
        }
    }

    function e(t) {
        return t === Object(t)
    }

    function n(t) {
        return "[object StopIteration]" === ye(t) || t instanceof se
    }

    function r(t, e) {
        if (ie && e.stack && "object" == typeof t && null !== t && t.stack && -1 === t.stack.indexOf(we)) {
            for (var n = [], r = e; r; r = r.source)r.stack && n.unshift(r.stack);
            n.unshift(t.stack);
            var o = n.join("\n" + we + "\n");
            t.stack = i(o)
        }
    }

    function i(t) {
        for (var e = t.split("\n"), n = [], r = 0; e.length > r; ++r) {
            var i = e[r];
            s(i) || o(i) || !i || n.push(i)
        }
        return n.join("\n")
    }

    function o(t) {
        return -1 !== t.indexOf("(module.js:") || -1 !== t.indexOf("(node.js:")
    }

    function a(t) {
        var e = /at .+ \((.+):(\d+):(?:\d+)\)$/.exec(t);
        if (e)return [e[1], Number(e[2])];
        var n = /at ([^ ]+):(\d+):(?:\d+)$/.exec(t);
        if (n)return [n[1], Number(n[2])];
        var r = /.*@(.+):(\d+)$/.exec(t);
        return r ? [r[1], Number(r[2])] : void 0
    }

    function s(t) {
        var e = a(t);
        if (!e)return !1;
        var n = e[0], r = e[1];
        return n === ae && r >= ue && Te >= r
    }

    function u() {
        if (ie)try {
            throw Error()
        } catch (t) {
            var e = t.stack.split("\n"), n = e[0].indexOf("@") > 0 ? e[1] : e[2], r = a(n);
            if (!r)return;
            return ae = r[0], r[1]
        }
    }

    function c(t, e, n) {
        return function () {
            return "undefined" != typeof console && "function" == typeof console.warn && console.warn(e + " is deprecated, use " + n + " instead.", Error("").stack), t.apply(t, arguments)
        }
    }

    function d(t) {
        return S(t)
    }

    function l() {
        function t(t) {
            e = t, o.source = t, fe(n, function (e, n) {
                de(function () {
                    t.promiseDispatch.apply(t, n)
                })
            }, void 0), n = void 0, r = void 0
        }

        var e, n = [], r = [], i = me(l.prototype), o = me(h.prototype);
        if (o.promiseDispatch = function (t, i, o) {
                var a = le(arguments);
                n ? (n.push(a), "when" === i && o[1] && r.push(o[1])) : de(function () {
                    e.promiseDispatch.apply(e, a)
                })
            }, o.valueOf = c(function () {
                if (n)return o;
                var t = p(e);
                return m(t) && (e = t), t
            }, "valueOf", "inspect"), o.inspect = function () {
                return e ? e.inspect() : {state: "pending"}
            }, d.longStackSupport && ie)try {
            throw Error()
        } catch (a) {
            o.stack = a.stack.substring(a.stack.indexOf("\n") + 1)
        }
        return i.promise = o, i.resolve = function (n) {
            e || t(S(n))
        }, i.fulfill = function (n) {
            e || t(T(n))
        }, i.reject = function (n) {
            e || t(z(n))
        }, i.notify = function (t) {
            e || fe(r, function (e, n) {
                de(function () {
                    n(t)
                })
            }, void 0)
        }, i
    }

    function f(t) {
        if ("function" != typeof t)throw new TypeError("resolver must be a function.");
        var e = l();
        return I(t, e.resolve, e.reject, e.notify).fail(e.reject), e.promise
    }

    function h(t, e, n) {
        void 0 === e && (e = function (t) {
            return z(Error("Promise does not support operation: " + t))
        }), void 0 === n && (n = function () {
            return {state: "unknown"}
        });
        var r = me(h.prototype);
        if (r.promiseDispatch = function (n, i, o) {
                var a;
                try {
                    a = t[i] ? t[i].apply(r, o) : e.call(r, i, o)
                } catch (s) {
                    a = z(s)
                }
                n && n(a)
            }, r.inspect = n, n) {
            var i = n();
            "rejected" === i.state && (r.exception = i.reason), r.valueOf = c(function () {
                var t = n();
                return "pending" === t.state || "rejected" === t.state ? r : t.value
            })
        }
        return r
    }

    function p(t) {
        if (m(t)) {
            var e = t.inspect();
            if ("fulfilled" === e.state)return e.value
        }
        return t
    }

    function m(t) {
        return e(t) && "function" == typeof t.promiseDispatch && "function" == typeof t.inspect
    }

    function v(t) {
        return e(t) && "function" == typeof t.then
    }

    function g(t) {
        return m(t) && "pending" === t.inspect().state
    }

    function y(t) {
        return !m(t) || "fulfilled" === t.inspect().state
    }

    function b(t) {
        return m(t) && "rejected" === t.inspect().state
    }

    function w() {
        _e || "undefined" == typeof window || window.Touch || !window.console || console.warn("[Q] Unhandled rejection reasons (should be empty):", xe), _e = !0
    }

    function x() {
        for (var t = 0; xe.length > t; t++) {
            var e = xe[t];
            e && e.stack !== void 0 ? console.warn("Unhandled rejection reason:", e.stack) : console.warn("Unhandled rejection reason (no stack):", e)
        }
    }

    function k() {
        xe.length = 0, ke.length = 0, _e = !1, Ce || (Ce = !0, "undefined" != typeof process && process.on && process.on("exit", x))
    }

    function _(t, e) {
        Ce && (ke.push(t), xe.push(e), w())
    }

    function C(t) {
        if (Ce) {
            var e = he(ke, t);
            -1 !== e && (ke.splice(e, 1), xe.splice(e, 1))
        }
    }

    function z(t) {
        var e = h({
            when: function (e) {
                return e && C(this), e ? e(t) : this
            }
        }, function () {
            return this
        }, function () {
            return {state: "rejected", reason: t}
        });
        return _(e, t), e
    }

    function T(t) {
        return h({
            when: function () {
                return t
            }, get: function (e) {
                return t[e]
            }, set: function (e, n) {
                t[e] = n
            }, "delete": function (e) {
                delete t[e]
            }, post: function (e, n) {
                return null === e || void 0 === e ? t.apply(void 0, n) : t[e].apply(t, n)
            }, apply: function (e, n) {
                return t.apply(e, n)
            }, keys: function () {
                return ge(t)
            }
        }, void 0, function () {
            return {state: "fulfilled", value: t}
        })
    }

    function S(t) {
        return m(t) ? t : v(t) ? E(t) : T(t)
    }

    function E(t) {
        var e = l();
        return de(function () {
            try {
                t.then(e.resolve, e.reject, e.notify)
            } catch (n) {
                e.reject(n)
            }
        }), e.promise
    }

    function $(t) {
        return h({
            isDef: function () {
            }
        }, function (e, n) {
            return M(t, e, n)
        }, function () {
            return S(t).inspect()
        })
    }

    function O(t, e, n, r) {
        return d(t).then(e, n, r)
    }

    function A(t, e, n) {
        return O(t, function (t) {
            return Z(t).then(function (t) {
                return e.apply(void 0, t)
            }, n)
        }, n)
    }

    function P(t) {
        return function () {
            function e(t, e) {
                var a;
                if (be) {
                    try {
                        a = r[t](e)
                    } catch (s) {
                        return z(s)
                    }
                    return a.done ? a.value : O(a.value, i, o)
                }
                try {
                    a = r[t](e)
                } catch (s) {
                    return n(s) ? s.value : z(s)
                }
                return O(a, i, o)
            }

            var r = t.apply(this, arguments), i = e.bind(e, "send"), o = e.bind(e, "throw");
            return i()
        }
    }

    function q(t) {
        d.done(d.async(t)())
    }

    function j(t) {
        throw new se(t)
    }

    function N(t) {
        return function () {
            return A([this, Z(arguments)], function (e, n) {
                return t.apply(e, n)
            })
        }
    }

    function M(t, e, n) {
        var r = l();
        return de(function () {
            S(t).promiseDispatch(r.resolve, e, n)
        }), r.promise
    }

    function R(t) {
        return function (e) {
            var n = le(arguments, 1);
            return M(e, t, n)
        }
    }

    function D(t, e) {
        var n = le(arguments, 2);
        return ze(t, e, n)
    }

    function L(t, e) {
        return M(t, "apply", [, e])
    }

    function I(t) {
        var e = le(arguments, 1);
        return L(t, e)
    }

    function F(t) {
        var e = le(arguments, 1);
        return function () {
            var n = e.concat(le(arguments));
            return M(t, "apply", [this, n])
        }
    }

    function Z(t) {
        return O(t, function (t) {
            var e = 0, n = l();
            return fe(t, function (r, i, o) {
                var a;
                m(i) && "fulfilled" === (a = i.inspect()).state ? t[o] = a.value : (++e, O(i, function (r) {
                    t[o] = r, 0 === --e && n.resolve(t)
                }, n.reject))
            }, void 0), 0 === e && n.resolve(t), n.promise
        })
    }

    function B(t) {
        return O(t, function (t) {
            return t = pe(t, S), O(Z(pe(t, function (t) {
                return O(t, ce, ce)
            })), function () {
                return t
            })
        })
    }

    function U(t) {
        return O(t, function (t) {
            return Z(pe(t, function (e, n) {
                return O(e, function (e) {
                    return t[n] = {state: "fulfilled", value: e}, t[n]
                }, function (e) {
                    return t[n] = {state: "rejected", reason: e}, t[n]
                })
            })).thenResolve(t)
        })
    }

    function V(t, e) {
        return O(t, void 0, e)
    }

    function W(t, e) {
        return O(t, void 0, void 0, e)
    }

    function H(t, e) {
        return O(t, function (t) {
            return O(e(), function () {
                return t
            })
        }, function (t) {
            return O(e(), function () {
                return z(t)
            })
        })
    }

    function Y(t, e, n, i) {
        var o = function (e) {
            de(function () {
                if (r(e, t), !d.onerror)throw e;
                d.onerror(e)
            })
        }, a = e || n || i ? O(t, e, n, i) : t;
        "object" == typeof process && process && process.domain && (o = process.domain.bind(o)), V(a, o)
    }

    function Q(t, e, n) {
        var r = l(), i = setTimeout(function () {
            r.reject(Error(n || "Timed out after " + e + " ms"))
        }, e);
        return O(t, function (t) {
            clearTimeout(i), r.resolve(t)
        }, function (t) {
            clearTimeout(i), r.reject(t)
        }, r.notify), r.promise
    }

    function X(t, e) {
        void 0 === e && (e = t, t = void 0);
        var n = l();
        return O(t, void 0, void 0, n.notify), setTimeout(function () {
            n.resolve(t)
        }, e), n.promise
    }

    function G(t, e) {
        var n = le(e), r = l();
        return n.push(r.makeNodeResolver()), L(t, n).fail(r.reject), r.promise
    }

    function J(t) {
        var e = le(arguments, 1), n = l();
        return e.push(n.makeNodeResolver()), L(t, e).fail(n.reject), n.promise
    }

    function K(t) {
        var e = le(arguments, 1);
        return function () {
            var n = e.concat(le(arguments)), r = l();
            return n.push(r.makeNodeResolver()), L(t, n).fail(r.reject), r.promise
        }
    }

    function te(t, e) {
        var n = le(arguments, 2);
        return function () {
            function r() {
                return t.apply(e, arguments)
            }

            var i = n.concat(le(arguments)), o = l();
            return i.push(o.makeNodeResolver()), L(r, i).fail(o.reject), o.promise
        }
    }

    function ee(t, e, n) {
        var r = le(n || []), i = l();
        return r.push(i.makeNodeResolver()), ze(t, e, r).fail(i.reject), i.promise
    }

    function ne(t, e) {
        var n = le(arguments, 2), r = l();
        return n.push(r.makeNodeResolver()), ze(t, e, n).fail(r.reject), r.promise
    }

    function re(t, e) {
        return e ? (t.then(function (t) {
            de(function () {
                e(null, t)
            })
        }, function (t) {
            de(function () {
                e(t)
            })
        }), void 0) : t
    }

    var ie = !1;
    try {
        throw Error()
    } catch (oe) {
        ie = !!oe.stack
    }
    var ae, se, ue = u(), ce = function () {
    }, de = function () {
        function t() {
            for (; e.next;) {
                e = e.next;
                var n = e.task;
                e.task = void 0;
                var i = e.domain;
                i && (e.domain = void 0, i.enter());
                try {
                    n()
                } catch (a) {
                    if (o)throw i && i.exit(), setTimeout(t, 0), i && i.enter(), a;
                    setTimeout(function () {
                        throw a
                    }, 0)
                }
                i && i.exit()
            }
            r = !1
        }

        var e = {task: void 0, next: null}, n = e, r = !1, i = void 0, o = !1;
        if (de = function (t) {
                n = n.next = {task: t, domain: o && process.domain, next: null}, r || (r = !0, i())
            }, "undefined" != typeof process && process.nextTick)o = !0, i = function () {
            process.nextTick(t)
        }; else if ("function" == typeof setImmediate)i = "undefined" != typeof window ? setImmediate.bind(window, t) : function () {
            setImmediate(t)
        }; else if ("undefined" != typeof MessageChannel) {
            var a = new MessageChannel;
            a.port1.onmessage = t, i = function () {
                a.port2.postMessage(0)
            }
        } else i = function () {
            setTimeout(t, 0)
        };
        return de
    }(), le = t(Array.prototype.slice), fe = t(Array.prototype.reduce || function (t, e) {
            var n = 0, r = this.length;
            if (1 === arguments.length)for (; ;) {
                if (n in this) {
                    e = this[n++];
                    break
                }
                if (++n >= r)throw new TypeError
            }
            for (; r > n; n++)n in this && (e = t(e, this[n], n));
            return e
        }), he = t(Array.prototype.indexOf || function (t) {
            for (var e = 0; this.length > e; e++)if (this[e] === t)return e;
            return -1
        }), pe = t(Array.prototype.map || function (t, e) {
            var n = this, r = [];
            return fe(n, function (i, o, a) {
                r.push(t.call(e, o, a, n))
            }, void 0), r
        }), me = Object.create || function (t) {
            function e() {
            }

            return e.prototype = t, new e
        }, ve = t(Object.prototype.hasOwnProperty), ge = Object.keys || function (t) {
            var e = [];
            for (var n in t)ve(t, n) && e.push(n);
            return e
        }, ye = t(Object.prototype.toString);
    se = "undefined" != typeof ReturnValue ? ReturnValue : function (t) {
        this.value = t
    };
    var be;
    try {
        Function("(function* (){ yield 1; })"), be = !0
    } catch (oe) {
        be = !1
    }
    var we = "From previous event:";
    d.nextTick = de, d.longStackSupport = !1, d.defer = l, l.prototype.makeNodeResolver = function () {
        var t = this;
        return function (e, n) {
            e ? t.reject(e) : arguments.length > 2 ? t.resolve(le(arguments, 1)) : t.resolve(n)
        }
    }, d.promise = f, d.makePromise = h, h.prototype.then = function (t, e, n) {
        function i(e) {
            try {
                return "function" == typeof t ? t(e) : e
            } catch (n) {
                return z(n)
            }
        }

        function o(t) {
            if ("function" == typeof e) {
                r(t, s);
                try {
                    return e(t)
                } catch (n) {
                    return z(n)
                }
            }
            return z(t)
        }

        function a(t) {
            return "function" == typeof n ? n(t) : t
        }

        var s = this, u = l(), c = !1;
        return de(function () {
            s.promiseDispatch(function (t) {
                c || (c = !0, u.resolve(i(t)))
            }, "when", [function (t) {
                c || (c = !0, u.resolve(o(t)))
            }])
        }), s.promiseDispatch(void 0, "when", [, function (t) {
            var e, n = !1;
            try {
                e = a(t)
            } catch (r) {
                if (n = !0, !d.onerror)throw r;
                d.onerror(r)
            }
            n || u.notify(e)
        }]), u.promise
    }, h.prototype.thenResolve = function (t) {
        return O(this, function () {
            return t
        })
    }, h.prototype.thenReject = function (t) {
        return O(this, function () {
            throw t
        })
    }, fe(["isFulfilled", "isRejected", "isPending", "dispatch", "when", "spread", "get", "set", "del", "delete", "post", "send", "mapply", "invoke", "mcall", "keys", "fapply", "fcall", "fbind", "all", "allResolved", "timeout", "delay", "catch", "finally", "fail", "fin", "progress", "done", "nfcall", "nfapply", "nfbind", "denodeify", "nbind", "npost", "nsend", "nmapply", "ninvoke", "nmcall", "nodeify"], function (t, e) {
        h.prototype[e] = function () {
            return d[e].apply(d, [this].concat(le(arguments)))
        }
    }, void 0), h.prototype.toSource = function () {
        return "" + this
    }, h.prototype.toString = function () {
        return "[object Promise]"
    }, d.nearer = p, d.isPromise = m, d.isPromiseAlike = v, d.isPending = g, d.isFulfilled = y, d.isRejected = b;
    var xe = [], ke = [], _e = !1, Ce = !0;
    d.resetUnhandledRejections = k, d.getUnhandledReasons = function () {
        return xe.slice()
    }, d.stopUnhandledRejectionTracking = function () {
        k(), "undefined" != typeof process && process.on && process.removeListener("exit", x), Ce = !1
    }, k(), d.reject = z, d.fulfill = T, d.resolve = S, d.master = $, d.when = O, d.spread = A, d.async = P, d.spawn = q, d["return"] = j, d.promised = N, d.dispatch = M, d.dispatcher = R, d.get = R("get"), d.set = R("set"), d["delete"] = d.del = R("delete");
    var ze = d.post = R("post");
    d.mapply = ze, d.send = D, d.invoke = D, d.mcall = D, d.fapply = L, d["try"] = I, d.fcall = I, d.fbind = F, d.keys = R("keys"), d.all = Z, d.allResolved = c(B, "allResolved", "allSettled"), d.allSettled = U, d["catch"] = d.fail = V, d.progress = W, d["finally"] = d.fin = H, d.done = Y, d.timeout = Q, d.delay = X, d.nfapply = G, d.nfcall = J, d.nfbind = K, d.denodeify = d.nfbind, d.nbind = te, d.npost = ee, d.nmapply = ee, d.nsend = ne, d.ninvoke = d.nsend, d.nmcall = d.nsend, d.nodeify = re;
    var Te = u();
    return d
}), define("q", ["lib/q/q"], function (t) {
    return t
}), function (t) {
    var e = {
        VERSION: "2.2.0",
        Result: {SUCCEEDED: 1, NOTRANSITION: 2, CANCELLED: 3, PENDING: 4},
        Error: {INVALID_TRANSITION: 100, PENDING_TRANSITION: 200, INVALID_CALLBACK: 300},
        WILDCARD: "*",
        ASYNC: "async",
        create: function (t, n) {
            var r = "string" == typeof t.initial ? {state: t.initial} : t.initial, i = t.terminal || t["final"], o = n || t.target || {}, a = t.events || [], s = t.callbacks || {}, u = {}, c = function (t) {
                var n = t.from instanceof Array ? t.from : t.from ? [t.from] : [e.WILDCARD];
                u[t.name] = u[t.name] || {};
                for (var r = 0; n.length > r; r++)u[t.name][n[r]] = t.to || n[r]
            };
            r && (r.event = r.event || "startup", c({name: r.event, from: "none", to: r.state}));
            for (var d = 0; a.length > d; d++)c(a[d]);
            for (var l in u)u.hasOwnProperty(l) && (o[l] = e.buildEvent(l, u[l]));
            for (var l in s)s.hasOwnProperty(l) && (o[l] = s[l]);
            return o.current = "none", o.is = function (t) {
                return t instanceof Array ? t.indexOf(this.current) >= 0 : this.current === t
            }, o.can = function (t) {
                return !this.transition && (u[t].hasOwnProperty(this.current) || u[t].hasOwnProperty(e.WILDCARD))
            }, o.cannot = function (t) {
                return !this.can(t)
            }, o.error = t.error || function (t, e, n, r, i, o, a) {
                    throw a || o
                }, o.isFinished = function () {
                return this.is(i)
            }, r && !r.defer && o[r.event](), o
        },
        doCallback: function (t, n, r, i, o, a) {
            if (n)try {
                return n.apply(t, [r, i, o].concat(a))
            } catch (s) {
                return t.error(r, i, o, a, e.Error.INVALID_CALLBACK, "an exception occurred in a caller-provided callback function", s)
            }
        },
        beforeAnyEvent: function (t, n, r, i, o) {
            return e.doCallback(t, t.onbeforeevent, n, r, i, o)
        },
        afterAnyEvent: function (t, n, r, i, o) {
            return e.doCallback(t, t.onafterevent || t.onevent, n, r, i, o)
        },
        leaveAnyState: function (t, n, r, i, o) {
            return e.doCallback(t, t.onleavestate, n, r, i, o)
        },
        enterAnyState: function (t, n, r, i, o) {
            return e.doCallback(t, t.onenterstate || t.onstate, n, r, i, o)
        },
        changeState: function (t, n, r, i, o) {
            return e.doCallback(t, t.onchangestate, n, r, i, o)
        },
        beforeThisEvent: function (t, n, r, i, o) {
            return e.doCallback(t, t["onbefore" + n], n, r, i, o)
        },
        afterThisEvent: function (t, n, r, i, o) {
            return e.doCallback(t, t["onafter" + n] || t["on" + n], n, r, i, o)
        },
        leaveThisState: function (t, n, r, i, o) {
            return e.doCallback(t, t["onleave" + r], n, r, i, o)
        },
        enterThisState: function (t, n, r, i, o) {
            return e.doCallback(t, t["onenter" + i] || t["on" + i], n, r, i, o)
        },
        beforeEvent: function (t, n, r, i, o) {
            return !1 === e.beforeThisEvent(t, n, r, i, o) || !1 === e.beforeAnyEvent(t, n, r, i, o) ? !1 : void 0
        },
        afterEvent: function (t, n, r, i, o) {
            e.afterThisEvent(t, n, r, i, o), e.afterAnyEvent(t, n, r, i, o)
        },
        leaveState: function (t, n, r, i, o) {
            var a = e.leaveThisState(t, n, r, i, o), s = e.leaveAnyState(t, n, r, i, o);
            return !1 === a || !1 === s ? !1 : e.ASYNC === a || e.ASYNC === s ? e.ASYNC : void 0
        },
        enterState: function (t, n, r, i, o) {
            e.enterThisState(t, n, r, i, o), e.enterAnyState(t, n, r, i, o)
        },
        buildEvent: function (t, n) {
            return function () {
                var r = this.current, i = n[r] || n[e.WILDCARD] || r, o = Array.prototype.slice.call(arguments);
                if (this.transition)return this.error(t, r, i, o, e.Error.PENDING_TRANSITION, "event " + t + " inappropriate because previous transition did not complete");
                if (this.cannot(t))return this.error(t, r, i, o, e.Error.INVALID_TRANSITION, "event " + t + " inappropriate in current state " + this.current);
                if (!1 === e.beforeEvent(this, t, r, i, o))return e.Result.CANCELLED;
                if (r === i)return e.afterEvent(this, t, r, i, o), e.Result.NOTRANSITION;
                var a = this;
                this.transition = function () {
                    return a.transition = null, a.current = i, e.enterState(a, t, r, i, o), e.changeState(a, t, r, i, o), e.afterEvent(a, t, r, i, o), e.Result.SUCCEEDED
                }, this.transition.cancel = function () {
                    a.transition = null, e.afterEvent(a, t, r, i, o)
                };
                var s = e.leaveState(this, t, r, i, o);
                return !1 === s ? (this.transition = null, e.Result.CANCELLED) : e.ASYNC === s ? e.Result.PENDING : this.transition ? this.transition() : void 0
            }
        }
    };
    "function" == typeof define ? define("lib/javascript-state-machine/state-machine", ["require"], function () {
        return e
    }) : t.StateMachine = e
}(this), define("state-machine", ["lib/javascript-state-machine/state-machine"], function (t) {
    return t
}), define("alias", ["zepto"], function (t) {
    return {w: window, h: t("html"), b: t("body")}
}), define("time", [], function () {
    var t, e, n;
    return t = function (t) {
        return setTimeout(t, 1)
    }, e = function (t, e) {
        return setTimeout(t, e)
    }, n = function (t, e) {
        return setTimeout(e, t)
    }, {defer: t, delay: e, wait: n}
}), define("globals", ["alias", "time"], function () {
    var t, e, n, r, i;
    for (e = {}, n = 0, i = arguments.length; i > n; n++) {
        t = arguments[n];
        for (r in t)e[r] = window[r] = t[r]
    }
    return e
}), define("array", [], function () {
    return Array.prototype.include = function (t) {
        return -1 !== this.indexOf(t)
    }, Array.prototype.empty = function () {
        return 0 === this.length
    }, Array.prototype.first = function () {
        return this[0]
    }, Array.prototype.last = function () {
        return this[this.length - 1]
    }, Array.prototype.find = function (t, e) {
        var n, r, i, o;
        for (n = i = 0, o = this.length; o >= 0 ? o > i : i > o; n = o >= 0 ? ++i : --i)if (t.call(e, this[n], n)) {
            r = this[n];
            break
        }
        return r
    }, Array.prototype.findAll = function (t, e) {
        return this.filter(t, e)
    }, Array.prototype.uniq = function () {
        var t, e, n, r;
        for (t = [], n = 0, r = this.length; r > n; n++)e = this[n], -1 === t.indexOf(e) && t.push(e);
        return t
    }, Array.prototype.shuffle = function () {
        var t, e, n, r;
        for (t = this.copy(), e = this.length; --e;)n = Math.floor(Math.random() * (e + 1)), r = t[e], t[e] = t[n], t[n] = r;
        return t
    }, Array.prototype.pluck = function (t) {
        return this.map(function (e) {
            return e[t]
        })
    }, Array.prototype.invoke = function (t) {
        var e, n, r, i, o, a;
        for (n = function () {
            var t, n, r;
            for (r = [], t = 0, n = arguments.length; n > t; t++)e = arguments[t], r.push(e);
            return r
        }.apply(this, arguments).slice(1), a = [], i = 0, o = this.length; o > i; i++)r = this[i], a.push(r[t].apply(r, n));
        return a
    }, Array.prototype.max = function () {
        return Math.max.apply(null, this)
    }, Array.prototype.without = function (t) {
        var e;
        return e = this.indexOf(t), e >= 0 && this.splice(e, 1), this
    }, Array.prototype.sum = function () {
        var t, e, n, r;
        for (t = 0, n = 0, r = this.length; r > n; n++)e = this[n], t += e;
        return t
    }, Array.prototype.copy = function () {
        return this.slice()
    }, Array
}), define("detect", ["globals", "array"], function () {
    var t, e, n, r, i, o, a, s, u, c, d, l, f, p;
    return r = {}, d = navigator.userAgent.toLowerCase(), u = function (t) {
        return !!d.match(t)
    }, p = u(/xp/i) || u(/windows nt 5.1/i), f = u(/windows/i), l = u(/webkit/i), i = u(/firefox/i), t = u(/android/i), c = u(/mobile/i), o = u(/ipad/i), a = u(/iphone/i), e = u(/chrome/i), r = {
        transforms_3d_a_grade: !(!i && !l || p || t || e && f),
        transforms_3d_b_grade: (i || l) && !c && !p && !t,
        touch: "ontouchstart"in window,
        ipad: o,
        iphone: a,
        android: t,
        retina: window.devicePixelRatio > 1
    }, r.handheld = (r.ipad || r.iphone || r.android) && r.touch, n = function () {
        var t;
        t = [];
        for (s in r)r[s] ? t.push(s) : t.push("no_" + s);
        return t
    }(), h.addClass(n.join(" ")), r
}), define("window", ["globals", "lib/zepto/zepto", "detect"], function (t, e, n) {
    var r, i, o, a;
    return null != w.orientation && n.iphone && (r = 20, i = {
        iphone: {
            landscape: {w: 960, h: 640, chrome: 32},
            portrait: {w: 640, h: 960, chrome: 44}
        }
    }, o = n.ipad ? "ipad" : "iphone", a = function () {
        var t, e, n, a;
        return n = 0 === w.orientation || 180 === w.orientation ? "portrait" : "landscape", t = i[o][n].h, a = r, w.navigator.standalone || (a += i[o][n].chrome), e = t - 2 * a, b.removeClass(["portrait", "landscape"].without(n)[0]).addClass(n).css({
            width: i[o][n].w + "px",
            height: e + "px"
        })
    }, e(w).on("orientationchange", function () {
        return a(), scrollTo(0, 1)
    }), a()), w
}), define("string", ["globals"], function () {
    var t, e;
    return String.prototype.without = function (t) {
        var e;
        return e = RegExp("" + t, "g"), this.replace(e, "")
    }, String.prototype.capitalize = function () {
        return this.charAt(0).toUpperCase() + this.substring(1).toLowerCase()
    }, String.prototype.include = function (t) {
        return this.indexOf(t) >= 0
    }, String.prototype.endsWith = function (t) {
        return -1 !== this.indexOf(t, this.length - t.length)
    }, String.prototype.lowerCamelCase = function () {
        return this.split(/_|-/).map(function (t, e) {
            return 0 !== e ? t.capitalize() : t
        }).join("")
    }, String.prototype.stripFunkyChars = function () {
        return this.replace(/&#39;/g, "").replace(/&quot;/g, "").replace(/[?!:"',]/g, "").replace(/-+$/g, "")
    }, String.prototype.convertChars = function () {
        var t;
        return t = document.createElement("div"), t.innerHTML = this, t.innerHTML
    }, String.prototype.plural = function () {
        return this.endsWith("y") ? this.replace(/y$/, "ies") : this + "s"
    }, e = function (t) {
        return function (e, n) {
            var r, i, o;
            return o = "", 1 === arguments.length && (n = e, e = {}), "function" == typeof n && (n = n()), 2 === arguments.length && (i = function () {
                var t;
                t = [];
                for (r in e)t.push("" + r + "='" + e[r] + "'");
                return t
            }(), o = " " + i.join(" ")), "<" + t + o + ">" + n + "</" + t + ">"
        }
    }, t = function (t) {
        return function (e) {
            var n, r;
            return r = function () {
                var t;
                t = [];
                for (n in e)t.push("" + n + "='" + e[n] + "'");
                return t
            }(), "<" + t + " " + r.join(" ") + " />"
        }
    }, "td tr div option span".split(" ").forEach(function (t) {
        return w[t] = e(t)
    }), "img input".split(" ").forEach(function (e) {
        return w[e] = t(e)
    }), w.setupSelecterRenderer = function (t, e, n) {
        var r;
        return r = function () {
            var r, i, o, a, s, u;
            for (o = "", i = s = 0; n >= 0 ? n >= s : s >= n; i = n >= 0 ? ++s : --s)r = i - e(), r > 0 && (r = "+" + r), u = i + "", 1 === u.length && (u = "&nbsp;" + u), a = 0 === r ? u : "" + u + "&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;" + r, o += option({value: i}, a);
            return t.innerHTML = o
        }
    }, String
}), define("event", ["alias", "zepto", "detect"], function (t, e, n) {
    var r, i;
    return i = {}, r = {
        customEvents: i,
        down: n.handheld ? "touchstart" : "mousedown",
        up: n.handheld ? "touchend" : "mouseup",
        move: n.handheld ? "touchmove" : "mousemove"
    }, r.ondown = "on" + r.down, r.onup = "on" + r.up, r.onmove = "on" + r.move, w.fire = function (t, e) {
        var n, r, o, a;
        if (t in i)for (n = i[t].copy(), o = 0, a = n.length; a > o; o++)r = n[o], r({data: e});
        return w
    }, w.after = function (t, e) {
        var n, r, o, a;
        for (t = t.split(" "), a = [], r = 0, o = t.length; o > r; r++)n = t[r], n in i ? a.push(i[n].push(e)) : a.push(i[n] = [e]);
        return a
    }, w.forget = function (t, e) {
        var n, r, o, a;
        for (null == e && (e = !1), t = t.split(" "), a = [], r = 0, o = t.length; o > r; r++)n = t[r], n in i ? e ? (i[n].without(e), i[n].empty() ? a.push(delete i[n]) : a.push(void 0)) : a.push(delete i[n]) : a.push(void 0);
        return a
    }, w.safe = function (t) {
        return function (e) {
            var n;
            return stop(e), n = 3 === e.target.nodeType ? e.target.parentNode : e.target, n.blur(), t(e)
        }
    }, w.stop = function (t) {
        return t.preventDefault(), t.stopPropagation()
    }, w.onQuickClick = function (t, n) {
        return e(t).on(r.up, safe(n))
    }, e.fn.doubleclick = function (t) {
        var e, n, r, i;
        return n = !1, r = i = !1, e = function (e) {
            return r ? i = new Date : r = new Date, r && i && 300 > i - r ? (t(e), r = i = !1, clearTimeout(n)) : n = wait(301, function () {
                return r = i = !1
            })
        }, this.on("click", e)
    }, r
}), define("types", [], function () {
    return ["Creature", "Artifact", "Enchantment", "Land", "Sorcery", "Instant", "Legendary", "Planeswalker", "Token"]
}), define("get-active-card", ["require"], function (t) {
    return function () {
        return mtg.mousedOverCard || t("popup").card
    }
}), define("shortcut", ["event", "get-active-card"], function (t, e) {
    var n, r;
    return n = function (t) {
        return function () {
            var n;
            return n = e(), n ? t.call(n) : void 0
        }
    }, r = function (t, e) {
        return after("" + t + ":pressed", n(e))
    }
}), define("zone-model", ["require", "event", "types", "string", "shortcut"], function (t, e, n, r, i) {
    var o;
    return o = function () {
        function e(t) {
            var e = this;
            $.extend(this, t), this.id = this.name.toLowerCase(), this.cards = [], after("onleave" + this.id, function (t) {
                return e.remove(t.data.card), fire("" + e.id + ":changed", t)
            }), after("onenter" + this.id, function (t) {
                return e.add(t.data.card), fire("" + e.id + ":changed", t)
            }), i(this.key, function (t) {
                return function () {
                    return this[t]()
                }
            }(this.id))
        }

        return e.prototype.view = function () {
            return t("zone-views")[this.id]
        }, e.prototype.add = function (t, e) {
            var n;
            return n = !1, this.cards.include(t) && (this.remove(t), n = !0), null != e ? (this.cards.splice(e, 0, t), n = !0) : this.cards.push(t), n && fire("" + this.id + ":reordered"), this.cards
        }, e.prototype.remove = function (t) {
            return this.cards.without(t)
        }, e.prototype.first = function () {
            return this.cards.first()
        }, e.prototype.last = function () {
            return this.cards.last()
        }, e.prototype.size = function () {
            return this.cards.length
        }, e.prototype.indexOf = function (t) {
            return this.cards.indexOf(t)
        }, e.prototype.include = function (t) {
            return this.cards.include(t)
        }, e.prototype.empty = function () {
            return this.cards.empty()
        }, e.prototype.findByName = function (t) {
            return this.cards.find(function (e) {
                return e.name === t
            })
        }, e.prototype.findAllByName = function (t) {
            return this.cards.findAll(function (e) {
                return e.name === t
            })
        }, e.prototype.exportState = function () {
            var t, e = this;
            return t = {size: this.size()}, t.power = this.creatures().pluck("p").sum(), t.toughness = this.creatures().pluck("t").sum(), n.forEach(function (n) {
                var r;
                return n = n.toLowerCase(), r = n.plural(), t[r] = e[r]().length
            }), t
        }, e
    }(), n.forEach(function (t) {
        return o.prototype[t.plural().toLowerCase()] = function () {
            return this.cards.findAll(function (e) {
                return e.type.include(t)
            })
        }
    }), o
}), define("library-model", ["zone-model"], function (t) {
    var e;
    return e = new t({name: "Library", key: "y"}), e.addToTop = function (t) {
        return t.library(), this.add(t, e.size() - 1), fire("library:addedToTop", {id: t.id})
    }, e.addToBottom = function (t) {
        return t.library(), this.add(t, 0), fire("library:addedToBottom", {id: t.id})
    }, e.shuffle = function () {
        return this.cards = this.cards.shuffle(), fire("library:shuffled"), this
    }, e
}), define("window-loaded", ["zepto", "q"], function (t, e) {
    var n;
    return n = e.defer(), window.loaded ? n.resolve() : t(window).on("load", n.resolve), n.promise
}), define("get-focused-field", ["zepto", "q", "event"], function (t, e, n) {
    var r, i, o, a, s, u;
    return i = document.body, o = "input, select", a = void 0, u = function (r) {
        var s, u, c;
        return a = r.target, s = e.defer(), u = function (e) {
            return a ? e.target !== a && -1 === t(e.target).parents().indexOf(a) ? s.resolve() : void 0 : s.resolve()
        }, c = function () {
            return t(i).off(n.down, u).off("blur", o, s.resolve), t(a).blur(), a = void 0
        }, s.promise.then(c), t(i).on(n.down, u).on("blur", o, s.resolve)
    }, r = function (e) {
        return 27 === e.keyCode && t(this).blur().trigger("blur"), !0
    }, t(i).on("focus", o, u).on("keyup", "select", r), s = function () {
        return a
    }
}), define("keys", ["alias", "event", "string", "shortcut", "get-focused-field"], function (t, e, n, r, i) {
    var o, a;
    return o = {13: "return", 32: "spacebar", 27: "escape"}, a = function (t) {
        return o[t.charCode] || o[t.keyCode]
    }, $(document.body).on("keypress", function (t) {
        var e;
        return (!i() || t.ctrlKey || t.metaKey) && (e = a(t), e || (e = n.fromCharCode(t.charCode)), e && fire(e + ":pressed", {e: t})), !0
    }), r("q", function () {
        return this.mod("p", 1)
    }), r("Q", function () {
        return this.mod("p", -1)
    }), r("w", function () {
        return this.mod("t", 1)
    }), r("W", function () {
        return this.mod("t", -1)
    }), r("e", function () {
        return this.mod("c", 1)
    }), r("E", function () {
        return this.mod("c", -1)
    }), r("f", function () {
        return this.can("transform") ? this.transform() : this.toggleFace()
    }), r("F", function () {
        return this.toggleFlipped()
    }), r("t", function () {
        return this.toggleTapped()
    }), {KEYS: o, shortcut: r, getKey: a}
}), define("draw", ["alias", "zepto", "event", "library-model", "window-loaded", "keys", "window"], function (t, e, n, r, i, o) {
    var a;
    return a = function (t, e) {
        var n, i, o;
        for (null == t && (t = 1), i = o = 0; t >= 0 ? t > o : o > t; i = t >= 0 ? ++o : --o) {
            if (0 === r.size())return;
            n = function () {
                return null != e ? r.findByName(e) : r.last()
            }(), n.hand()
        }
    }, e(function () {
        var t, n;
        return t = e("#draw_card_by_name_field"), 0 !== t.length ? (n = function () {
            var e;
            return e = function () {
                var t, e;
                return r.cards.empty() ? option({value: ""}, "There are no cards in the library.") : (e = r.cards.pluck("name").uniq(), function () {
                    var n, r, i, o;
                    for (i = e.sort(), o = [], n = 0, r = i.length; r > n; n++)t = i[n], o.push(option({value: escape(t)}, t));
                    return o
                }().join(""))
            }(), e.length !== t.html().length ? t.html(e) : void 0
        }, after("onlibrary onleavelibrary", n), after("/:pressed", function () {
            return t.focus().trigger("focus")
        }), i.then(function () {
            return e("#draw_card_by_name").show(), onQuickClick("#draw_card_button", function () {
                return a(1, unescape(t.val()))
            }), t.on("keypress", function (e) {
                return "return" === o.getKey(e) && e.target === t[0] ? a(1, unescape(t.val())) : void 0
            })
        }), after("d:pressed", function () {
            return a()
        })) : void 0
    }), a
}), define("deck-loaded", ["event", "q"], function (t, e) {
    var n, r;
    return n = e.defer(), r = n.promise.timeout(15e3), r.fail(function () {
        return fire("deck:errored")
    }), after("deck:loaded", function (t) {
        return n.resolve(t.data)
    }), r
}), define("loaded", ["deck-loaded", "window-loaded"], function (t, e) {
    return e.then(function () {
        return t.then(function (t) {
            return t
        })
    })
}), define("card-dimensions", {}), define("preload-images", ["globals", "zepto", "q", "card-dimensions"], function (t, e, n, r) {
    var i, o;
    return o = function (t, i) {
        var o, a;
        return a = n.defer(), r[t] ? a.resolve() : (o = e(document.createElement("img")), o.addClass("temp_image").on("load", a.resolve), a.promise.then(function () {
            var e;
            return e = {
                width: o.width(),
                height: o.height()
            }, r[t] = e, o.off("load").remove(), fire("" + t.without(" ") + ":dimensions:loaded", e)
        }), b.append(o), o.attr("src", i)), a.promise
    }, i = function (t, e) {
        var r;
        return r = "object" == typeof t ? t : [[t, e]], n.allSettled(r.map(function (t) {
            return o.apply(null, t)
        }))
    }
}), define("card-model-string-helpers", ["string"], function () {
    var t, e, n;
    return e = {}, t = function (t, e) {
        return t.stripFunkyChars().replace(/\s/g, e).replace("//", e).replace("---", e).toLowerCase()
    }, e.buildPath = function (e, n, r) {
        var i, o;
        return e ? e.replace(/\s/g, "%20").replace(/&#39;/g, "'") : (o = mtg.local ? "/decks/images/" : "http://static.tappedout.net/mtg-cards/", n = t(n, "-"), r = t(r, "-"), i = "" + o + n + "/" + r + ".jpg")
    }, e.formatIdString = function (e, n) {
        return t(("id_" + e + "_" + n).replace(/\/+/g, "_").replace(/\./g, ""), "_")
    }, n = function (t) {
        return Number(!!t.match(/\*|x/i) || t)
    }, e.parsePTC = function (t) {
        var e, r;
        return e = {
            p: 0,
            t: 0,
            c: 0
        }, t && (t.include("/") ? (r = t.split("/"), e.p = n(r[0]), e.t = n(r[1])) : t.include("Loyalty") && (e.c = Number(t.split(" Loyalty")[0]))), e
    }, e
}), define("sizes", ["alias", "event", "loaded", "preload-images", "detect", "card-model-string-helpers"], function (t, e, n, r, i, o) {
    var a, s, u, c;
    return c = {small: "small", medium: "small", large: "medium"}, u = {
        cardPadding: 6,
        cardHeight: function () {
            return u.targetSizes[u.current].height + u.cardPadding
        },
        cardWidth: function () {
            return u.targetSizes[u.current].width + u.cardPadding
        },
        tappedoutSizes: c,
        options: ["small", "medium", "large"],
        targetSizes: {
            small: {width: 80, height: 114},
            medium: {height: 170, width: 120},
            large: {height: 285, width: 200}
        }
    }, u.current = i.handheld || 770 > b.height() ? "small" : "medium", u.replaceSizeInPath = function (t) {
        var e;
        return t ? (e = u.tappedoutSizes[i.retina ? "large" : u.current], t.replace("medium.", e + ".")) : ""
    }, a = function (t) {
        var e, n, i, a, s;
        return a = function () {
            var r;
            r = [];
            for (n in t.cards)e = t.cards[n], s = o.buildPath(t.media_root + u.replaceSizeInPath(e.image_path), e.edition, n), i = n.stripFunkyChars(), r.push([i, s]);
            return r
        }(), r(a)
    }, s = function (t) {
        var e, n, r, i, o;
        for (n = t.data.value, o = ["small", "medium", "large"].without(n), r = 0, i = o.length; i > r; r++)e = o[r], h.removeClass("card_size_" + e);
        return h.addClass("card_size_" + n), u.current = n
    }, after("size:changed", s), n.then(function () {
        return after("size:changed", function () {
            return n.then(a)
        })
    }), u
}), define("mtg", ["window", "globals", "array", "string", "detect", "draw", "loaded", "sizes"], function (t, e, n, r, i, o, a, s) {
    var u;
    return u = {
        local: location.host.include("local"), cardBackPath: function () {
            return "/images/back-" + (i.retina ? "large" : s.current) + ".png"
        }, transitionDuration: 500, mousedOverCard: void 0, Z: {FLOATING: 201, HOVER: 0}
    }, a.then(function () {
        return scrollTo(0, 1), b.removeClass("loading"), o(7), defer(function () {
            return fire("hand:drawn")
        })
    }), t.mtg = u
}), define("loader-helpers", ["array"], function () {
    var t, e;
    return e = function (t) {
        var e;
        return t = t || location, e = !1, t.pathname && "/" !== t.pathname && t.host.match(/playtest.tappedout.net|local|mtgplaytester.herokuapp.com/) ? e = "http://tappedout.net/mtg-decks" + t.pathname + "playtest.js" : t.hash.length > 0 && "#" !== t.hash && (e = t.hash.split("#d=")[1]), e
    }, t = function (t) {
        return t.match("playtest.js") ? t.split("/").without("playtest.js").last() : t.split("/").last().without(".js")
    }, {getPlaytestURL: e, getDeckId: t}
}), define("deck-url", ["loader-helpers"], function (t) {
    return function (e) {
        return "http://tappedout.net/mtg-decks/" + t.getDeckId(e) + "/"
    }
}), define("card-views", {}), define("zone-names", [], function () {
    return ["battlefield", "hand", "library", "exiled", "graveyard", "commander"]
}), define("battlefield-model", ["zone-model"], function (t) {
    return new t({
        name: "Battlefield", key: "b", tapped: function () {
            return this.cards.filter(function (t) {
                return "tapped" === t.tapState.current
            })
        }, untap: function () {
            var t;
            return t = this.tapped(), t.empty() || t.invoke("untap"), this
        }, getRecipients: function () {
            return this.cards.filter(function (t) {
                return !t.isPlaneswalker && (t.isCreature || t.hasNotes())
            })
        }
    })
}), define("hand-model", ["zone-model"], function (t) {
    var e;
    return e = new t({name: "Hand", key: "a"}), e.hidden = function () {
        var t;
        return t = this.cards.findAll(function (t) {
            return "facedown" === t.faceState.current
        }), t.length === this.cards.length && !this.empty()
    }, e
}), define("graveyard-model", ["zone-model"], function (t) {
    return new t({name: "Graveyard", key: "g"})
}), define("exiled-model", ["zone-model"], function (t) {
    return new t({name: "Exiled", key: "x"})
}), define("commander-model", ["zone-model"], function (t) {
    return new t({name: "Commander", key: "c"})
}), define("zone-models", ["zone-model", "battlefield-model", "hand-model", "library-model", "graveyard-model", "exiled-model", "commander-model"], function (t, e, n, r, i, o, a) {
    return {hand: n, library: r, battlefield: e, exiled: o, graveyard: i, commander: a}
}), define("super-select", ["lib/zepto/zepto", "globals"], function (t) {
    var e;
    return e = function () {
        function e(e) {
            var n = this;
            this.$el = t(e.el), this.template = e.template || this.template, this.render = e.render || this.render, this.$button = e.button ? t(e.button) : t(document.createElement("div")).addClass("super_select_button"), e.button || this.$el.before(this.$button), this.$el.on("mouseover", function () {
                return n.mouseover()
            }).on("mouseout", function () {
                return n.mouseout()
            }).addClass("super_select").on("change", function () {
                return n.render()
            }), defer(function () {
                return n.render()
            })
        }

        return e.prototype.render = function () {
            return this.$button.html(this.template(this.$el.val()))
        }, e.prototype.template = function () {
            return this.$el.val()
        }, e.prototype.mouseover = function () {
            return this.$button.addClass("hover")
        }, e.prototype.mouseout = function () {
            return this.$button.removeClass("hover")
        }, e.prototype.toggle = function (t) {
            return this.$button.toggleClass("disabled", t)
        }, e
    }()
}), define("modal", ["lib/zepto/zepto", "event"], function (t, e) {
    var n, r, i, o;
    return n = t("#modal_overlay"), o = [], r = function () {
        function e(e, n) {
            var r = this;
            return this.id = e, this.el = t("#" + e), this.closeEl = t("#" + n), this.visible = !1, onQuickClick(this.closeEl, function () {
                return r.hide()
            }), o.push(this), this
        }

        return e.prototype.show = function () {
            return i(), this.el.show(), this.visible = !0, n.addClass("visible"), fire("" + this.id + ":shown"), this
        }, e.prototype.hide = function () {
            return this.el.hide(), this.visible = !1, n.removeClass("visible"), fire("" + this.id + ":hidden"), w.modalRecentlyVisible = wait(200, function () {
                return w.modalRecentlyVisible = !1
            }), this
        }, e.prototype.toggle = function () {
            return this.visible ? this.hide() : this.show()
        }, e
    }(), i = function () {
        var t, e, n;
        for (e = 0, n = o.length; n > e; e++)t = o[e], t.visible && t.hide();
        return !1
    }, n.on(e.down, i), {Modal: r, $modalOverlay: n, modals: o}
}), define("button", ["event", "zepto"], function (t, e) {
    var n;
    return n = function (n, r) {
        var i, o, a, s, u = this;
        return this.id = n, this.$element = e("#" + n), a = (null != r ? r.type : void 0) || t.up, o = function (t) {
            return u.disabled || (fire("button:clicked", {id: n}), r.handler(t)), stop(t)
        }, this.$element.on(a, o), this.disable = function () {
            return u.disabled = !0, u.$element.addClass("disabled")
        }, this.enable = function () {
            return u.disabled = !1, u.$element.removeClass("disabled")
        }, r.control ? (i = function () {
            return r.control.condition() ? u.enable() : u.disable()
        }, i(), after(r.control.events, i)) : this.enable(), null != (s = r.init) && s.call(this), this
    }
});
var __hasProp = {}.hasOwnProperty, __extends = function (t, e) {
    function n() {
        this.constructor = t
    }

    for (var r in e)__hasProp.call(e, r) && (t[r] = e[r]);
    return n.prototype = e.prototype, t.prototype = new n, t.__super__ = e.prototype, t
};
define("popup", ["require", "modal", "button", "super-select", "detect", "event", "zone-models", "deck-loaded"], function (t, e, n, r, i, o, a, s) {
    var u, c, d, l, f, h, p, m, v, g, y, w, x, k, _, C, z, T;
    return k = e.modals, c = e.Modal, u = e.$modalOverlay, f = a.battlefield, v = a.hand, m = a.graveyard, p = a.exiled, y = a.library, x = void 0, s.then(function (t) {
        return x = t.media_root
    }), T = "p t c".split(" "), z = function (t) {
        return T.forEach(t)
    }, d = function (t) {
        function n(t, e) {
            n.__super__.constructor.call(this, t, e)
        }

        return __extends(n, t), n.prototype.card = void 0, n.prototype.img = $("#zoom_image").get(0), n.prototype.name = $("#zoomed_name"), n.prototype.edition = $("#zoomed_edition"), n.prototype.cardActionsEl = $("#card_actions"), n.prototype.height = 30, n.prototype.clickEventFromCard = !1, n.prototype.linkTemplate = function (t) {
            var e, n;
            return n = t, t.include("zoom") || (t.include("tap") && (e = "t"), t.match(/flip|face|form/) && (e = "f"), t = t.split(e).join(span({"class": "underline"}, e))), "<li><a class='button " + n + "' data-method='" + n + "'>" + t + "</a></li>"
        }, n.prototype.visible = !1, n.prototype.show = function (t) {
            var n, r, i, o = this;
            for (scrollTo(0, 1), this.card = t, this.img.src = ("transformed" === (null != (i = t.transformState) ? i.current : void 0) ? x + t.data.transformation.image_path : t.path).replace("small", "medium"), this.updateFields(), this.name.text(t.name), this.edition.text(t.edition), this.updateLinks(), this.boundUpdateFields = function () {
                return o.updateFields()
            }, after("" + t.id + ":p:changed " + t.id + ":t:changed " + t.id + ":c:changed", this.boundUpdateFields), this.boundOnTokenDestroy = function () {
                return forget("" + t.id + ":destroyed", o.boundOnTokenDestroy), o.hide(), o
            }, t.isToken && after("" + t.id + ":destroyed", this.boundOnTokenDestroy), n = 0, r = k.length; r > n; n++)e = k[n], e.visible && e !== this && e.hide();
            return this.el.show(), this.visible = !0, u.addClass("visible"), fire("" + this.id + ":shown"), this
        }, n.prototype.hide = function () {
            return forget("" + this.card.id + ":p:changed " + this.card.id + ":t:changed " + this.card.id + ":c:changed", this.boundUpdateFields), this.card = void 0, n.__super__.hide.call(this), scrollTo(0, 1)
        }, n.prototype.dismiss = function (t) {
            return t.target.offsetParent !== this.card && t.target.offsetParent !== this.el.get(0) ? (b.off(o.up, n.dismiss), this.hide()) : void 0
        }, n.prototype.tryToSetupFields = function () {
            var t = this;
            return $(this.p).html().length > 0 ? void 0 : (this.optionRenderers = {}, z(function (e) {
                return t.optionRenderers[e] = setupSelecterRenderer(t[e], function () {
                    return t.card[e]
                }, 100)
            }))
        }, n.prototype.updateFields = function () {
            var t = this;
            return this.card ? (this.tryToSetupFields(), z(function (e) {
                return t.optionRenderers[e](), t[e].value = t.card[e]
            }), this.pSelector.render(), this.tSelector.render(), this.cSelector.render(), fire("popup:fields:changed"), this) : !1
        }, n.prototype.updateLinks = function () {
            var t, e, n;
            t = this.card.capabilities();
            for (n in a)t.without(n);
            return _.card.can("transform") && t.without("facedown"), e = t.map(this.linkTemplate).join(""), this.cardActionsEl.html("<ul>" + e + "</ul>")
        }, n
    }(c), _ = new d("popup", "close_popup"), _.p = $("#zoomed_card_p").get(0), _.t = $("#zoomed_card_t").get(0), _.c = $("#zoomed_card_c").get(0), _.pSelector = new r({el: _.p}), _.tSelector = new r({el: _.t}), _.cSelector = new r({el: _.c}), after("popup:invoked", function (t) {
        return _.show(t.data.card), _.clickEventFromCard = !!t.data.fromCard, t.data.fromCard ? b.on(o.up, _.unsetClickEventFromCard) : void 0
    }), _.unsetClickEventFromCard = function () {
        return b.off(o.up, _.unsetClickEventFromCard), _.clickEventFromCard = !1
    }, after("spacebar:pressed", function () {
        return _.visible ? _.hide() : mtg.mousedOverCard ? _.show(mtg.mousedOverCard) : void 0
    }), z(function (t) {
        return $(_[t]).on("change", function () {
            var e;
            return e = Number(_[t].value - _.card[t]), _.card.mod(t, e)
        })
    }), C = function (t) {
        return function (e) {
            return _.clickEventFromCard ? _.unsetClickEventFromCard() : t(e)
        }
    }, _.eventType = i.iphone ? "click" : o.up, _.cardActionsEl.on(_.eventType, "a", C(function (t) {
        return _.card[$(t.currentTarget).data("method")](), _.hide(), !1
    })), l = function (t) {
        return safe(function () {
            var e;
            return (null != (e = _.card) ? e.can(t.id) : void 0) ? (_.card[t.id](), _.hide()) : void 0
        })
    }, h = function (t) {
        return {
            handler: C(l(t)), type: _.eventType, control: {
                condition: function () {
                    var e;
                    return (null != (e = _.card) ? e.zone() : void 0) !== t
                }, events: "popup:shown"
            }
        }
    }, new n("move_battlefield", h(f)), new n("move_hand", h(v)), new n("move_graveyard", h(m)), new n("move_exiled", h(p)), new n("move_library", {
        handler: C(safe(function () {
            return y.addToTop(_.card), _.hide()
        })), type: _.eventType, control: {
            condition: function () {
                return _.card && y.last() !== _.card
            }, events: "popup:shown"
        }
    }), new n("move_bottom_library", {
        handler: C(safe(function () {
            return y.addToBottom(_.card), _.hide()
        })), type: _.eventType, control: {
            condition: function () {
                return _.card && y.first() !== _.card
            }, events: "popup:shown"
        }
    }), w = function (t) {
        return safe(function () {
            return g() ? 0 === _.card.c && -1 === t ? !1 : z(function (e) {
                return _.card.mod(e, t)
            }) : void 0
        })
    }, g = function () {
        var t;
        return (null != (t = _.card) ? t.zone() : void 0) === f
    }, new n("add_plus_counter", {
        handler: C(w(1)),
        type: _.eventType,
        control: {condition: g, events: "popup:shown"}
    }), new n("remove_plus_counter", {
        handler: C(w(-1)), type: _.eventType, control: {
            condition: function () {
                return g() && _.card.c > 0
            }, events: "popup:shown popup:fields:changed"
        }
    }), _
}), define("zone-view", ["alias", "lib/zepto/zepto", "super-select", "event", "window-loaded", "popup"], function (t, e, n, r, i, o) {
    var a, s;
    return s = "Select a Card", a = function () {
        function t(t) {
            var r, i = this;
            this.model = t, this.$element = e("#" + this.model.id), this.counter = e("#" + this.model.id + " strong"), this.shell = this.$element, this.margin = this.length = 0, this.id = this.model.id, after("onenter" + this.id, function (t) {
                return i.add(t.data.card)
            }), after("" + this.id + ":changed", function () {
                return i.render()
            }), after("" + this.id + ":reordered", function () {
                return i.render()
            }), this.cacheDimensions(), e(w).on("resize", function () {
                return i.cacheDimensions()
            }), after("size:changed", function () {
                return i.cacheDimensions()
            }), this.$selectElement = e("#" + this.id + "_card_list"), this.superSelect = new n({
                el: this.$selectElement,
                button: this.$element.find(".super_select_button"),
                template: function () {
                },
                render: function () {
                }
            }), r = function () {
                return i.$element.removeClass("browsing"), i.$selectElement.get(0).blur()
            }, this.$selectElement.length > 0 && this.$selectElement.on("blur", r).on("change", function () {
                var t;
                return r(), i.$selectElement.val() !== s ? (t = i.model.cards[Number(i.$selectElement.val().split(" - ").first()) - 1], o.show(t), defer(function () {
                    return i.renderCardList()
                })) : i.$selectElement.get(0).blur()
            }), this.render()
        }

        return t.prototype.renderCardList = function () {
            var t, e, n;
            for (e = [option({value: s}, s)], t = this.model.cards.length; t > 0;)n = "" + t + " - " + this.model.cards[t - 1].name, e.push(option({value: n}, n)), t -= 1;
            return this.$selectElement.html(e.join(""))
        }, t.prototype.cacheDimensions = function () {
            var t = this;
            return i.then(function () {
                return t.width = t.$element.get(0).clientWidth, t.height = t.$element.get(0).clientHeight, t.left = function () {
                    var e, n;
                    return e = t.$element.parent().get(0), n = "side" === e.id ? e.offsetLeft : 0, t.$element.offset().left + 1 + n
                }(), t.top = t.$element.offset().top + 1
            })
        }, t.prototype.add = function (t, e) {
            var n;
            return n = t.view(), this.model.length - 1 > (null != e) ? this.model.cards[e + 1].view().$element.before(n.$element) : n ? this.$element.get(0).appendChild(n.$element.get(0)) : void 0
        }, t.prototype.fire = function (t, e) {
            return fire(this.id + ":" + t, e)
        }, t.prototype.updateCounter = function () {
            return this.$element.find(".super_select_button strong").text(this.model.size())
        }, t.prototype.restingZ = function () {
            return 1
        }, t.prototype.render = function () {
            return this.updateCounter(), this.$element.toggleClass("empty", this.model.empty()), this.superSelect.toggle(this.model.empty()), this.renderCardList()
        }, t
    }()
});
var __bind = function (t, e) {
    return function () {
        return t.apply(e, arguments)
    }
}, __hasProp = {}.hasOwnProperty, __extends = function (t, e) {
    function n() {
        this.constructor = t
    }

    for (var r in e)__hasProp.call(e, r) && (t[r] = e[r]);
    return n.prototype = e.prototype, t.prototype = new n, t.__super__ = e.prototype, t
};
define("distributable-zone-view", ["time", "zone-view"], function (t, e) {
    var n;
    return n = function (e) {
        function n(t) {
            this.delayedDistributeCards = __bind(this.delayedDistributeCards, this), n.__super__.constructor.call(this, t), this.margin = 5
        }

        return __extends(n, e), n.prototype.render = function () {
            return n.__super__.render.call(this), this.renderLastVisibleCards(), this.delayedDistributeCards()
        }, n.prototype.distributeCards = function () {
            var t, e, n, r, i, o, a;
            for (a = [], e = i = 0, o = this.model.size(); o >= 0 ? o > i : i > o; e = o >= 0 ? ++i : --i)t = this.model.cards[e], n = r = t.restingY = this.margin, a.push(t.move(n, r, this.restingZ(t)));
            return a
        }, n.prototype.delayedDistributeCards = function () {
            var e = this;
            return t.defer(function () {
                return e.distributeCards()
            }), !1
        }, n.prototype.restingZ = function (t) {
            var e, n;
            return e = this.model.cards.indexOf(t), n = e + 1
        }, n.prototype.renderLastVisibleCards = function () {
            var t, e, n, r, i;
            for (e = this.model.size() - 2, i = this.model.cards, n = 0, r = i.length; r > n; n++)t = i[n], e > this.model.indexOf(t) && t.view() && t.view().$element.removeClass("visible");
            return this.model.cards[e] && this.model.cards[e].view() && this.model.cards[e].view().$element.addClass("visible"), !this.model.empty() && this.model.last().view() ? this.model.last().view().$element.addClass("visible") : void 0
        }, n
    }(e)
}), define("collection", ["array"], function () {
    var t;
    return t = function () {
        function t(t) {
            this.models = (null != t ? t.models : void 0) || []
        }

        return t.prototype.add = function (t) {
            return this.models.push(t), this
        }, t.prototype.remove = function (t) {
            return this.models.without(t), this
        }, t.prototype.indexOf = function (t) {
            return this.models.indexOf(t)
        }, t.prototype.include = function (t) {
            return this.models.include(t)
        }, t.prototype.first = function () {
            return this.models[0]
        }, t.prototype.last = function () {
            return this.models.last()
        }, t.prototype.at = function (t) {
            return this.models[t]
        }, t.prototype.pluck = function (t) {
            return this.models.pluck(t)
        }, t.prototype.empty = function () {
            return 0 === this.models.length
        }, t
    }()
});
var __hasProp = {}.hasOwnProperty, __extends = function (t, e) {
    function n() {
        this.constructor = t
    }

    for (var r in e)__hasProp.call(e, r) && (t[r] = e[r]);
    return n.prototype = e.prototype, t.prototype = new n, t.__super__ = e.prototype, t
};
define("cell", ["collection", "sizes"], function (t, e) {
    var n, r, i, o, a;
    i = 30, r = 10, o = {}, n = function (t) {
        function n(t) {
            n.__super__.constructor.call(this), this.x = t.x, this.y = t.y, this.collection = t.collection, this.destroyHandlers = {}
        }

        return __extends(n, t), n.prototype.add = function (t) {
            var e, r, i = this;
            return e = this.position(this.models.length), n.__super__.add.call(this, t.move(e.x, e.y, this.models.length)), r = t.id, this.destroyHandlers[r] = function () {
                return i.remove(t)
            }, after("" + t.id + ":destroyed", this.destroyHandlers[r]), this
        }, n.prototype.remove = function (t) {
            var e = this;
            return n.__super__.remove.call(this, t), this.teardownCard(t.id), this.models.forEach(function (t, n) {
                var r;
                return r = e.position(n), t.move(r.x, r.y, n)
            }), this
        }, n.prototype.teardownCard = function (t) {
            return forget("" + t + ":destroyed", this.destroyHandlers[t]), delete this.destroyHandlers[t]
        }, n.prototype.teardown = function () {
            var t, e, n, r;
            for (r = this.models, e = 0, n = r.length; n > e; e++)t = r[e], this.teardownCard(t.id);
            return this.models = []
        }, n.prototype.position = function (t) {
            var o, a, s, u, c;
            return o = e.targetSizes[e.current], s = o.height - o.width, a = s / 2, u = t * r + a + n.padding.left, c = t * i + n.padding.top, {
                x: u + this.x,
                y: c + this.y
            }
        }, n.prototype.full = function () {
            return this.models.length >= 3
        }, n.prototype.next = function () {
            var t;
            return null != (t = this.collection) ? t.at(this.collection.indexOf(this) + 1) : void 0
        }, n.prototype.previous = function () {
            var t;
            return null != (t = this.collection) ? t.at(this.collection.indexOf(this) - 1) : void 0
        }, n
    }(t), n.width = function () {
        return o[e.current].width
    }, n.height = function () {
        return o[e.current].height
    }, n.padding = {top: 5, left: 5, right: 5, bottom: 20};
    for (a in e.targetSizes)o[a] = {height: e.targetSizes[a].height + (n.padding.bottom + n.padding.top) + 2 * i}, o[a].width = e.targetSizes[a].height + n.padding.left + n.padding.right + 2 * r;
    return n.gridSizes = o, n
});
var __hasProp = {}.hasOwnProperty, __extends = function (t, e) {
    function n() {
        this.constructor = t
    }

    for (var r in e)__hasProp.call(e, r) && (t[r] = e[r]);
    return n.prototype = e.prototype, t.prototype = new n, t.__super__ = e.prototype, t
};
define("cell-collection", ["collection", "cell"], function (t, e) {
    var n;
    return n = function (t) {
        function n(t) {
            n.__super__.constructor.call(this), this.width = t.width, this.height = t.height, this.top = t.top, this.left = t.left, this.createCells(), t.cards && this.addCards(t.cards)
        }

        return __extends(n, t), n.prototype.addCards = function (t) {
            var e, n, r;
            for (n = 0, r = t.length; r > n; n++)e = t[n], this.addCard(e);
            return this
        }, n.prototype.addCard = function (t) {
            var e, n, r, i, o, a, s, u, c, d;
            return c = t.type.join("").match(/Creature|Planeswalker|Equipment|Sorcery|Instant/), this.removeCard(t), u = c ? this.models : this.bottomRow(), i = u.find(function (t) {
                return t.empty()
            }), a = null != (d = u.filter(function (t) {
                return t.empty()
            })) ? d.last() : void 0, n = u.find(function (e) {
                return !e.full() && e.pluck("name").include(t.name)
            }), o = u.find(function (t) {
                return !t.full()
            }), s = u.pluck("models").pluck("length").max(), r = u.find(function (t) {
                    return s > t.models.length
                }) || u.first(), e = !1, e = c ? i || n : n || (t.is("land") ? i : a), (e || o || r).add(t), this
        }, n.prototype.removeCard = function (t) {
            var e, n, r, i, o;
            for (n = !1, o = this.models, r = 0, i = o.length; i > r; r++)if (e = o[r], n = e.include(t) ? e : !1) {
                n.remove(t);
                break
            }
            return this
        }, n.prototype.createCells = function (t) {
            var n, r, i, o;
            for (t && (this.height = t.height, this.width = t.width), this.teardown(), r = i = 0, o = this.maxCells(); o >= 0 ? o > i : i > o; r = o >= 0 ? ++i : --i)n = this.getCoordinates(r), this.add(new e({
                x: n.x,
                y: n.y,
                collection: this
            }));
            return this
        }, n.prototype.teardown = function () {
            return this.models.invoke("teardown"), this.models = [], $(".guide").remove(), this
        }, n.prototype.findCellByCoords = function (t, n) {
            var r, i, o, a, s;
            for (i = !1, s = this.models, o = 0, a = s.length; a > o; o++)if (r = s[o], t >= r.x && r.x + e.width() > t && n >= r.y && r.y + e.height() > n) {
                i = r;
                break
            }
            return i
        }, n.prototype.horizontalLimit = function () {
            return Math.ceil(this.maxCoords().x / e.width())
        }, n.prototype.verticalLimit = function () {
            return Math.max(Math.ceil(this.maxCoords().y / e.height()), 2)
        }, n.prototype.maxCells = function () {
            return this.horizontalLimit() * this.verticalLimit()
        }, n.prototype.maxCoords = function () {
            return {x: this.width - e.width(), y: this.height - e.height()}
        }, n.prototype.renderGuides = function () {
            var t, n, r, i, o, a, s, u, c = this;
            for ($(".guide").remove(), i = function (t, n) {
                var r;
                return r = {
                    vertical: {left: "" + ((t + 1) * e.width() + c.left) + "px"},
                    horizontal: {top: "" + ((t + 1) * e.height() + c.top) + "px"}
                }, $(document.createElement("div")).addClass("guide guide_" + n).css(r[n])
            }, n = [], r = 0, o = 0; this.verticalLimit() > r;)n.push(i(r, "horizontal")), r += 1;
            for (; this.horizontalLimit() > o;)n.push(i(o, "vertical")), o += 1;
            for (u = [], a = 0, s = n.length; s > a; a++)t = n[a], u.push($("body").append(t));
            return u
        }, n.prototype.getCoordinates = function (t) {
            var n, r, i;
            if (n = {x: 0, y: 0}, 0 !== t) {
                for (r = this.horizontalLimit(), i = t; i >= r;)i -= r;
                n.x = e.width() * i, i = Math.floor(t * (1 / this.horizontalLimit())), n.y = e.height() * i
            }
            return n
        }, n.prototype.bottomRow = function () {
            var t;
            return t = this.maxCells() - this.horizontalLimit(), this.models.filter(function (e, n) {
                return n >= t
            })
        }, n
    }(t)
}), define("grid", ["cell-collection"], function (t) {
    var e, n, r, i, o;
    return r = {}, o = void 0, n = function (n) {
        return e(), o = new t(n), r
    }, e = function () {
        return o && (o.teardown(), o = void 0), r
    }, i = function () {
        return o && o.createCells(), r
    }, r.get = function () {
        return o
    }, r.render = i, r.enable = n, r.disable = e, r
}), define("store", [], function () {
    var t;
    return t = function () {
        var e, n, r, i;
        return r = function () {
            var t;
            try {
                return "localStorage"in window && null !== window.localStorage
            } catch (e) {
                return t = e, !1
            }
        }(), r ? (i = function (t, e) {
            var n, r, o;
            try {
                return localStorage.setItem(t, e), e
            } catch (a) {
                for (n = a, r = o = 0; 5 >= o; r = ++o)localStorage.removeItem(localStorage.key(localStorage.length - 1));
                return i(t, e)
            }
        }, {
            set: i, get: function (t) {
                return localStorage[t]
            }, expire: function (t) {
                var e;
                return e = localStorage[t], localStorage.removeItem(t), e
            }
        }) : (e = function (t, e, n) {
            var r, i;
            return n ? (r = new Date, r.setTime(r.getTime() + 1e3 * 60 * 60 * 24 * n), i = "; expires=" + r.toGMTString()) : i = "", document.cookie = t + "=" + e + i + "; path=/", e
        }, n = function (t) {
            var e, n, r, i;
            for (t += "=", i = document.cookie.split(";"), n = 0, r = i.length; r > n; n++)if (e = i[n], 0 === e.indexOf(t))return e.replace(/^\s+/, "").substring(t.length, e.length);
            return null
        }, {
            set: function (t, n) {
                return e(t, n, 1)
            }, get: n, expire: function (n) {
                var r;
                return r = t.get(n), e(n, "", -1), r
            }
        })
    }()
}), define("settings", ["lib/zepto/zepto", "event", "button", "detect", "store", "sizes"], function (t, e, n, r, i, o) {
    var a, s, u, c, d, l;
    return a = function () {
        function n(n, o, a) {
            var s, u = this;
            null == o && (o = !0), null == a && (a = !1), this.message = a, this.setting = n.lowerCamelCase(), this.element = t("#" + n).get(0), this.element && (s = null != i.get(this.setting) ? JSON.parse(i.get(this.setting)) : o, this.defaultValue = o, this.set(this.value = s), r.handheld && t(this.element).parent().on(e.down, function () {
                return u.message && !confirm(u.message) ? !1 : (u.set(u.element.checked = !u.element.checked), !1)
            }), t(this.element).on("change", function () {
                return u.message && !confirm(u.message) ? (u.element.checked = !u.element.checked, !1) : u.set(u.element.checked)
            }), d[this.setting] = this)
        }

        return n.prototype.set = function (t) {
            return i.set(this.setting, this.element.checked = this.value = t), fire("setting:changed", {
                setting: this.setting,
                value: t
            }), fire("" + this.setting + ":changed", {value: t}), this
        }, n.prototype.get = function () {
            return this.value
        }, n.prototype.enable = function () {
            return this.set(!0)
        }, n.prototype.disable = function () {
            return this.set(!1)
        }, n.prototype.revert = function () {
            return this.set(this.defaultValue)
        }, n
    }(), s = function () {
        function n(n, r) {
            var o, a = this;
            this.setting = n, this.defaultValue = r, this.elements = t("input[type=radio][name=" + n + "]"), this.elements.length && (this.set(o = null != i.get(this.setting) ? i.get(this.setting) : r), d[n] = this, t(this.elements).parent().on(e.down, function (e) {
                return a.set(t(e.target).find("input").val())
            }), t(this.elements).on("change", function (t) {
                return t.target.checked ? a.set(t.target.value) : void 0
            }))
        }

        return n.prototype.set = function (e) {
            return null !== e && (this.value = e, i.set(this.setting, e), t("#" + e + "_" + this.setting).get(0).checked = !0, fire("setting:changed", {
                setting: this.setting,
                value: e
            }), fire("" + this.setting + ":changed", {value: e})), this
        }, n.prototype.get = function () {
            return t("input[type=radio][name=" + this.setting + "]:checked").val()
        }, n.prototype.revert = function () {
            return this.set(this.defaultValue)
        }, n
    }(), d = {}, new a("auto_untap"), new a("auto_draw"), new a("3d_transforms_2", r.transforms_3d_a_grade, "You must refresh the browser for this change to work."), new s("size", o.current, o.options), new a("auto_layout", !0), c = function () {
        var t, e;
        e = [];
        for (t in d)e.push(d[t].revert());
        return e
    }, after("restore:defaults", c), l = function () {
        var t, e, n;
        t = !1;
        for (e in d)if (n = d[e], n.get() !== n.defaultValue) {
            t = !0;
            break
        }
        return t
    }, new n("restore_defaults", {
        handler: c,
        control: {condition: l, events: "settings:shown setting:changed"}
    }), u = function () {
        var t;
        return h.toggleClass("enable_3d_transforms", null != (t = d["3dTransforms2"]) ? t.get() : void 0)
    }, u(), after("3dTransforms2:changed", function () {
        return location.reload()
    }), d
}), define("get-setting", ["settings"], function (t) {
    return function (e) {
        var n;
        return null != (n = t[e]) ? n.get() : void 0
    }
});
var __hasProp = {}.hasOwnProperty, __extends = function (t, e) {
    function n() {
        this.constructor = t
    }

    for (var r in e)__hasProp.call(e, r) && (t[r] = e[r]);
    return n.prototype = e.prototype, t.prototype = new n, t.__super__ = e.prototype, t
};
define("battlefield-zone-view", ["distributable-zone-view", "cell", "grid", "get-setting", "window-loaded"], function (t, e, n, r, i) {
    var o, a, s;
    return a = function (t) {
        return {height: t.height, width: t.width, left: t.left, top: t.top, cards: t.model.cards}
    }, s = function (t) {
        return i.then(function () {
            return r("autoLayout") && n.enable(a(t)), after("size:changed", n.render), after("autoLayout:changed", function () {
                return r("autoLayout") ? n.enable(a(t)) : n.disable()
            })
        })
    }, o = function (t) {
        function e(t) {
            e.__super__.constructor.call(this, t), s(this)
        }

        return __extends(e, t), e.prototype.cacheDimensions = function () {
            var t, i = this;
            return t = {
                width: this.width,
                height: this.height
            }, e.__super__.cacheDimensions.call(this).then(function () {
                return r("autoLayout") ? n.enable(a(i)) : void 0
            })
        }, e.prototype.distributeCards = function () {
        }, e.prototype.restingZ = function (t) {
            return t.calculateZ()
        }, e
    }(t)
});
var __hasProp = {}.hasOwnProperty, __extends = function (t, e) {
    function n() {
        this.constructor = t
    }

    for (var r in e)__hasProp.call(e, r) && (t[r] = e[r]);
    return n.prototype = e.prototype, t.prototype = new n, t.__super__ = e.prototype, t
};
define("hand", ["time", "alias", "zepto", "distributable-zone-view", "detect", "sizes"], function (t, e, n, r, i, o) {
    var a;
    return a = function (e) {
        function r(t) {
            var e, o = this;
            r.__super__.constructor.call(this, t), e = function () {
                return o.delayedDistributeCards()
            }, n(window).on("resize orientationchange", e), after("size:changed", e), i.handheld || this.$element.doubleclick(function (t) {
                var e, r;
                return e = null != (r = n(t.target).closest(".card")) ? r[0] : void 0, e ? o.model.cards.filter(function (t) {
                    return t.view().$element.get(0) === e
                }).first().battlefield() : void 0
            })
        }

        return __extends(r, e), r.prototype.distributeCards = function () {
            var e, n, r, i, a, s, u, c, d, l, f = this;
            for (i = 0, r = this.width < o.cardWidth() * this.model.size(), r && (a = this.$element.get(0).clientWidth - o.cardWidth() * this.model.size(), i = -1 * (a / (this.model.size() - 1))), n = d = 0, l = this.model.size(); l >= 0 ? l > d : d > l; n = l >= 0 ? ++d : --d)e = this.model.cards[n], s = (o.cardWidth() - i) * n, e.restingY = u = this.margin, e.r = 0, c = e.z >= 200 ? e.z : this.restingZ(e), e.move(s, u, c), c >= 200 && function (e) {
                var n;
                return n = e, t.wait(300, function () {
                    return n.move(null, null, f.restingZ(n))
                })
            }(e);
            return !1
        }, r.prototype.restingZ = function (t) {
            var e;
            return e = this.model.cards.indexOf(t), e + 1
        }, r
    }(r)
});
var __hasProp = {}.hasOwnProperty, __extends = function (t, e) {
    function n() {
        this.constructor = t
    }

    for (var r in e)__hasProp.call(e, r) && (t[r] = e[r]);
    return n.prototype = e.prototype, t.prototype = new n, t.__super__ = e.prototype, t
};
define("library", ["event", "distributable-zone-view", "zepto", "draw"], function (t, e, n, r) {
    var i;
    return i = function (t) {
        function e(t) {
            var n, i = this;
            e.__super__.constructor.call(this, t), n = function (t) {
                return (popup.visible || !t.target.className.match(/card|back/)) && t.target.id !== i.model.name || i.model.last().recentlyDragged ? void 0 : (r(), t.preventDefault())
            }, this.$element.on("click", n), after("library:shuffled", function () {
                return i.shuffle()
            })
        }

        return __extends(e, t), e.prototype.shuffle = function () {
            return this.model.cards.empty() || this.model.cards.first().view() && (this.model.cards.invoke("view").invoke("attach"), this.render(), this.renderLastVisibleCards()), this
        }, e
    }(e)
}), define("zone-views", ["zone-models", "distributable-zone-view", "battlefield-zone-view", "hand", "library"], function (t, e, n, r, i) {
    var o;
    return o = {
        library: new i(t.library),
        hand: new r(t.hand),
        battlefield: new n(t.battlefield),
        graveyard: new e(t.graveyard),
        exiled: new e(t.exiled)
    }
}), define("are-you-sure", [], function () {
    var t;
    return t = function (t, e) {
        return confirm("Are you sure " + t) ? e() : void 0
    }
}), define("mulligan", ["event", "draw", "are-you-sure", "library-model", "hand-model"], function (t, e, n, r, i) {
    var o, a;
    return a = 0, o = function () {
        return n("you want to mulligan?", function () {
            for (a = 6 === a ? 6 : a + 1, fire("mulligan", {mulligans: a}); !i.empty();)i.first().library();
            return r.shuffle(), defer(function () {
                return e(7 - a)
            })
        })
    }
}), define("zone-initialize", ["lib/zepto/zepto", "zone-models", "zone-views", "get-active-card", "mulligan", "are-you-sure"], function (t, e, n, r, i, o) {
    var a, s, u, c, d, l, f, h, p;
    return l = e.library, d = e.hand, a = e.battlefield, c = e.graveyard, u = e.exiled, onQuickClick("#untap_link", function () {
        return a.untap()
    }), after("u:pressed", function () {
        return a.untap()
    }), p = t("#toggle_hand_visibility_link"), h = function () {
        var t, e;
        return t = d.cards.findAll(function (t) {
            return t.flipping
        }), t.length > 0 ? !1 : (e = d.hidden() ? "faceup" : "facedown", d.cards.invoke(e), f())
    }, f = function () {
        var t;
        return t = d.hidden() ? "Show Hand" : "Hide Hand", t !== p.text() ? p.text(t) : void 0
    }, s = function (t, e) {
        return after(t, function (t) {
            var n;
            return n = t.data.card, f(), w[e]("" + n.id + ":facedown " + n.id + ":faceup", f)
        })
    }, s("onhand", "after"), s("onleavehand", "forget"), after("h:pressed", h), onQuickClick(p, h), onQuickClick("#mulligan", i), after("m:pressed", i), onQuickClick("#restart", function () {
        return o("you want to restart?", function () {
            return location.reload()
        })
    }), onQuickClick("#shuffle_library", function () {
        return l.shuffle()
    }), after("s:pressed", function () {
        return l.shuffle()
    }), after("Y:pressed", function () {
        var t;
        return t = r(), t.can("library") ? l.addToBottom(t) : void 0
    }), {zones: e, zoneViews: n}
}), define("draganddrop", ["event", "zepto", "zone-initialize", "detect", "sizes", "get-focused-field"], function (t, e, n, r, i, o) {
    var a, s, u, c, d, l, f, h, p, m;
    return l = n.zones.hand, a = n.zones.battlefield, m = n.zoneViews, u = {dragged: !1}, s = function (t, e) {
        return {x: t.clientX - e.x, y: t.clientY - e.y}
    }, u.startDrag = function (n, i) {
        return void 0 === u.dragWithCard ? (u.dragWithCard = function (t) {
            return c(r.handheld ? t.targetTouches[0] : t, i)
        }, e(document).on(t.move, u.dragWithCard).on(t.up, u.tearDown)) : void 0
    }, u.tearDown = function () {
        return e(document).off(t.move, u.dragWithCard).off(t.up, u.tearDown), u.dragWithCard = void 0
    }, u.draggedCard = u.targetZone = u.dragWithCard = void 0, h = function (n, r) {
        var i;
        return u.dragged = !0, o() && e(o()).blur().trigger("blur"), r.view().unbindToMouseOver(), u.draggedCard && f(), r.view().detach(), u.draggedCard = r, r.view().$element.addClass("dragging"), p(n), i = s(n, r), u.initialOffsetX = i.x, u.initialOffsetY = i.y, e(document).on(t.up, f)
    }, f = function (n) {
        var r, i, o, s, c, h, v, g, y;
        return r = u.draggedCard, i = r.view(), v = r.zone(), g = v.view(), (null != (y = n.changedTouches) ? y.length : void 0) > 0 && (n = n.changedTouches[0]), p(n), c = n.clientX - m[u.targetZone.id].left - u.initialOffsetX, h = n.clientY - m[u.targetZone.id].top - u.initialOffsetY, r.move(c, h), u.targetZone === v ? (o = v === a ? "attach" : "revert", v === l && "revert" === o ? (i.attach(), r.move(null, null, mtg.Z.FLOATING), s = d(r, n), v.add(r, s)) : i[o](n)) : (r[u.targetZone.id](), u.targetZone === l && l.include(r) && (s = d(r, n), u.targetZone.add(r, s))), i.$element.removeClass("dragging"), e(".zone").removeClass("hover"), i.bindToMouseOver(), e(document).off(t.up, f), r.recentlyDragged = !0, wait(20, function () {
            var t;
            return t = u.draggedCard, function () {
                return t.recentlyDragged = !1
            }
        }()), u.draggedCard = void 0, u.dragged = !1
    }, d = function (t, e) {
        var n, r, o, a, s;
        if (a = l.cards.pluck("x").without(t.x).map(function (t) {
                return t + i.cardWidth() / 2
            }), s = e.clientX - l.view().left, r = !1, o = a.length - 1, a[0] >= s ? r = 0 : s >= a.last() && (r = o + 1), r === !1)for (n = 1; o >= n && r === !1;)s >= a[n - 1] && a[n] >= s && (r = n), n += 1;
        return r
    }, p = function (t) {
        var n, i, o, s, c;
        c = [];
        for (n in m)i = m[n], u.targetZone !== i.model ? i.left + i.width > (o = t.clientX) && o > i.left && i.top + i.height > (s = t.clientY) && s > i.top ? (u.targetZone = i.model, r.handheld || e(".zone").removeClass("hover"), i.model !== a ? c.push(i.$element.addClass("hover")) : c.push(void 0)) : c.push(void 0) : c.push(void 0);
        return c
    }, c = function (t, e) {
        var n, r;
        return u.dragged || h(t, e), p(t), n = t.clientX - u.initialOffsetX, r = t.clientY - u.initialOffsetY, u.draggedCard.move(n, r)
    }, u
}), define("card-state-helpers", [], function () {
    var t, e, n;
    return t = function () {
        return this.card.r += this.deg
    }, n = function () {
        return this.card.r -= this.deg
    }, e = function (t) {
        return this.card = t, this
    }, {rotate: t, unrotate: n, setCard: e}
}), define("card-tap-state", ["state-machine", "card-state-helpers"], function (t, e) {
    var n, r, i;
    return n = function () {
        function t() {
            return i = r.apply(this, arguments)
        }

        return r = e.setCard, t.prototype.deg = 90, t.prototype.ontap = e.rotate, t.prototype.onuntap = e.unrotate, t.prototype.ontapped = function () {
            return this.card ? fire("" + this.card.id + ":tapped") : void 0
        }, t.prototype.onuntapped = function () {
            return this.card ? fire("" + this.card.id + ":untapped") : void 0
        }, t
    }(), t.create({
        target: n.prototype,
        initial: "untapped",
        events: [{name: "tap", from: "untapped", to: "tapped"}, {name: "untap", from: "tapped", to: "untapped"}]
    }), n
}), define("card-flip-state", ["state-machine", "card-state-helpers"], function (t, e) {
    var n, r, i;
    return n = function () {
        function t() {
            return i = r.apply(this, arguments)
        }

        return r = e.setCard, t.prototype.deg = 180, t.prototype.onflip = e.rotate, t.prototype.onunflip = e.unrotate, t.prototype.onflipped = function () {
            return this.card ? fire("" + this.card.id + ":flipped") : void 0
        }, t.prototype.onunflipped = function () {
            return this.card ? fire("" + this.card.id + ":unflipped") : void 0
        }, t
    }(), t.create({
        target: n.prototype,
        initial: "unflipped",
        events: [{name: "flip", from: "unflipped", to: "flipped"}, {name: "unflip", from: "flipped", to: "unflipped"}]
    }), n
}), define("card-face-state", ["state-machine"], function (t) {
    var e;
    return e = function () {
        function t(t) {
            this.card = t, this.dofacedown()
        }

        return t.prototype.onfacedown = function () {
            return fire("" + this.card.id + ":facedown")
        }, t.prototype.onfaceup = function () {
            return fire("" + this.card.id + ":faceup")
        }, t
    }(), t.create({
        target: e.prototype,
        events: [{name: "dofacedown", from: ["faceup", "none"], to: "facedown"}, {
            name: "dofaceup",
            from: "facedown",
            to: "faceup"
        }]
    }), e
}), define("card-transform-state", ["state-machine", "card-state-helpers"], function (t, e) {
    var n, r, i;
    return n = function () {
        function t() {
            return i = r.apply(this, arguments)
        }

        return r = e.setCard, t.prototype.ontransform = function () {
            return this.card.copyProperties("transformation").facedown(), fire("" + this.card.id + ":transformed")
        }, t.prototype.onuntransform = function () {
            return this.card.copyProperties("data").faceup(), fire("" + this.card.id + ":untransformed")
        }, t
    }(), t.create({
        target: n.prototype,
        initial: "default",
        events: [{name: "untransform", from: "transformed", to: "default"}, {
            name: "transform",
            from: "default",
            to: "transformed"
        }]
    }), n
}), define("card-zone-state", ["state-machine", "card-state-helpers", "zone-names"], function (t, e, n) {
    var r, i;
    return r = [], n.forEach(function (t) {
        return ["onbefore", "onleave", "onenter", "on"].forEach(function (e) {
            return r.push(e + t)
        })
    }), i = function () {
        function e(e) {
            var n = this;
            return this.card = e, r.forEach(function (t) {
                return n[t] = function (e, r, i) {
                    var o;
                    return o = {card: n.card, event: e, from: r, to: i}, fire(t, o), fire("" + n.card.id + ":" + t, o)
                }
            }), w.jasmine || (this.onleavenone = function () {
                return t.ASYNC
            }), this[this.card.initialZone](), this
        }

        return e
    }(), t.create({
        target: i.prototype, events: function () {
            var t;
            return t = n.copy(), t.push("none"), t.map(function (e) {
                var n;
                return {
                    name: e, to: e, from: function () {
                        var r, i, o;
                        for (o = [], r = 0, i = t.length; i > r; r++)n = t[r], n !== e ? o.push(n) : o.push(void 0);
                        return o
                    }()
                }
            })
        }()
    }), i
});
var __slice = [].slice;
define("card-model", ["zone-names", "zone-initialize", "string", "draganddrop", "sizes", "detect", "card-model-string-helpers", "grid", "card-tap-state", "card-flip-state", "card-face-state", "card-transform-state", "card-zone-state", "card-views", "mtg", "loaded"], function (t, e, n, r, i, o, a, s, u, c, d, l, f, h, p, m) {
    var v, g, y, b, w, x;
    x = e.zones, w = x.hand, y = x.battlefield, v = function () {
        function t(t) {
            var e, n, h, v, g, b, x, k, _, C, z, T, S, E, O, A = this;
            if (this.p = this.t = this.c = 0, $.extend(this, t), this.data = t, null == (k = this.id) && (this.id = a.formatIdString(this.name, this.n)), this.transformed = !1, this.isTransformable = "transformation"in t, this.r = this.x = this.y = this.z = 0, this.events = [], this.isCreature = !!t.p, this.isPlainswalker = !!t.c, null == (_ = this.isCommander) && (this.isCommander = !1), this.type) {
                for (h = [], C = this.type, v = 0, b = C.length; b > v; v++)if (n = C[v], "Basic Land" === n)h.push("Land"); else if (n.include(" "))for (z = n.split(" "), g = 0, x = z.length; x > g; g++)e = z[g], h.push(e); else h.push(n);
                this.type = h
            }
            this.type || (this.type = [], this.isCreature && this.type.push("Creature"), this.isPlaneswalker && this.type.push("Planeswalker")), null == (T = this.pMod) && (this.pMod = 0), null == (S = this.tMod) && (this.tMod = 0), null == (E = this.cMod) && (this.cMod = 0), this.tapState = new u(this), this.flipState = new c(this), this.faceState = new d(this), this.isTransformable && (this.transformState = new l(this)), null == (O = this.initialZone) && (this.initialZone = "library"), this.zoneState = new f(this), m.then(function () {
                return A.after("" + A.id + ":onhand " + A.id + ":onbattlefield " + A.id + ":onlibrary " + A.id + ":onexiled " + A.id + ":ongraveyard", function () {
                    var t, e, n;
                    return t = A.zone().view(), t && A !== r.draggedCard && (e = A.x - t.left, n = A.y - t.top), A.move(e, n, A.calculateZ())
                })
            }), this.after("" + this.id + ":onbattlefield", function () {
                var t;
                return A.faceup(), t = !!function () {
                    return r.draggedCard
                }(), y.view() && defer(function () {
                    return s.get() ? s.get().addCard(A) : t ? void 0 : A.move(null, 5)
                }), A
            }), this.after("" + this.id + ":onleavebattlefield", function () {
                var t;
                return null != (t = s.get()) && t.removeCard(A), A.untap().unflip().revertNoteValues(), A.transformed && A.transform(), A
            }), m.then(function () {
                return A.after("" + A.id + ":onleavehand " + A.id + ":onleavebattlefield " + A.id + ":onleavelibrary " + A.id + ":onleaveexiled " + A.id + ":onleavegraveyard", function () {
                    var t, e, n, i, o;
                    return n = A.zone(), i = null != n ? n.view() : void 0, o = i && null != i.left && null != i.top, n && i && o && r.draggedCard !== A && (t = A.x + i.left, e = A.y + i.top, A.move(t, e, p.Z.FLOATING)), A
                })
            }), this.after("" + this.id + ":onhand", function () {
                return (!w.hidden() || 1 === w.size() && w.first() === A) && A.faceup(), A
            }), this.after("" + this.id + ":ongraveyard", function () {
                return A.faceup()
            }), this.after("" + this.id + ":onexiled", function () {
                return A.faceup()
            }), this.after("" + this.id + ":onlibrary", function () {
                return A.facedown()
            }), this.boundRewritePath = function () {
                var t;
                return t = /small\.|medium\.|large\./, A.path.match(t) && (A.path = A.path.replace(t, "" + i.tappedoutSizes[o.retina ? "large" : i.current] + "."), fire("" + A.id + ":path:changed", A)), A
            }, this.after("size:changed", this.boundRewritePath)
        }

        return t.prototype.move = function (t, e, n) {
            return null != t && (this.x = t), null != e && (this.y = e), null != n && (this.z = n), fire("" + this.id + ":moved", this), this
        }, t.prototype.mod = function (t, e) {
            var n;
            return n = t.toUpperCase(), 0 > this[t] + e && (e = 0), this["" + t + "Mod"] += e, this[n](this[t] + e), this
        }, t.prototype.plus = function (t, e) {
            return this.mod(t, e), this
        }, t.prototype.minus = function (t, e) {
            return this.mod(t, -1 * e), this
        }, t.prototype.destroy = function () {
            var t = this;
            return this.zoneState.none(), this.events.forEach(function (e) {
                return t.forget(e)
            }), fire("" + this.id + ":destroyed"), this
        }, t.prototype.after = function (t, e) {
            return this.events.include(t) || this.events.push(t), after(t, e), this
        }, t.prototype.forget = function (t, e) {
            return null == e && (e = !1), this.events.without(t), forget(t, e), this
        }, t.prototype.zone = function () {
            return x[this.zoneState.current]
        }, t.prototype.view = function () {
            return require("card-views")[this.id]
        }, t.prototype.revertNoteValues = function () {
            return this.P(this.data.p), this.T(this.data.t), this.C(this.data.c), this.pMod = this.tMod = this.cMod = 0, this
        }, t.prototype.hasNotes = function () {
            return this.t > 0 || this.c > 0
        }, t.prototype.serialize = function () {
            var t, e;
            return t = {
                name: this.name,
                id: this.id,
                p: this.p,
                t: this.t,
                pMod: this.pMod,
                tMod: this.tMod,
                r: this.r,
                x: this.x,
                y: this.y,
                z: this.z,
                states: [this.tapState.current, null != (e = this.transformState) ? e.current : void 0, this.flipState.current, this.faceState.current, this.zoneState.current]
            }, JSON.stringify(t)
        }, t.prototype.update = function (t) {
            var e, n;
            return n = JSON.parse(t), this.name = n.name, this.id = n.id, this.p = n.p, this.t = n.t, this.pMod = n.pMod, this.tMod = n.tMod, this.r = n.r, this.x = n.x, this.y = n.y, this.z = n.z, this.zoneState[n.states.last()](), e = n.states[0].without("ped"), this.can(e) && this[e](), n.states[1] && null != this.transformState && (e = "transformed" === n.states[1] ? "transform" : "untransform", this.transformState[e]()), e = n.states[2].without("ped"), this.can(e) && this[e](), e = n.states[3], this.can(e) ? this[e]() : void 0
        }, t.prototype.transform = function () {
            return this.can("transform") && (this.transformState.is("default") ? (this.transformState.transform(), this.transformed = !0) : (this.transformState.untransform(), this.transformed = !1)), this
        }, t.prototype.copyProperties = function (t) {
            return this.P(this[t].p + this.pMod, !0), this.T(this[t].t + this.tMod, !0), this.name = this[t].name, this
        }, t.prototype.is = function () {
            var t, e;
            return t = arguments[0], e = arguments.length >= 2 ? __slice.call(arguments, 1) : [], e || (e = [t]), e.include(t) || e.push(t), e = e.map(function (t) {
                return t.toLowerCase()
            }), this.type.filter(function (t) {
                return e.include(t.toLowerCase())
            }).length === e.length
        }, t
    }(), "r x y z".split(" ").forEach(function (t) {
        return v.prototype[t.toUpperCase()] = function (e) {
            return null !== e && "number" == typeof e && (this[t] = e, fire("" + t + ":changed", this), fire("" + this.id + ":" + t + ":changed", this)), this[t]
        }
    }), "p t c".split(" ").forEach(function (t) {
        return v.prototype[t.toUpperCase()] = function (e) {
            return null !== e && "number" == typeof e && (this[t] = e, fire("" + t + ":changed", this), fire("" + this.id + ":" + t + ":changed", this)), this[t]
        }
    }), b = function (t, e) {
        return v.prototype[e] = function () {
            return this.can(e) && this[t][e](), this
        }
    }, ["tap", "flip"].forEach(function (t) {
        var e;
        return e = "un" + t, b("" + t + "State", t), b("" + t + "State", e), v.prototype["toggle" + t.capitalize() + "ped"] = function () {
            var n;
            return n = this.can(t) ? t : e, this[n]()
        }
    }), v.prototype.conditions = {
        tap: function () {
            return this.zone() === y && this.tapState.can("tap")
        }, untap: function () {
            return this.tapState.can("untap")
        }, flip: function () {
            return this.zone() === y && this.flipState.can("flip")
        }, unflip: function () {
            return this.flipState.can("unflip")
        }, faceup: function () {
            return this.faceState.can("dofaceup")
        }, facedown: function () {
            return this.faceState.can("dofacedown")
        }, commander: function () {
            return this.isCommander && this.zoneState.can("commander")
        }, transform: function () {
            return this.isTransformable && this.zone() === y
        }
    }, t.forEach(function (t) {
        var e, n;
        return b("zoneState", t), null != (n = (e = v.prototype.conditions)[t]) ? n : e[t] = function () {
            return this.zoneState.can(t)
        }
    }), v.actions = [];
    for (g in v.prototype.conditions)v.actions.push(g);
    return v.prototype.capabilities = function () {
        var t = this;
        return v.actions.filter(function (e) {
            return t.can(e)
        })
    }, v.prototype.can = function (t) {
        return this.conditions[t].call(this)
    }, v.prototype.cannot = function (t) {
        return !this.can(t)
    }, v.prototype.facedown = function () {
        return this.can("facedown") && this.faceState.dofacedown(), this
    }, v.prototype.faceup = function () {
        return this.can("faceup") && this.faceState.dofaceup(), this
    }, v.prototype.toggleFace = function () {
        var t;
        return t = this.can("faceup") ? "faceup" : "facedown", this[t]()
    }, v.detectIntersection = function (t, e) {
        var n, r, o, a, s, u, c, d;
        return r = e.x <= (s = t.x) && e.x + i.cardWidth() >= s, a = e.y <= (u = t.y) && e.y + i.cardHeight() >= u, o = e.x <= (c = t.x + i.cardWidth()) && e.x + i.cardWidth() >= c, n = e.y <= (d = t.y + i.cardHeight()) && e.y + i.cardHeight() >= d, r && a || o && a || o && n || r && n
    }, v.prototype.calculateZ = function () {
        var t;
        return t = this.intersecting(), t.length + 1
    }, v.prototype.intersecting = function (t) {
        var e, n, r, i, o;
        for (r = t || [], n = this.zone().cards, i = 0, o = n.length; o > i; i++)e = n[i], r.include(e) || this !== e && v.detectIntersection(this, e) && (r.push(e), e.intersecting(r));
        return t || r.splice(r.indexOf(this), 1), r
    }, v.prototype.cleanupZ = function () {
        var t;
        return t = this.z, this.intersecting().forEach(function (e) {
            return e.z > t ? e.move(null, null, e.z - 1) : void 0
        }), this
    }, v
}), define("element", [], function () {
    return function (t, e) {
        var n, r;
        r = document.createElement(t);
        for (n in e)r[n] = e[n];
        return r
    }
}), define("css", ["zepto", "globals", "get-setting"], function (t, e, n) {
    var r;
    return r = {
        getTransformProperty: function () {
            var t, e, n, r, i;
            for (n = ["transform", "WebkitTransform", "MozTransform"], t = w.b.get(0), r = 0, i = n.length; i > r; r++)if (e = n[r], null != t.style[e])return e
        }(), transform: function (e, i, o, a, s) {
            var u, c;
            return u = t(e), e = u.get(0), n("3dTransforms2") ? (c = " translate3d(" + i + "px, " + o + "px, " + s + "px)", e.style[r.getTransformProperty] = c) : u.css({
                zIndex: s,
                left: i + "px",
                top: o + "px"
            }), e
        }
    }
}), define("card-view", ["require", "zepto", "event", "element", "zone-models", "css", "draganddrop", "string", "sizes", "grid", "get-setting", "card-dimensions", "popup", "deck-loaded"], function (t, e, n, r, i, o, a, s, u, c, d, l, f, h) {
    var p, m, v, g;
    return v = i.library, m = i.battlefield, g = t("mtg"), p = function () {
        function t(t) {
            var i, o, s = this;
            this.model = t, this.invokingAfterAWhile = this.pressed = !1, this.id = this.model.id, this.$element = e(r("div", {
                className: "card",
                id: this.id
            })), this.render(), l[this.model.name] && this.setImageDimensions(), after("" + this.model.name.without(" ") + ":dimensions:loaded", function (t) {
                return s.setImageDimensions(t.data)
            }), wait(100, function () {
                return after("size:changed", function () {
                    return s.setImageDimensions(), s.renderBackImageSrc()
                })
            }), after("" + this.model.id + ":path:changed", function (t) {
                return s.renderImageSrc(t.data.path)
            }), this.bindToMouseOver(), o = function () {
                return function (t) {
                    return t.touches || null != t.which && 1 === t.which ? (s.pressed = !0, a.draggedCard || a.startDrag(t, s.model), t.preventDefault(), s.invokePopupAfterAWhile(t)) : void 0
                }
            }(), this.on(n.down, o), i = function () {
                return s.pressed = !1, a.dragged || f.visible || w.modalRecentlyVisible ? void 0 : s.model.toggleTapped()
            }, this.$element.on(n.up, ".face", i), e(".notes", this.$element).on("click", function () {
                return f.show(s.model)
            }), e(".face", this.$element).on(n.down, function () {
            }), this.after("tapped", function () {
                return s.tap()
            }).after("untapped", function () {
                return s.untap()
            }).after("flipped", function () {
                return s.flip()
            }).after("unflipped", function () {
                return s.unflip()
            }).after("facedown", function () {
                return s.facedown()
            }).after("faceup", function () {
                return s.faceup()
            }).after("moved", function () {
                return s.updateTransform()
            }).after("p:changed", function () {
                return s.renderNotes()
            }).after("t:changed", function () {
                return s.renderNotes()
            }).after("c:changed", function () {
                return s.renderNotes()
            }), this[this.model.faceState.current](), this.after("destroyed", function () {
                return s.destroy()
            }), this.model.isTransformable && this.after("onbattlefield", function () {
                return s.useTransformImagePath()
            }).after("onleavebattlefield", function () {
                return s.renderBackImageSrc()
            }).after("transformed", function () {
                return s.toggleTransformedClass(!0)
            }).after("untransformed", function () {
                return s.toggleTransformedClass(!1)
            })
        }

        return t.prototype.render = function () {
            var t;
            return t = "        <div class='card_shape'>          <div class='card_image face' style=\"background-image: url(" + this.model.path.replace(/'/g, "\\'") + ")\"></div>          <div class='back_rotate face'>            <div style='background-image: url(" + g.cardBackPath() + ")' class='card_back'></div>          </div>        </div>        <div id='" + this.model.id + "notes' class='notes'>          <span class='note power_and_toughness' id='" + this.model.id + "power_and_toughness'></span>          <span class='note counters' id='" + this.model.id + "counters'></span>        </div>", this.$element.html(t), this.setNoteElements(), this
        }, t.prototype.on = function (t, e) {
            return this.$element.on(t, e), this
        }, t.prototype.after = function (t, e) {
            return after("" + this.id + ":" + t, e), this
        }, t.prototype.forget = function (t, e) {
            return forget("" + this.id + ":" + t, e), this
        }, t.prototype.invokePopupAfterAWhile = function () {
            var t = this;
            return this.invoking && clearTimeout(this.invoking), this.invoking = wait(350, function () {
                return null == a.draggedCard && t.pressed ? (a.tearDown(), fire("popup:invoked", {
                    card: t.model,
                    fromCard: !0
                })) : void 0
            })
        }, t.prototype.detach = function () {
            var t, e;
            return this.rememberCardX = this.model.x, this.rememberCardY = this.model.y, this.rememberCardZ = this.model.z, b.append(this.$element), this.model.cleanupZ(), t = this.model.zone().view().left + this.model.x, e = this.model.zone().view().top + this.model.y, this.model.move(t, e, g.Z.FLOATING)
        }, t.prototype.attach = function (t) {
            var e, n, r = this;
            return this.model.zone().view().$element.append(this.$element), t && this.model.zone() === m ? (e = t.clientX, n = t.clientY, defer(function () {
                var t, i, o, a;
                return a = c.get(), a ? (i = e - m.view().left, o = n - m.view().top, t = a.findCellByCoords(i, o), t ? (a.removeCard(r.model), t.add(r.model)) : a.addCard(r.model)) : void 0
            })) : void 0
        }, t.prototype.revert = function () {
            var t = this;
            return this.attach(), defer(function () {
                return t.model.move(t.rememberCardX, t.rememberCardY, t.rememberCardZ), t.rememberCardX = t.rememberCardY = t.rememberCardZ = void 0
            }), !1
        }, t.prototype.tap = function () {
            return this.$element.addClass("tapped"), this.updateTransform()
        }, t.prototype.untap = function () {
            return this.$element.removeClass("tapped"), this.updateTransform()
        }, t.prototype.flip = function () {
            return this.$element.addClass("flipped"), this.updateTransform()
        }, t.prototype.unflip = function () {
            return this.$element.removeClass("flipped"), this.updateTransform()
        }, t.prototype.facedown = function (t) {
            var e, n, r = this;
            return e = d("3dTransforms2") ? g.transitionDuration : 0, null != t && t === !0 && (e = 0), this.flipping = !0, n = this.model.z, this.model.z = g.Z.FLOATING, v.include(this.model) && (this.model.z += this.model.zone().size()), e > 0 ? (wait(e / 2, function () {
                return r.$element.addClass("facedown").removeClass("faceup")
            }), wait(2 * (e / 3), function () {
                return r.model.z = n, r.updateTransform(), r.flipping = !1
            })) : (this.$element.addClass("facedown").removeClass("faceup"), this.model.z = n, this.flipping = !1), this.updateTransform(), this.fire("facedown")
        }, t.prototype.faceup = function () {
            var t, e, n = this;
            return t = d("3dTransforms2") ? g.transitionDuration : 0, this.flipping = !0, e = this.z, this.model.z = g.Z.FLOATING, v.include(this.model) && (this.model.z += i[this.model.zoneState.current].size()), t > 0 ? (wait(t / 2, function () {
                return n.$element.removeClass("facedown").addClass("faceup")
            }), wait(2 * (t / 3), function () {
                return n.model.z = e, n.updateTransform(), n.flipping = !1
            })) : (this.$element.removeClass("facedown").addClass("faceup"), this.model.z = e, this.updateTransform(), this.flipping = !1), this.updateTransform(), this.fire("faceup"), this
        }, t.prototype.updateTransform = function () {
            return o.transform(this.$element, this.model.x, this.model.y, this.model.r, this.model.z), this
        }, t.prototype.fire = function (t) {
            return fire("card:" + t, {action: t, card: this})
        }, t.prototype.setNoteElements = function () {
            var t, e, n;
            return null == (t = this.$notes) && (this.$notes = this.$element.find("#" + this.model.id + "notes")), null == (e = this.$ptField) && (this.$ptField = this.$element.find("#" + this.model.id + "power_and_toughness")), null == (n = this.$cField) && (this.$cField = this.$element.find("#" + this.model.id + "counters")), this.renderNotes()
        }, t.prototype.renderNotes = function () {
            return this.$ptField.text(this.model.t > 0 ? "" + this.model.p + " / " + this.model.t : ""), this.$cField.text(this.model.c > 0 ? this.model.c + "c" : ""), this.$element.toggleClass("has_notes", this.model.hasNotes())
        }, t.prototype.destroy = function () {
            return this.$ptField.remove(), this.$cField.remove(), this.$notes.remove(), this.$element.remove(), this.forget("tapped").forget("untapped").forget("flipped").forget("unflipped").forget("facedown").forget("faceup").forget("moved").forget("p:changed").forget("t:changed").forget("c:changed").forget("destroyed")
        }, t.prototype.useTransformImagePath = function () {
            var t = this;
            return h.then(function (e) {
                return t.renderBackImageSrc(e.media_root + t.model.data.transformation.image_path)
            }), this
        }, t.prototype.toggleTransformedClass = function (t) {
            return t ? this.useTransformImagePath() : this.renderBackImageSrc(), this.$element.toggleClass("transformed", t)
        }, t.prototype.setImageDimensions = function (t) {
            var n, r;
            return t || (t = l[this.model.name]), r = u.targetSizes[u.current], n = !t || t && (t.width > r.width + 5 || t.height > r.height + 5), n && (t = r), e(".face", this.$element).css({
                height: "" + t.height + "px",
                width: "" + t.width + "px",
                backgroundSize: "" + t.width + "px " + t.height + "px"
            })
        }, t.prototype.renderImageSrc = function (t) {
            return e(".card_image.face", this.$element).css("background-image", "url(" + t + ")")
        }, t.prototype.renderBackImageSrc = function (t) {
            return e(".card_back", this.$element).css("background-image", "url(" + (t || g.cardBackPath()) + ")")
        }, t.prototype.bindToMouseOver = function () {
            var t = this;
            return this.on("mouseover", function () {
                return g.mousedOverCard = t.model
            }).on("mouseout", function () {
                return g.mousedOverCard === t.model ? g.mousedOverCard = void 0 : void 0
            })
        }, t.prototype.unbindToMouseOver = function () {
            return this.$element.off("mouseover").off("mouseout")
        }, t
    }()
}), define("process-deck", ["q", "globals", "zepto", "card-views", "card-model", "card-view", "detect", "preload-images", "sizes", "get-setting", "card-model-string-helpers"], function (t, e, n, r, i, o, a, s, u, c, d) {
    var l, f, h, p, m;
    return h = t.defer(), m = h.promise, l = t.defer(), f = l.promise, p = function (t) {
        var e, n, a, c, p, v, g, y, b, x, k, _, C, z, T, S, E, $, O, A, P, q;
        c = [];
        for (a in t.cards)for (n = t.cards[a], C = d.parsePTC(n.pt), _ = d.buildPath(t.media_root + u.replaceSizeInPath(n.image_path), n.edition, a), p = n.edition, k = a.convertChars(), S = n.transform, b = null != n.isCommander == !0, S && (E = d.parsePTC(n.transform.pt), S.p = E.p, S.t = E.t, S.c = E.c || 0), z = O = 1, q = Number(n.quantity); q >= 1 ? q >= O : O >= q; z = q >= 1 ? ++O : --O)e = {
            name: k,
            edition: p,
            path: _,
            p: C.p,
            t: C.t,
            c: C.c || 0,
            n: z,
            isCommander: b,
            type: n.type.copy()
        }, S && (e.transformation = n.transform), c.push(e);
        if (c = c.shuffle(), y = function (t) {
                return [t.name, t.path]
            }, v = c.slice(-7).map(y), T = c.slice(0, c.length - 7).map(y), g = s(v), g.then(function () {
                return s(T).then(function () {
                })
            }), !w.jasmine)for ($ = function (t) {
            return g.then(function () {
                var e;
                return e = new o(t), r[t.id] = e, t.zoneState.transition()
            })
        }, A = 0, P = c.length; P > A; A++)n = c[A], x = new i(n), x.isCommander && function (t) {
            return m.then(function () {
                return l.resolve(t)
            })
        }(x), $(x);
        return "tokens"in t || (t.tokens = {
            Thopter: {
                image_url: "http://static.tappedout.net/mtg-cards/tokens/thopter_1.jpg",
                pt: "1/1"
            }
        }), g.then(function () {
            return w.jasmine || fire("deck:loaded", t), h.resolve(t), m.then(function () {
                return f.isFulfilled() ? void 0 : l.reject("No commander in deck.")
            })
        })
    }, {processDeck: p, promise: m, commander: f}
}), define("dom", ["element"], function (t) {
    return {
        visible: function (t) {
            return "none" !== t.style.display
        }, element: t
    }
}), define("requestanimationframe", ["alias"], function (t) {
    var e;
    return e = t.w, {
        requestAnimFrame: function () {
            return e.requestAnimationFrame || e.webkitRequestAnimationFrame || e.mozRequestAnimationFrame || e.oRequestAnimationFrame || e.msRequestAnimationFrame || function (t) {
                    return e.setTimeout(t, 1e3 / 60)
                }
        }(), clearAnimFrame: function () {
            return e.cancelRequestAnimationFrame || e.webkitCancelRequestAnimationFrame || e.mozCancelRequestAnimationFrame || e.oCancelRequestAnimationFrame || e.msCancelRequestAnimationFrame || e.cancelAnimationFrame || e.webkitCancelAnimationFrame || e.mozCancelAnimationFrame || e.oCancelAnimationFrame || e.msCancelAnimationFrame || function (t) {
                    return e.clearTimeout(t)
                }
        }()
    }
}), define("analytics", ["event"], function () {
    var t;
    return t = function (t, e) {
        return w._gaq ? _gaq.push(["_trackEvent", t, e]) : void 0
    }, after("setting:changed", function (e) {
        return t("setting:" + e.data.setting, e.data.value)
    }), after("submenu:shown", function (e) {
        return t("submenu:shown", e.data.text)
    }), after("button:clicked", function (e) {
        return t("button:clicked", e.data.id)
    }), after("library:shuffled", function () {
        return t("library", "shuffled")
    }), after("library:addedToTop", function (e) {
        return t("library", "addedToTop", e.data.id)
    }), after("library:addedToBottom", function (e) {
        return t("library", "addedToBottom", e.data.id)
    }), t
}), define("progress", ["lib/zepto/zepto", "event", "globals"], function (t) {
    var e, n, r, i;
    return e = function () {
        function t(t, e) {
            var n, r, i = this;
            this.events = e, this.$el = t, r = function () {
                var t;
                return t = n, after(i.events[t], function () {
                    return i.$el.addClass(t), i[t] = !0
                }), i[t] = !1
            };
            for (n in this.events)r()
        }

        return t
    }(), n = {
        started: "progress:started",
        requested: "deck:requested",
        errored: "deck:errored",
        finished: "hand:drawn",
        hidden: "progressbar:hidden"
    }, i = new e(t("#progress"), n), r = function (e, n) {
        return after(e, function () {
            return t("#progress_label").text(n)
        })
    }, r("deck:errored", "Deck data is loading too slow or errored."), r("hand:drawn", "Deck data successfully loaded."), fire("progress:started"), after("hand:drawn", function () {
        return wait(800, function () {
            return fire("progressbar:hidden")
        })
    }), i
}), define("turns", ["array"], function () {
    return []
}), define("log-turn-action", ["turns"], function (t) {
    var e;
    return e = function (e) {
        var n;
        return n = t.last(), n ? n.action(e) : void 0
    }
}), define("commander", ["process-deck", "button", "zepto", "popup"], function (t, e, n, r) {
    return t.commander.then(function (t) {
        return n(function () {
            return n("html").addClass("has_commander"), n("#draw_card_by_name").after("<a id='get_commander' class='button button_spacer'>Commander</a>"), new e("get_commander", {
                handler: function () {
                    return r.show(t)
                }
            })
        }), t
    }), t.commander
}), define("health", ["q", "zepto", "super-select", "string", "log-turn-action", "commander"], function (t, e, n, r, i, o) {
    var a, s, u, c, d;
    return s = {life: 0, poison: 0}, c = function (t) {
        return function () {
            return s.life = t, e(d)
        }
    }, a = c(40), u = c(20), o.then(a, u), d = function () {
        var t;
        return t = function (t, r) {
            var o, a, u, c, d, l, f;
            return o = e("#" + t + "_field"), f = new n({
                el: o,
                button: e("#" + t + " .super_select_button"),
                template: function () {
                    var e;
                    return e = ("" + t + ": ").capitalize(), function (t) {
                        return "" + e + " " + t
                    }
                }()
            }), a = function () {
                return s[t]
            }, d = function (e) {
                return s[t] = Number(e)
            }, l = function () {
                var e;
                return e = a(), c(), function (e) {
                    return wait(400, function () {
                        return e === a() ? i({name: t.capitalize(), value: e}) : void 0
                    })
                }(e), o.val(e), f.render(), e
            }, c = setupSelecterRenderer(o.get(0), a, r), u = function (e) {
                return d(a() + e), 0 > a() ? d(0) : "poison" === t && a() > 10 && d(10), l()
            }, after(t[0] + ":pressed", function () {
                return u(1)
            }), after(t[0].toUpperCase() + ":pressed", function () {
                return u(-1)
            }), o.on("change", function () {
                return d(o.val()), defer(l)
            }), l()
        }, t("poison", 10), t("life", 100)
    }, s
}), define("format-token-data", ["array", "card-model-string-helpers"], function (t, e) {
    var n;
    return n = function (t) {
        var n, r, i, o, a, s, u, c, d;
        i = t.tokkens, c = [], n = [];
        for (u in i)c.include(u) || (s = i[u], r = e.parsePTC(s.pt), n.push({
            tokenId: u,
            name: s.name,
            p: r.p,
            t: r.t,
            type: "string" == typeof s.type ? [s.type] : s.type.copy(),
            path: s.image_url || t.token_media_root + s.image_path,
            n: 0
        }), c.push(u));
        return o = n.map(function (t) {
            return "" + t.name + t.tokenId
        }).sort(), a = function () {
            var t, e, r;
            for (r = [], t = 0, e = o.length; e > t; t++)d = o[t], r.push(n.find(function (t) {
                return "" + t.name + t.tokenId === d
            }));
            return r
        }(), n = a
    }
}), define("formatted-token-data", ["q", "format-token-data", "deck-loaded"], function (t, e, n) {
    var r, i;
    return r = t.defer(), i = [], n.then(function (t) {
        return i = e(t), i.length ? r.resolve(i) : r.reject(), t
    }), r.promise
});
var __hasProp = {}.hasOwnProperty, __extends = function (t, e) {
    function n() {
        this.constructor = t
    }

    for (var r in e)__hasProp.call(e, r) && (t[r] = e[r]);
    return n.prototype = e.prototype, t.prototype = new n, t.__super__ = e.prototype, t
};
define("token-view", ["card-view", "token-views"], function (t, e) {
    var n;
    return n = function (t) {
        function n(t) {
            n.__super__.constructor.call(this, t), this.setImageDimensions()
        }

        return __extends(n, t), n.prototype.destroy = function () {
            return n.__super__.destroy.call(this), e || (e = require("token-views")), delete e[this.model.id]
        }, n
    }(t)
}), define("token-views", ["lib/zepto/zepto", "keys", "token-view", "formatted-token-data", "token-model", "preload-images"], function (t, e, n, r, i, o) {
    var a, s, u, c, d;
    return d = {}, a = t("#add_token_field"), s = function () {
        return r.then(function (t) {
            var e, r;
            return i || (i = require("token-model")), e = new i(i.getTokenDataById(t, a.val())), r = new n(e), d[e.id] = r, e.zoneState.transition()
        })
    }, onQuickClick("#add_token_link", s), after("k:pressed", function () {
        return a.focus().trigger("focus")
    }), u = function () {
        return r.then(function (t) {
            var e;
            return i || (i = require("token-model")), e = i.getTokenDataById(t, a.val()), o("" + e.name + e.tokenId, e.path)
        })
    }, a.on("keypress", function (t) {
        return "return" === e.getKey(t) && t.target === a[0] ? s() : void 0
    }).on("change", u), c = function (e) {
        var n, r, i, o;
        for (r = "", i = 0, o = e.length; o > i; i++)n = e[i], r += option({value: n.tokenId}, "" + n.name + " " + n.p + "/" + n.t);
        return a.html(r), t("#add_token").show()
    }, r.then(c), d
});
var __hasProp = {}.hasOwnProperty, __extends = function (t, e) {
    function n() {
        this.constructor = t
    }

    for (var r in e)__hasProp.call(e, r) && (t[r] = e[r]);
    return n.prototype = e.prototype, t.prototype = new n, t.__super__ = e.prototype, t
};
define("token-model", ["require", "globals", "card-model", "card-model-string-helpers", "token-views"], function (t, e, n, r) {
    var i;
    return i = function (e) {
        function n(t) {
            var e = this;
            this.initialZone = "battlefield", this.isToken = !0, this.id = "" + r.formatIdString(t.name, t.n) + "_" + n.uniqueIndex, n.__super__.constructor.call(this, t), this.type.include("Token") || this.type.push("Token"), this.type.include("Creature") || this.type.push("Creature"), this.faceup(), n.uniqueIndex += 1, this.boundDestroy = function (t) {
                return t.data.card === e ? (forget("ongraveyard onlibrary onhand onexiled", e.boundDestroy), e.destroy()) : void 0
            }, after("ongraveyard onlibrary onhand onexiled", this.boundDestroy), this.forget("size:changed", this.boundRewritePath)
        }

        return __extends(n, e), n.prototype.view = function () {
            return t("token-views")[this.id]
        }, n
    }(n), i.uniqueIndex = 0, i.getTokenDataById = function (t, e) {
        var n;
        return n = t.find(function (t) {
            return t.tokenId === e
        }), n.n += 1, n
    }, i
}), define("dropdown", ["zepto", "detect", "event"], function (t, e, n) {
    var r, i;
    return r = function () {
        function r(n) {
            var r = this;
            this.$el = t(n), this.$button = this.$el.find(".submenu_button"), this.$submenu = this.$el.find(".submenu"), this.visible = !1, this.disabled = 0 === this.$submenu.length || this.$el.hasClass("no_submenu"), this.disabled ? this.$el.addClass("no_submenu") : (this.$button.on("click", function () {
                return r.visible ? r.hide() : r.show()
            }), e.handheld || this.$button.on("mouseover", function (e) {
                return t(e.target).addClass("hover")
            }).on("mouseout", function (e) {
                return t(e.target).removeClass("hover")
            }))
        }

        return r.prototype.show = function () {
            var t = this;
            return this.$el.addClass("visible"), wait(50, function () {
                return t.boundHideFromOutsideList = function (e) {
                    return t.hideFromOutsideList(e)
                }, h.on(n.down, t.boundHideFromOutsideList)
            }), fire("submenu:shown", {text: this.$el.text()}), this.visible = !0
        }, r.prototype.hide = function () {
            return this.$el.removeClass("visible"), h.off(n.down, this.boundHideFromOutsideList), this.visible = !1
        }, r.prototype.hideFromOutsideList = function (e) {
            return -1 === t(e.target).parents().indexOf(this.$el.get(0)) ? this.hide() : void 0
        }, r
    }(), null == (i = w.app) && (w.app = {}), app.nav = {submenus: [], Submenu: r}, t(function () {
        return app.nav.submenus.push(new r(t("#settings_link").parent())), app.nav.submenus.push(new r(t("#logger_link").parent())), app.nav.submenus.push(new r(t("#stats_link").parent()))
    }), app.nav
}), define("stage", ["string", "zepto", "deck-loaded", "mtg", "sizes", "q"], function (t, e, n, r, i) {
    var o;
    return o = function (t) {
        return e(function () {
            var n;
            return e("html").addClass("card_size_" + i.current), r.deckUrl && e("#deck_name").attr("href", r.deckUrl), e("#deck_name").html(t.name), e("#author").text(t.author).attr("href", "http://tappedout.net/users/" + t.author + "/"), document.title = "" + e("#deck_name").text() + " by " + t.author + ": " + document.title, (null != (n = t.thumb) ? n.length : void 0) > 0 ? e("#pie").html(img({src: t.thumb})).show() : void 0
        })
    }, n.then(o)
}), define("turn-model", ["event", "zone-models", "health"], function (t, e, n) {
    var r, i;
    return r = function () {
        function t(t) {
            var e;
            for (e in t)this[e] = t[e]
        }

        return t
    }(), i = function () {
        function t(t) {
            var e = this;
            this.actions = [], this.n = t, this.current = !0, this.recordState(), this.boundRecordState = function () {
                return e.recordState()
            }, after("turn:" + this.n + ":action:added p:changed t:changed", this.boundRecordState), this.boundEndTurn = function () {
                return e.current = !1, forget("turn:" + e.n + ":action:added", e.boundRecordState), forget("p:changed", e.boundRecordState), forget("t:changed", e.boundRecordState), forget("end:turn", e.boundEndTurn)
            }, after("end:turn", this.boundEndTurn)
        }

        return t.prototype.recordState = function () {
            var t;
            this.life = n.life, this.poison = n.poison;
            for (t in e)this[t] = e[t].exportState();
            return fire("state:updated"), this
        }, t.prototype.action = function (t) {
            var e;
            return null == (e = t.i) && (t.i = this.actions.length), this.actions.push(new r(t)), fire("turn:action:added")
        }, t
    }()
}), define("turn", ["q", "event", "zone-names", "draw", "zone-initialize", "get-setting", "turn-model", "turns", "log-turn-action"], function (t, e, n, r, i, o, a, s, u) {
    var c, d, l;
    return l = i.zones, c = l.battlefield, d = {turns: s, turn: 1}, after("turn:action:added", function () {
        return fire("turn:" + d.turn + ":action:added")
    }), after("end:turn", function () {
        return fire("next:turn")
    }), after("next:turn", function () {
        return s.push(new a(s.length + 1)), d.turn = s.length, $("#turn_el").text(d.turn), o("autoUntap") && c.untap(), o("autoDraw") ? r() : void 0
    }), onQuickClick("#next_turn", function () {
        return fire("end:turn")
    }), after("n:pressed", function () {
        return fire("end:turn")
    }), after("hand:drawn", function () {
        var t;
        return s.push(new a(1)), u({name: "Drawn", zoneName: "Hand"}), t = "", n.forEach(function (e) {
            return t += " on" + e
        }), t += " battlefield:token:added token:destroyed", after(t, function (t) {
            return u({cardName: t.data.card.name, zoneName: t.data.card.zone().name, cardId: t.data.card.id})
        }), after("library:shuffled", function () {
            return u({name: "Shuffled", zoneName: "Library"})
        }), after("mulligan", function (t) {
            return u({name: "Mulligan " + t.data.mulligans, zoneName: "Hand"})
        })
    }), d
}), define("buffs", ["event", "zepto", "battlefield-model"], function (t, e, n) {
    var r;
    return r = function (t, e) {
        return function () {
            return n.getRecipients().forEach(function (n) {
                return n.mod(t, e)
            })
        }
    }, ["power", "toughness", "counters"].forEach(function (n) {
        return e("#increase_" + n).on(t.down, r(n[0], 1)), e("#decrease_" + n).on(t.down, r(n[0], -1))
    })
}), define("logger", ["alias", "event", "zone-names", "types", "zone-initialize", "super-select", "popup", "card-views", "token-views", "turns"], function (t, e, n, r, i, o, a, s, u, c) {
    var d, l;
    return l = 0, after("next:turn:taken", function () {
        return l = 0
    }), after("hand:drawn", function () {
        return LoggerView.start(), StatsView.start()
    }), w.LoggerView = {
        start: function () {
            var t = this;
            return this.nextTurn(), this.nav().$button.on("click", function () {
                return t.render()
            }), after("next:turn", function () {
                return t.nextTurn()
            }), this.$next.on("click", function () {
                return t.nextTurn()
            }), this.$previous.on("click", function () {
                return t.previousTurn()
            }), this.$el.on("click", ".linked_card", function (t) {
                var e, n;
                return n = $(t.target).data("card-id"), e = (s[n] || u[n]).model, a.show(e)
            })
        },
        $el: $("#logger2"),
        $title: $("#turn_title"),
        $tableBody: $("#action_table tbody"),
        $next: $("#view_next_turn"),
        $previous: $("#view_previous_turn"),
        nav: function () {
            return app.nav.submenus[1]
        },
        setTurn: function (t) {
            return this.turn ? t && (this.turn = c[c.indexOf(this.turn) + t]) : this.turn = c.last(), this
        },
        visible: function () {
            return this.nav().visible
        },
        render: function () {
            return this.visible() && (this.$title.text("Turn " + this.turn.n), this.$previous.toggleClass("visible", c.first() !== this.turn), this.$next.toggleClass("visible", c.last() !== this.turn), this.renderActions()), this
        },
        renderActions: function () {
            return this.$tableBody.get(0).innerHTML = this.actionTableRowTemplate(), this
        },
        actionTableRowTemplate: function () {
            var t;
            return this.turn.actions.empty() ? "" : (t = this.turn.actions.map(function (t, e) {
                var n, r, i;
                return t.cardId && (n = s[t.cardId] || u[t.cardId], n && (r = span({
                    "class": "linked_card",
                    "data-card-id": t.cardId
                }, t.cardName))), tr("            " + td({"class": "action_count"}, e + 1) + "            " + td(r || t.cardName || t.name) + "            " + td({"class": "zone_name"}, null != t.value ? t.value : null != (i = t.zoneName) ? i.capitalize() : void 0))
            }), t.join(""))
        },
        previousTurn: function () {
            return this.unbindToTurn().setTurn(-1).render().bindToTurn(), this
        },
        nextTurn: function () {
            return this.unbindToTurn().setTurn(1).render().bindToTurn(), this
        },
        bindToTurn: function () {
            var t = this;
            return this.boundRender = function () {
                return t.render()
            }, after("turn:" + this.turn.n + ":action:added", this.boundRender), this
        },
        unbindToTurn: function () {
            return this.turn && forget("turn:" + this.turn.n + ":action:added", this.boundRender), this
        }
    }, w.StatsView = {
        start: function () {
            var t, e = this;
            return t = $(".metric", this.$el), this.subviews.push(new d({
                el: t[0],
                defaultMetric: "Lands",
                defaultMetricZone: "Hand"
            })), this.subviews.push(new d({
                el: t[1],
                defaultMetric: "Lands",
                defaultMetricZone: "Battlefield"
            })), this.subviews.push(new d({
                el: t[2],
                defaultMetric: "Creatures",
                defaultMetricZone: "Battlefield"
            })), this.subviews.push(new d({
                el: t[3],
                defaultMetric: "Artifacts",
                defaultMetricZone: "Battlefield"
            })), this.subviews.push(new d({
                el: t[4],
                defaultMetric: "Life"
            })), this.nav().$button.on("click", function () {
                return e.render()
            })
        }, subviews: [], $el: $("#stats"), nav: function () {
            return app.nav.submenus[2]
        }, visible: function () {
            return this.nav().visible
        }, render: function () {
            return this.visible() ? this.subviews.invoke("render") : void 0
        }
    }, d = function () {
        function t(t) {
            var e = this;
            this.$el = $(t.el), this.$selectedMetric = $(".selected_metric", this.$el), this.$selectedMetricZone = $(".selected_metric_zone", this.$el), this.superSelectMetric = new o({el: this.$selectedMetric}), this.superSelectMetricZone = new o({el: this.$selectedMetricZone}), this.superSelectMetricZone.$button.addClass("second_column"), this.$selectedMetricValue = $(".selected_metric_value", this.$el), this.$graphArea = $(".graph_area", this.$el), this.$selectedMetric.html(this.metrics.map(function (t) {
                return option(t)
            }).join("")), this.$selectedMetricZone.html(this.zoneNames.map(function (t) {
                return option(t)
            }).join("")), this.$selectedMetric.on("change", function () {
                return e.render()
            }), this.$selectedMetricZone.on("change", function () {
                return e.render()
            }), t.defaultMetric && this.$selectedMetric.val(t.defaultMetric), t.defaultMetricZone && this.$selectedMetricZone.val(t.defaultMetricZone), this.render(), after("state:updated", function () {
                return e.render()
            })
        }

        return t.prototype.render = function () {
            var t, e, n = this;
            return t = this.$selectedMetric.val().toLowerCase(), e = t in c.last() ? !1 : this.$selectedMetricZone.val().toLowerCase(), this.$selectedMetricZone.toggleClass("hidden", !e), this.superSelectMetricZone.toggle(!e), this.$el.find(".in").toggleClass("hidden", !e), this.$selectedMetricValue.text(this.getMetricValue(c.last(), t, e)), this.$graphArea.get(0).innerHTML = function () {
                var r, i;
                return i = (e ? c.pluck(e) : c).pluck(t).max(), r = c.map(function (r, o) {
                    var a, s;
                    return s = n.getMetricValue(r, t, e), a = 40 * (s / i || 0), div({
                        title: "" + s + " " + t + " on turn " + (o + 1),
                        "class": "graph_column"
                    }, div({"class": "graph_bar" + (0 === a ? " empty" : ""), style: "height: " + a + "px;"}, ""))
                }), r.join("")
            }()
        }, t.prototype.getMetricValue = function (t, e, n) {
            return (n ? t[n] : t)[e]
        }, t.prototype.metrics = function () {
            var t, e, n, i;
            for (e = ["Life", "Poison"], n = 0, i = r.length; i > n; n++)t = r[n], e.push(t.plural());
            return e.push("Power"), e.push("Toughness"), e
        }(), t.prototype.zoneNames = n.copy().without("commander").without("none").invoke("capitalize"), t
    }()
}), define("help", ["detect", "event", "modal"], function (t, e, n) {
    var r;
    return t.handheld ? void 0 : (r = new n.Modal("help", "close_help"), $("#help_link").on(e.down, function () {
        return r.show()
    }), after("?:pressed", function () {
        return r.toggle()
    }))
}), define("roll", ["lib/zepto/zepto", "event", "log-turn-action"], function (t, e, n) {
    var r, i, o;
    return r = [2, 4, 6, 8, 10, 12, 20, 100], i = function (t) {
        return Math.ceil(t * Math.random())
    }, o = function () {
        var t, e;
        return e = function () {
            var e, n, o;
            for (o = [], e = 0, n = r.length; n > e; e++)t = r[e], o.push("" + i(t) + "/" + t);
            return o
        }(), n({name: e.join(", "), value: "Roll"}), alert("Your Dice Rolled:\n" + e.join(", "))
    }, t(function () {
        return t("#roll").on(e.down, o)
    }), after("r:pressed", o), i
}), require.config({shim: {"lib/zepto/zepto": {exports: "$"}}}), require(["zepto", "q", "state-machine", "mtg", "deck-url", "loader-helpers", "process-deck", "dom", "super-select", "alias", "string", "array", "requestanimationframe", "detect", "event", "time", "store", "window", "css", "globals", "analytics", "progress", "keys", "sizes", "cell", "cell-collection", "grid", "zone-names", "zone-view", "zone-views", "zone-model", "battlefield-model", "hand", "hand-model", "library-model", "library", "zone-initialize", "distributable-zone-view", "battlefield-zone-view", "card-dimensions", "card-state-helpers", "card-tap-state", "card-flip-state", "card-face-state", "card-transform-state", "card-zone-state", "card-model", "health", "draganddrop", "card-view", "card-views", "format-token-data", "formatted-token-data", "get-focused-field", "token-model", "token-view", "token-views", "draw", "button", "modal", "settings", "get-setting", "dropdown", "popup", "commander", "stage", "log-turn-action", "turn", "turns", "turn-model", "buffs", "logger", "help", "loaded", "deck-loaded", "window-loaded", "roll", "get-active-card", "shortcut"], function () {
    var t, e, n, r, i, o, a, s, u;
    return t = require("zepto"), e = require("q"), a = require("loader-helpers"), r = require("deck-url"), s = require("mtg"), u = require("process-deck"), o = require("dom"), i = e.defer(), window.jasmine || (t(window).on("hashchange", function () {
        return a.getPlaytestURL() ? location.reload() : void 0
    }), n = a.getPlaytestURL(), n ? (s.deckUrl = r(n), fire("deck:requested"), window.processDeck = u.processDeck, t("head").append(o.element("script", {src: n})), i.resolve()) : (i.reject("No deckSrc. Redirecting."), location.href = "http://www.tappedout.net"), i.reject("Jasmine's on the page. Not starting.")), i.promise
}), define("load", function () {
}), require(["load"]);
//@ sourceMappingURL=load-built-min-req.js.map